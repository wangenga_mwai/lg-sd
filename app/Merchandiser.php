<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;

class Merchandiser extends BaseModel
{
    protected $table = 'lg_merchandisers';

    protected static $code = 'LGPC';


    public function checks()
    {
    	return $this->hasMany('App\MerchandiserCheck','merchandiser_code','code');
    }

    
}
