<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'lg_banks';

    public function branches()
    {
    	return $this->hasMany('App\BankBranch','bank_code','code');
    }
}
