<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;

class Shop extends BaseModel
{
    protected $table = 'lg_outlets';

    public function branches()
    {
    	return $this->hasMany('App\ShopBranch','Outlet_code','code');
    }
}
