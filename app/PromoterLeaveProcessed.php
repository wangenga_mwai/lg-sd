<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromoterLeaveProcessed extends BaseModel
{
    protected $table = 'lg_promoters_leaves_processed';
    protected $fillable = ['user_id','promoter_code','year','leave_type_id','start_date','end_date','no_of_days','reason','status','active','created_at','updated_at'];
    protected static $person_code = 'promoter_code';
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    
    public function person()
    {
    	return $this->belongsTo('App\Promoter','promoter_code','code');
    }

    public function type()
    {
    	return $this->belongsTo('App\LeaveType','leave_type_id');
    }
}
