<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    protected $table = 'lg_issue_management';

    public function promoter()
    {
    	return $this->belongsTo('App\Promoter','promoter_code','code');
    }
}
