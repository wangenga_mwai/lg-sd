<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;

class MerchandiserLeaveRequest extends BaseModel
{
    protected $table = 'lg_merchandisers_leaves_requests';

    protected static $person_code = 'merchandiser_code';

    public function person()
    {
    	return $this->belongsTo('App\Merchandiser','merchandiser_code','code');
    }

    public function type()
    {
    	return $this->belongsTo('App\LeaveType','leave_type_id');
    }
}
