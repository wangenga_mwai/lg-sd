<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;

class Group extends BaseModel
{
    protected $table = 'lg_groups';

    public function sub_category()
    {
    	return $this->belongsTo('App\SubCategory', 'sub_category_id');
    }

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }
}
