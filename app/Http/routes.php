<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/home',function(){
	// return public_path().'\assets\img\small_logo.png';
	return view('layouts.mail');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
	Route::get('/login',['uses'=>'UsersController@login']);
	Route::post('/login',['uses'=>'UsersController@authenticate']);
	Route::post('/register',['uses'=>'UsersController@store']);
	
});


Route::group(['middleware' => ['web','auth']], function () {

	Route::get('/',['uses'=>'DashboardController@index']);
	Route::get('/logout',['uses'=>'UsersController@logout']);


	Route::get('/dashboard',['uses'=>'DashboardController@index']);
	Route::get('/dashboard/chart',['uses'=>'DashboardController@chart']);
	
	// www.mtd.com/
	// settings

	// categories
	Route::get('categories',['uses'=>'CategoriesController@index']);
	Route::post('category/add',['uses'=>'CategoriesController@store']);
	Route::get('category/activate/{id}',['uses'=>'CategoriesController@activate']);
	Route::get('category/edit/{id}',['uses'=>'CategoriesController@edit']);
	Route::post('category/edit',['uses'=>'CategoriesController@update']);

	// sub-categries
	Route::get('subcategories',['uses'=>'SubCategoriesController@index']);
	Route::post('sub_category/add',['uses'=>'SubCategoriesController@store']);
	Route::get('sub_category/activate/{id}',['uses'=>'SubCategoriesController@activate']);
	Route::get('sub_category/edit/{id}',['uses'=>'SubCategoriesController@edit']);
	Route::post('sub_category/edit',['uses'=>'SubCategoriesController@update']);

	// Group
	Route::get('groups',['uses'=>'GroupsController@index']);
	Route::post('group/add',['uses'=>'GroupsController@store']);
	Route::get('group/activate/{id}',['uses'=>'GroupsController@activate']);
	Route::get('group/edit/{id}',['uses'=>'GroupsController@edit']);
	Route::post('group/edit',['uses'=>'GroupsController@update']);

	//models
	Route::get('models',['uses'=>'ModelsController@index']);
	Route::post('model/add',['uses'=>'ModelsController@store']);
	Route::get('model/activate/{id}',['uses'=>'ModelsController@activate']);
	Route::get('model/edit/{id}',['uses'=>'ModelsController@edit']);
	Route::post('model/edit',['uses'=>'ModelsController@update']);
	

	// sales
	Route::get('sales',['uses'=>'SalesController@index']);
	Route::get('sales/csv',['uses'=>'SalesController@export_csv']);
	Route::post('sale/delete/{id}',['uses'=>'SalesController@delete']);
	

	// commissions

	Route::get('/commissions',['uses' => 'SalesController@commissions']);
	Route::get('/commissions/csv',['uses' => 'SalesController@commissions_csv']);
	Route::get('/commissions/details',['uses' => 'SalesController@commission_details']);
	Route::get('/commissions/details/csv',['uses' => 'SalesController@commission_details_csv']);

	// sales target
	Route::get('/sales/targets',['uses'=>'SalesTargetsController@index']);
	Route::post('/sales/targets',['uses'=>'SalesTargetsController@set_targets']);
	
	// staff
	Route::get('staff/merchandisers',['uses'=>'MerchandisersController@index']);
	Route::get('staff/promoters',['uses'=>'PromotersController@index']);
	
	// management
	Route::get('issue-management',['uses'=>'ManagementController@issues']);
	Route::get('leave/promoters',['uses'=>'ManagementController@leave_promoters']);
	Route::get('leave/merchandisers',['uses'=>'ManagementController@leave_merchandisers']);
	Route::get('leave/{person}/csv',['uses'=>'ManagementController@csv']);
	Route::get('leave-request/merchandisers',['uses'=>'ManagementController@merchandisers_request']);
	Route::post('leave-request/merchandiser',['uses'=>'ManagementController@approve_merchandiser']);
	Route::get('leave-request/merchandisers/{id}',['uses'=>'ManagementController@merchandisers_request_approve']);

	Route::get('leave-request/promoters',['uses'=>'ManagementController@promoters_request']);
	Route::post('leave-request/promoter',['uses'=>'ManagementController@approve_promoter']);
	Route::get('leave-request/promoters/{id}',['uses'=>'ManagementController@promoters_request_approve']);

	// checks
	Route::get('check/merchandisers',['uses'=>'ChecksController@merchandiser']);
	Route::get('check/merchandisers/{code}',['uses'=>'ChecksController@merchandiser_show']);
	Route::get('checks/merchandiser/{code}/csv',['uses'=>'ChecksController@merchandiser_csv']);
	Route::get('check/promoters',['uses'=>'ChecksController@promoter']);
	Route::get('check/promoters/{code}',['uses'=>'ChecksController@promoter_show']);
	Route::get('checks/promoter/{code}/csv',['uses'=>'ChecksController@promoter_csv']);
	
	Route::get('checks/csv/{person}',['uses'=>'ChecksController@export_csv']);
	
	// merchandiser
	Route::post('merchandisers/add',['uses'=>'MerchandisersController@store']);
	Route::get('merchandisers/csv',['uses'=>'MerchandisersController@csv']);
	Route::get('merchandisers/activate/{id}',['uses'=>'MerchandisersController@activate']);
	Route::get('merchandisers/view/{id}',['uses'=>'MerchandisersController@show']);
	Route::get('merchandisers/edit/{id}',['uses'=>'MerchandisersController@edit']);
	Route::post('merchandisers/edit',['uses'=>'MerchandisersController@update']);
	Route::post('merchandisers/delete/{id}',['uses'=>'MerchandisersController@delete']);
	Route::get('merchandisers/{id}/clear-imei',['uses'=>'MerchandisersController@clear_imei']);

	// promoter
	Route::post('promoters/add',['uses'=>'PromotersController@store']);
	Route::get('promoters/csv',['uses'=>'PromotersController@csv']);
	Route::get('promoters/activate/{id}',['uses'=>'PromotersController@activate']);
	Route::get('promoters/view/{id}',['uses'=>'PromotersController@show']);
	Route::get('promoters/edit/{id}',['uses'=>'PromotersController@edit']);
	Route::post('promoters/edit',['uses'=>'PromotersController@update']);
	Route::post('promoters/delete/{id}',['uses'=>'PromotersController@delete']);
	Route::get('promoters/{id}/clear-imei',['uses'=>'PromotersController@clear_imei']);

	// delivery personnel
	// Route::post('delivery-personnel/add',['uses'=>'DeliveryPersonnelsController@store']);
	// Route::get('delivery-personnel/activate/{id}',['uses'=>'DeliveryPersonnelsController@activate']);
	// Route::get('delivery-personnel/view/{id}',['uses'=>'DeliveryPersonnelsController@show']);
	// Route::get('delivery-personnel/edit/{id}',['uses'=>'DeliveryPersonnelsController@edit']);
	// Route::post('delivery-personnel/edit',['uses'=>'DeliveryPersonnelsController@update']);
	// Route::post('delivery-personnel/delete/{id}',['uses'=>'DeliveryPersonnelsController@delete']);
	


	// display share
	Route::get('display-share',['uses'=>'DashboardController@display_share']);
	Route::get('display-share/{code}',['uses'=>'DashboardController@branch_display_share']);



	// outlets
	Route::get('outlets',['uses'=>'OutletsController@index']);
	Route::post('outlet/add',['uses'=>'OutletsController@store']);
	Route::get('outlet/activate/{id}',['uses'=>'OutletsController@activate']);
	Route::get('outlet/edit/{id}',['uses'=>'OutletsController@edit']);
	Route::post('outlet/edit',['uses'=>'OutletsController@update']);
	Route::get('outlet/{code}/branches',['uses'=>'OutletsController@branches']);
	Route::post('outlet/delete/{id}',['uses'=>'OutletsController@delete']);

	// outlet branches
	Route::post('outlet/branch/add',['uses'=>'OutletBranchesController@store']);
	Route::get('outlet/branch/activate/{id}',['uses'=>'OutletBranchesController@activate']);
	Route::get('outlet/branch/edit/{id}',['uses'=>'OutletBranchesController@edit']);
	Route::post('outlet/branch/edit',['uses'=>'OutletBranchesController@update']);
	Route::post('outlet/branch/delete/{id}',['uses'=>'OutletBranchesController@delete']);

	// customers
	Route::get('customers',['uses'=>'CustomersController@index']);
	Route::post('customer/add',['uses'=>'CustomersController@store']);
	Route::get('customer/csv',['uses'=>'CustomersController@csv']);
	Route::get('customer/activate/{id}',['uses'=>'CustomersController@activate']);
	Route::get('customer/edit/{id}',['uses'=>'CustomersController@edit']);
	Route::post('customer/edit',['uses'=>'CustomersController@update']);
	Route::get('customer/sales/{id}',['uses'=>'CustomersController@sales']);
	

	Route::get('messages',['uses'=>'DashboardController@messages']);
	Route::post('messages',['uses'=>'DashboardController@post_messages']);



	// user management
	Route::get('users', ['uses' => 'UsersController@user_permissions']);
	Route::post('user', ['uses' => 'UsersController@store_by_admin']);
	Route::post('user/set-permission', ['uses' => 'UsersController@user_set_permission']);

	Route::get('/user/change-password', ['uses' => 'UsersController@show_change_password']);
	Route::post('/user/change-password', ['uses' => 'UsersController@change_password']);

	Route::get('leads',['uses'=>'LeadsController@index']);
	Route::get('leads/promoters',['uses'=>'LeadsController@promoters']);
	Route::get('leads/promoter/{code}/view',['uses'=>'LeadsController@promoter_leads']);
	

	//digital brochure
	Route::get('brochures',['uses'=>'BrochuresController@index']);
	Route::get('brochures/csv',['uses'=>'BrochuresController@csv']);
	


	// Ajax responses
	Route::get('/banks/{code}',['uses'=>'BanksController@index']);
	Route::get('/outlets/{code}',['uses'=>'OutletsController@index']);

});
