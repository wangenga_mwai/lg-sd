<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Bank;

use App\Http\Requests;

class BanksController extends Controller
{
    public function index($code=0)
    {
    	if($code !== 0){
    		return Bank::where('code',$code)->firstorFail()->branches;
    	}
    }
}
