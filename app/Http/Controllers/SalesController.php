<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;
use App\Sale;

use CSV;
use DateTime;

use App\Http\Requests;

class SalesController extends BaseController
{


    public function index()
    {
    ini_set('memory_limit', '-1');
        $data = [
            'sales' => Sale::whereBetween('updated_at',$this->dates)
                             ->with('category','model','sub_category','promoter.merchandiser','outlet','outlet_branch','customer')
                             ->where('active',1)
                             ->orderBy('created_at','desc')
                             ->paginate(40),
            'old_year' => $this->year,
            'old_month' => $this->month,
            'old_start_date' => substr($this->dates[0], 0, 10),
            'old_end_date' => substr($this->dates[1], 0, 10),

        ];
        return view('sales.view',$data);
    }


    public function commissions()
    {
    ini_set('memory_limit', '-1');
        $data = [
                'commissions' => $this->commission_data(),
                'old_year' => $this->year,
                'old_month'=> $this->month
            ];

        return view('sales.commissions',$data);
    }

    public function commission_details()
    {
    ini_set('memory_limit', '-1');
        // return App\Promoter::active()->with(['commission_details'=>function($q) use($week){
        //             $q->where('week',$week);
        //         }])->get();
        $data = [
                'promoters' => $this->promoter_commission_data(),
                'old_year' => $this->year,
                'old_week' => $this->week
            ];
        return view('sales.commission_details',$data);
    }

    public function commissions_csv()
    {
        ini_set('memory_limit', '-1');
        $headers = [
                'Week',
                'Period',
                'Total Sales Value',
                'Commissions Earned',
                'Commissions Paid',
                'Balance'
            ];

        $commissions = $this->commission_data();

        $data = array();

        foreach ($commissions as $key => $commission) {
            $data[] = [
                    $commission->week,
                    date('jS M',strtotime($commission->start_date)) . ' &#47; ' . date('jS M',strtotime($commission->end_date)),
                    number_format($commission->sales_value),
                    number_format($commission->earned_commissions),
                    number_format($commission->paid_commissions),
                    number_format($commission->unpaid_commissions)
                ];
            
        }

        $file_name = $this->year . ' commissions.csv';

        return Sale::CSV($headers,$data,$file_name);
    }

    public function commission_details_csv()
    {
        ini_set('memory_limit', '-1');
        $headers = [
                'Promoter',
                'Total Sales Value',
                'Commissions Earned',
                'Commissions Paid',
                'Balance'
            ];

        $promoters = $this->promoter_commission_data();

        $data = array();

        foreach ($promoters as $key => $promoter) {
            $data[] = [
                    $promoter->code . ': ' .$promoter->first_name . ' ' . $promoter->last_name,
                    (isset($promoter->commission_details[0]))?number_format($promoter->commission_details[0]->sales_value):0,
                    (isset($promoter->commission_details[0]))?number_format($promoter->commission_details[0]->earned_commissions):0,
                    (isset($promoter->commission_details[0]))?number_format($promoter->commission_details[0]->paid_commissions):0,
                    (isset($promoter->commission_details[0]))?number_format($promoter->commission_details[0]->unpaid_commissions):0
                ];
           
        }

        $file_name = $this->year . ' week ' . $this->week . ' commissions.csv';

        return Sale::CSV($headers,$data,$file_name);
    }

    private function promoter_commission_data()
    {
        ini_set('memory_limit', '-1');
        return App\Promoter::active()->with(['commission_details'=>function($q){
                    $q->where('week',$this->week);
                }])->get();
    }

    private function commission_data()
    {
        ini_set('memory_limit', '-1');
        
        return App\Commission::where('year',$this->year)->whereMonth('updated_at','=',$this->month)->get();
    }


    public function export_csv(Request $request)
    {
    ini_set('memory_limit', '-1');
        $csvHeader = [
            'Date',
            'Category',
            'Sub-Category',
            'Model',
            'Quantity',
            'Amount',
            'Commission',
            'Additional Commission',
            'Promoter',
            'Merchandiser',
            'Outlet',
            'Outlet Branch',
            ];

        $sales = Sale::whereBetween('updated_at',$this->dates)
                             ->with('category','model','sub_category','promoter.merchandiser','outlet','outlet_branch','customer')
                             ->where('active',1)
                             ->get();

        $csvData = array();
        foreach ($sales as $key => $sale) {
            $csvData[] = [
                    date('jS M Y', strtotime($sale->created_at)),
                    @$sale->category->title,
                    @$sale->sub_category->title,
                    @$sale->model->title,
                    (@$sale->quantity) ? $sale->quantity : '1',
                    number_format(@$sale->amount),
                    number_format(@$sale->commission),
                    @$sale->additional_commission,
                    (@$sale->promoter)?@$sale->promoter->first_name.' '.@$sale->promoter->last_name : '*',
                    (@$sale->promoter)?@$sale->promoter->merchandiser->first_name.' '.@$sale->promoter->merchandiser->last_name : '*',
                    (@$sale->outlet)?@$sale->outlet->title:'*',
                    (@$sale->sale_outlet_branch())? @$sale->sale_outlet_branch()->title : '*',
                ];
            
        }

        $file_name = $this->dates[0] . ' to ' .$this->dates[1] . ' sales.csv';
        return CSV::create($csvData, $csvHeader)->render($file_name);
    }


    public function delete($id)
    {
        $sale = Sale::findOrFail($id);

        $message = '';

        // update commission balance

        $commission_balance = App\CommissionBalance::where('promoter_code', $sale->promoter_code)->first();

        $commission_balance->commission_balance -= $sale->commission;
        $commission_balance->commission_earned -= $sale->commission;

        $initial_update = $commission_balance->updated_at;

        $commission_balance->save();

        if($initial_update != $commission_balance->updated_at)
            $message .= 'Commission balance updated.';
        else
            $message .= 'Commission balance was <b>NOT</b> updated.';




        // update commission 2016


        $sale_date = explode('-', substr($sale->created_at,0,10));

        $date = new DateTime();

        $date->setDate($sale_date[0],$sale_date[1],$sale_date[2]);


        $commission = App\Commission::where('year',$date->format('Y'))->where('week',$date->format('W'))->first();

        $commission->sales_value -= $sale->amount;
        $commission->earned_commissions -= $sale->commission;
        $commission->unpaid_commissions -= $sale->commission;


        $initial_update = $commission->updated_at;

        $commission->save();

        if($initial_update != $commission->updated_at)
            $message .= '<br>Commission updated.';
        else
            $message .= '<br>Commission was <b>NOT</b> updated.';


        // update commission promoter 2016

        $commission_promoter = App\CommissionDetail::where('year',$date->format('Y'))
                                                    ->where('week',$date->format('W'))
                                                    ->where('promoter_code', $sale->promoter_code)
                                                    ->first();

        $commission_promoter->sales_value -= $sale->amount;
        $commission_promoter->earned_commissions -= $sale->commission;
        $commission_promoter->unpaid_commissions -= $sale->commission;

        
        $initial_update = $commission_promoter->updated_at;
        
        $commission_promoter->save();

        if($initial_update != $commission_promoter->updated_at)
            $message .= '<br>Commission promoter updated.';
        else
            $message .= '<br>Commission promoter was <b>NOT</b> updated.';


        $customer = App\Customer::find($sale->customer_id);

        $customer->active = 0;


        $initial_update = $customer->updated_at;

        $customer->save();

        if($initial_update != $customer->updated_at)
            $message .= '<br>Customer updated.';
        else
            $message .= '<br>Customer was <b>NOT</b> updated.';
        
        $sale->active = 0;

        


        if($sale->save()){
            $response = [
                'status' => 1,
                'message' => $message
                ];
        }else {
            $response = [
                'status' => 0,
                'message' => 'Failed: Sale was not deleted'
                ];
        }

        return json_encode($response);
    }
}
