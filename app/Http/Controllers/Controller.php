<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use DateTime;

use App\Http\Requests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $year;
    protected $month;
    protected $week;
    protected $from;
    protected $to;

    function __construct(Request $request)
    {
        $this->year = (isset($request->year) && $request->year != '' )? $request->year: date('Y');
        $this->month = (isset($request->month) && $request->month != '' )? $request->month: date('n');
        $this->week = (isset($request->week) && $request->week != '')? $request->week: date('W');


        $datetime = new DateTime();
        $datetime->setISODate( $this->year, $this->week, 1 );
        $dates[0] = $datetime->format( "Y-m-d" );
        $datetime->setISODate( $this->year, $this->week, 7 );
        $dates[1] = $datetime->format( "Y-m-d" );
            
        $this->from = $dates[0];
        $this->to = $dates[1];
    }

}
