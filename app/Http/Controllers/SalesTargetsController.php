<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;
use App\SalesTarget;
use App\Promoter;

use App\Http\Requests;

# BaseController is inherited to initialze dates

class SalesTargetsController extends BaseController
{

    public function index()
    {

    	$data = ['promoters'=>Promoter::with('outlet_branch.outlet','merchandiser')
    							->with(['sales_targets'=>function($q){
							    		$q->where('year',$this->year)->where('month',$this->month)->orderBy('created_at');
							    	}])
    							->where('active',1)
    							->get(),

    			 'old_year' => $this->year,
    			 'old_month' => $this->month,
    			 'dates' => $this->dates,
    		];
        // dd($this->dates);
    	return view('sales.target',$data);
    }

    public function set_targets(Request $request)
    {
    	// return json_encode($request->all());
    	$promoters = Promoter::where('active',1)->with('outlet_branch.outlet')->get();

		$data = array();

        $count = 0;
    	
    	foreach ($promoters as $key => $promoter) {
    		// echo $request->input('target.'.$promoter->id) . ': ' . $promoter->id . '<br>';
    		$data = [
    			'month' => $request->month,
    			'year' => $request->year,
    			'promoter_code' => $promoter->code
    		];
    		$target = SalesTarget::firstOrNew($data);

    		$target->sales_target = $request->input('target.'.$promoter->id);
    		$target->outlet_code = ($promoter->outlet_branch)? $promoter->outlet_branch->outlet->code : 0 ;
    		$target->outlet_branch_code = ($promoter->outlet_branch)? $promoter->outlet_branch->code : 0 ;

    		$data[] = $target;

            $last_update = $target->updated_at;

            $target->save();

            if($target->updated_at != $last_update)
                $count++;
    	}

        $response = [
                'status' => 1,
                'message' => $count . ' records updated.',
                'url' => '/sales/targets?year=' . $this->year . '&month=' . $this->month
            ];

    	return json_encode($response);
    }
}
