<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;
use App\Promoter;
use App\Merchandiser;
use App\PromoterCheck;
use App\MerchandiserCheck;
use CSV;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use DateTime;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class ChecksController extends BaseController
{

	// protected $date;

	// function __construct(Request $request)
	// {
 //        // parent::__construct();
	// 	$this->date = (isset($request->date) && $request->date != '')? $request->date: date('Y-m-d');
	// }
    public function merchandiser(Request $request)
    {
    	

        $data = [
            'checks' => $this->merchandiser_data($request),
            'sub_title' => 'Merchandisers',
            'old_start_date' => substr($this->dates[0], 0, 10),
            'old_end_date' => substr($this->dates[1], 0, 10),
            ];

    	// return json_encode($merchandisers);
    	return view('checks.merchandiser_view',$data);
    }

    public function merchandiser_show(Request $request, $code)
    {   
        $input = $request->all();
        //check if dates exist
        $input['start_date'] = (array_key_exists('start_date', $input)) ? $input['start_date'] : date("Y-m-d");
        $input['end_date'] = (array_key_exists('end_date', $input)) ? $input['end_date'] : date("Y-m-d");

    	$checks = $this->merchandiser_show_data($code,$input);
        $checks = $this->paginate_results($request, $checks);

        $data = [
            'checks' => $checks,
            'sub_title' => 'Merchandiser',
            'start_date' => $input['start_date'],
            'end_date' => $input['end_date']
            ];
    	return view('checks.show',$data);

    }


    public function promoter(Request $request)
    {
        
        $input = $request->all();
        //check if dates exist
        $input['start_date'] = (array_key_exists('start_date', $input)) ? $input['start_date'] : date("Y-m-d");
        $input['end_date'] = (array_key_exists('end_date', $input)) ? $input['end_date'] : date("Y-m-d");
        $check_results = $this->promoter_data($input);
        $checks = $check_results['checks'];
        $limit = $check_results['count'];
        $defaults['limit'] = $limit;



        $checks = $this->paginate_results($request, $checks,$defaults);
        $data = [
            'checks'=>$checks,
            'sub_title' => 'Promoters',
            'start_date' => $input['start_date'],
            'end_date' => $input['end_date']
            ];

        // return json_encode($promoters);
        return view('checks.view',$data);
    }

    private function promoter_data($input)
    {   

        $promoter_count= Promoter::active()->with('outlet')->get()->count();
        $checks = [];
                // loop to get  those without set date
        $begin = new DateTime( $input['start_date'] );
        $end   = new DateTime( $input['end_date'] );

        $current_pin = 0;

        for($i = $begin; $i <= $end; $i->modify('+1 day')){

            $promoters = Promoter::active()->with(['checks'=>function($q) use ($i){
                $q->where('date',$i->format("Y-m-d"));
            }])->with('outlet')->get();

            foreach ($promoters as $key => $m) {
                foreach ($m->checks as $key => $check) {

                    if ($check->created_at->format('Y-m-d') == $i->format("Y-m-d")) {
                        if(empty($check->location)){
                            $fullurl = "http://maps.googleapis.com/maps/api/geocode/json?latlng=".$check->latitude.",".$check->longitude."&sensor=false%22";
                            $string = file_get_contents($fullurl); // get json content
                            $json_a = json_decode($string, true); //json decoder

                            if(isset($json_a['results'][0])){
                                $address = $json_a['results'][0]['formatted_address']; // get name from json
                                $check->location = $address;
                                $check->save();
                            }
                        }

                        if ($check->check_type_id == 1) {
                            $m['checkin'] = $check;
                        } else {
                            $m['checkout'] = $check;

                        }
                    }
                    
                }
                $m->check_date = $i->format("Y-m-d");
                array_push($checks, $m);

            }
        }
        $results['checks'] = $checks;
        $results['count'] = $promoter_count;

        return $results;

        // $promoter_keys = [];

        // $promoters = Promoter::active()->with(['checks'=>function($q) use ($input){
        //     $q->whereBetween('date',[$input['start_date'],$input['end_date']]);
        // }])->with('outlet')->get();

        // $promoter_count = $promoters->count();
        // $checks = [];
        // $checkins = array();
        // $checkouts = array();

        // foreach ($promoters as $key => $m) {
        //     foreach ($m->checks as $key => $check) {
        //         if(empty($check->location)){
        //             $fullurl = "http://maps.googleapis.com/maps/api/geocode/json?latlng=".$check->latitude.",".$check->longitude."&sensor=false%22";
        //             $string = file_get_contents($fullurl); // get json content
        //             $json_a = json_decode($string, true); //json decoder

        //             if(isset($json_a['results'][0])){
        //                 $address = $json_a['results'][0]['formatted_address']; // get name from json
        //                 $check->location = $address;
        //                 $check->save();
        //             }
        //         }

        //         if ($check->check_type_id == 1) {
        //             $person = $check->person;
        //             $person['checkin'] = $check;
        //             $checkout = PromoterCheck::where('check_type_id',2)->where('promoter_code',$m->code)->where('date',$check->date)->first();
        //             $person['checkout'] = $checkout;
        //             array_push($checks, $person);

        //             //to be used as a reference to insert non checked in users 
        //             if (array_key_exists($check->date, $promoter_keys)) {
        //                 $promoter_keys[$check->date] = $promoter_keys[$check->date] + 1;
        //             }
        //             else{
        //                 $promoter_keys[$check->date] = 1 ;
        //             }
        //         }
        //     }
        // }
        // $checks = array_values(array_sort($checks, function ($value) {
        //     return $value['checkin']->date;
        // }));

        // // loop to get  those without set date
        // $begin = new DateTime( $input['start_date'] );
        // $end   = new DateTime( $input['end_date'] );
        // $current_pin = 0;

        // for($i = $begin; $i <= $end; $i->modify('+1 day')){
        //     if (array_key_exists($i->format("Y-m-d"), $promoter_keys)) {
        //         $promoter_no = $promoter_keys[$i->format("Y-m-d")];
        //         $current_pin = $current_pin + $promoter_no;
        //     }
        //     else{
        //         $promoter_no = 0;
        //     }
            
        //     if ($promoter_no == 0 && count($checks) == 0) {
        //         $checks = Promoter::active()->with(['checks'=>function($q) use ($input,$i){
        //             $q->whereNotBetween('date',[$i->format("Y-m-d"),$i->format("Y-m-d")]);
        //         }])->with('outlet')->get();

        //         foreach ($checks as $key => $value) {
        //             $value['checkin'] = '';
        //             $value['checkout'] = '';
        //             $current_pin = $current_pin + 1; 
        //         }
                
        //     }

        //     else{

        //         $current_pin = $current_pin + $promoter_no - 1;
        //         $unset_promoters = Promoter::active()->with(['checks'=>function($q) use ($i){
        //             $q->whereNotBetween('date',[$i->format("Y-m-d"),$i->format("Y-m-d")]);
        //         }])->with('outlet')->get();
        //         foreach ($unset_promoters as $key => $value) {
        //             $value['checkin'] = '';
        //             $value['checkout'] = '';   
        //             $new_key = $current_pin + 1;
        //             $checks = $this->array_insert_after($current_pin, $checks, $new_key, $value); 
        //             $current_pin = $new_key;
        //         }

        //     }

        // }
        // $results['checks'] = $checks;
        // $results['count'] = $promoter_count;
        // return $results;
    }

    private function merchandiser_data($request)
    {   

        if ((!isset($request->start_date) || $request->start_date == '') && (!isset($request->end_date) || $request->end_date == '')) {
            $this->dates[0] = date('Y-m-d') . ' 00:00:00';
            $this->dates[1] = date('Y-m-d') . ' 23:59:59';
        }


        $merchandisers = Merchandiser::active()->with(['checks'=>function($q){
            $q->whereBetween('date',$this->dates);
        }])->with('outlet')->get();
        

        foreach ($merchandisers as $key => $m) {
            $checkins = array();
            $checkouts = array();

            foreach ($m->checks as $key => $check) {
                if(empty($check->location)){
                    $fullurl = "http://maps.googleapis.com/maps/api/geocode/json?latlng=".$check->latitude.",".$check->longitude."&sensor=false%22";
                    $string = file_get_contents($fullurl); // get json content
                    $json_a = json_decode($string, true); //json decoder

                    if(isset($json_a['results'][0])){
                        $address = $json_a['results'][0]['formatted_address']; // get name from json
                        $check->location = $address;
                        $check->save();
                    }
                }
                if ($check->check_type_id == 1) {
                    $checkins[] = $check;
                } else {
                    $checkouts[] = $check;

                }
            }

            $m['checkin'] = $checkins;
            $m['checkout'] = $checkouts;
        }

        return $merchandisers;
    }
    private function merchandiser_checkin_data($input)
    {   

        $promoters = Merchandiser::active()->with(['checks'=>function($q) use ($input){
            $q->whereBetween('date',[$input['start_date'],$input['end_date']]);
        }])->with('outlet')->get();
        $promoter_count = $promoters->count();
        $checks = [];
        $checkins = array();
        $checkouts = array();

        foreach ($promoters as $key => $m) {
            foreach ($m->checks as $key => $check) {

                if(empty($check->location)){
                    $fullurl = "http://maps.googleapis.com/maps/api/geocode/json?latlng=".$check->latitude.",".$check->longitude."&sensor=false%22";
                    $string = file_get_contents($fullurl); // get json content
                    $json_a = json_decode($string, true); //json decoder

                    if(isset($json_a['results'][0])){
                        $address = $json_a['results'][0]['formatted_address']; // get name from json
                        $check->location = $address;
                        $check->save();
                    }
                }

                if ($check->check_type_id == 1) {
                    $person = $check->person;
                    $person['checkin'] = $check;
                    $checkout = MerchandiserCheck::where('check_type_id',2)->where('merchandiser_code',$m->code)->where('date',$check->date)->first();
                    $person['checkout'] = $checkout;
                    array_push($checks, $person);
                }
            }
        }
        $checks = array_values(array_sort($checks, function ($value) {
            return $value['checkin']->date;
        }));
        $results['checks'] = $checks;
        $results['count'] = $promoter_count;
        return $results;
    }

    private function merchandiser_show_data($code,$input)
    {   
        $checks = [];
                // loop to get  those without set date
        $begin = new DateTime( $input['start_date'] );
        $end   = new DateTime( $input['end_date'] );
        for($i = $begin; $i <= $end; $i->modify('+1 day')){

            $promoters = Merchandiser::where('code',$code)->with(['checks'=>function($q) use ($i){
                $q->where('date',$i->format("Y-m-d"));
            }])->get();

            foreach ($promoters as $key => $m) {
                foreach ($m->checks as $key => $check) {
                    $person = Merchandiser::where('code',$code)->first();
                    if(empty($check->location)){
                        $fullurl = "http://maps.googleapis.com/maps/api/geocode/json?latlng=".$check->latitude.",".$check->longitude."&sensor=false%22";
                        $string = file_get_contents($fullurl); // get json content
                        $json_a = json_decode($string, true); //json decoder

                        if(isset($json_a['results'][0])){
                            $address = $json_a['results'][0]['formatted_address']; // get name from json
                            $check->location = $address;
                            $check->save();
                        }
                    }

                    if ($check->check_type_id == 1) {
                        $person['checkin'] = $check;
                        array_push($checks, $person);
                    } else {
                        $last_key = end($checks);
                        $last_key['checkout'] = $check;

                    }
                }
                if ($m->checks->count() == 0) {
                    array_push($checks, $m);
                }
                $m->check_date = $i->format("Y-m-d");
                

            }
        }

        return $checks;
    }


    public function promoter_show(Request $request, $code)
    {
        
        $input = $request->all();
        //check if dates exist
        $input['start_date'] = (array_key_exists('start_date', $input)) ? $input['start_date'] : date("Y-m-d");
        $input['end_date'] = (array_key_exists('end_date', $input)) ? $input['end_date'] : date("Y-m-d");
        $checks = $this->promoter_show_data($code,$input);
        $checks = $this->paginate_results($request, $checks);

        $data = [
                'checks' => $checks,
                'sub_title' => 'Promoter',
                'start_date'=> $input['start_date'],
                'end_date'=> $input['end_date']
            ];

        return view('checks.show',$data);
        return json_encode($checkins);

    }

    private function promoter_show_data($code,$input)
    {
        
        
        $checks = [];
                // loop to get  those without set date
        $begin = new DateTime( $input['start_date'] );
        $end   = new DateTime( $input['end_date'] );
        for($i = $begin; $i <= $end; $i->modify('+1 day')){

            $promoters = Promoter::where('code',$code)->with(['checks'=>function($q) use ($i){
                $q->where('date',$i->format("Y-m-d"));
            }])->with('outlet')->get();

            foreach ($promoters as $key => $m) {
                foreach ($m->checks as $key => $check) {
                    if(empty($check->location)){
                        $fullurl = "http://maps.googleapis.com/maps/api/geocode/json?latlng=".$check->latitude.",".$check->longitude."&sensor=false%22";
                        $string = file_get_contents($fullurl); // get json content
                        $json_a = json_decode($string, true); //json decoder

                        if(isset($json_a['results'][0])){
                            $address = $json_a['results'][0]['formatted_address']; // get name from json
                            $check->location = $address;
                            $check->save();
                        }
                    }

                    if ($check->check_type_id == 1) {
                        $m['checkin'] = $check;
                    } else {
                        $m['checkout'] = $check;

                    }
                }
                $m->check_date = $i->format("Y-m-d");
                array_push($checks, $m);

            }
        }

        return $checks;
    }



    public function export_csv(Request $request, $person)
    {   
        $input = $request->all();
        //check if dates exist
        $input['start_date'] = (array_key_exists('start_date', $input)) ? $input['start_date'] : date("Y-m-d");
        $input['end_date'] = (array_key_exists('end_date', $input)) ? $input['end_date'] : date("Y-m-d");

        if ($person === 'merchandisers') {
            $data = $this->merchandiser_checkin_data($input);
            return Merchandiser::check_csv($data,'Merchandiser',$this->date);
        }

        if ($person === 'promoters') {
            $data = $this->promoter_data($input);
            $data = $data['checks'];

            return Promoter::check_csv($data,'Promoter',$this->date);
        }

        return show_404();
    }

    public function promoter_csv(Request $request, $code)
    {   
        $input = $request->all();
        //check if dates exist
        $input['start_date'] = (array_key_exists('start_date', $input)) ? $input['start_date'] : date("Y-m-d");
        $input['end_date'] = (array_key_exists('end_date', $input)) ? $input['end_date'] : date("Y-m-d");

        return Promoter::person_checks_csv($code, $this->promoter_show_data($code,$input));
    }

    public function merchandiser_csv(Request $request, $code)
    {
        $input = $request->all();
        //check if dates exist
        $input['start_date'] = (array_key_exists('start_date', $input)) ? $input['start_date'] : date("Y-m-d");
        $input['end_date'] = (array_key_exists('end_date', $input)) ? $input['end_date'] : date("Y-m-d");

        return Merchandiser::person_checks_csv($code, $this->merchandiser_show_data($code,$input));
    }

    /////////////////////////////////
    /////////////////////////////////

    /**
     * @param Request $request
     * @param         $results
     * @param array   $defaults
     *
     * @return array|Paginator
     */
    public function paginate_results(Request $request, $results, $defaults = [ ])
    {
        $results_data = [ ];
        $paginated_data = [ ];

        $limit = ( Input::has('limit') ) ? Input::get('limit')
            : array_get($defaults, 'limit', 10);

        if ($limit != -1) {
            $offset = ( Input::has('page') ) ? ( Input::get('page') - 1 ) * $limit : 0;
        }

        $page = ( Input::has('page') ) ? Input::get('page')
            : array_get($defaults, 'page', 1);


        foreach ($results as $result) {
            $results_data [] = $result;
        }
        $itemsForCurrentPage = ( $limit != -1 ) ? array_slice($results_data, $offset, $limit, true) : $results_data;

        $paginated_data = ( $limit != -1 ) ? new Paginator($itemsForCurrentPage, count($results), $limit, $page, [
            'path'  => $request->url(),
            'query' => $request->query(),
        ]) : $results;

        return $paginated_data;
    }
        /*
     * Inserts a new key/value after the key in the array.
     *
     * @param $key
     *   The key to insert after.
     * @param $array
     *   An array to insert in to.
     * @param $new_key
     *   The key to insert.
     * @param $new_value
     *   An value to insert.
     *
     * @return
     *   The new array if the key exists, FALSE otherwise.
     *
     * @see array_insert_before()
     */
    public function array_insert_after($key, $array, $new_key, $new_value) {
        $new_checks = [];
        foreach ($array as $k => $value) {
            array_push($new_checks, $value);
            if ($k == $key) {
                $new_checks[$new_key] = $new_value;
            }
        }
        return $new_checks;
    }
}
