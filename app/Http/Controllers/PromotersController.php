<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;
use App\Promoter;

use CSV;
use Mail;
use Validator;

use App\Http\Requests;

class PromotersController extends Controller
{

    # display all the resources
    public function index()
    {
    	$data = [
    		'promoters' => Promoter::paginate(15),
    		'banks' => App\Bank::get(),
            'merchandisers' => App\Merchandiser::where('active',1)->get(),
    		'outlets' => App\Outlet::where('active',1)->get()
    		];

    	return view('promoters.view',$data);
    }

    # show a specific resource

    public function show($id)
    {
        $promoter = Promoter::where('id',$id)->with('bank','bank_branch','outlet','outlet_branch','merchandiser')->firstorFail();
        return view('promoters.show', ['promoter' => $promoter]);
    }

    # add a new resource

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),$this->rules());

        if ($validator->fails()) {
            return json_encode(['status'=>0,'errors'=>$validator->errors()]);
        }

        $promoter = new Promoter;

        $promoter->code = $promoter->generate_code();
        $promoter->first_name = $request->first_name;
        $promoter->last_name = $request->last_name;
        $promoter->id_number = $request->id_number;
        $promoter->email = $request->email;
        $promoter->phone = $request->phone;
        $promoter->mobile_money = $request->mobile_money;
        $promoter->promoter_type_id = $request->promoter_type_id;
        $promoter->salary = $request->salary;
        $promoter->salary_level = $request->salary_level;
        $promoter->merchandiser_code = $request->merchandiser_code;
        $promoter->bank_code = $request->bank_code;
        $promoter->bank_branch_code = $request->bank_branch_code;
        $promoter->outlet_code = $request->outlet_code;
        $promoter->outlet_branch_code = $request->outlet_branch_code;
        $promoter->account_no = $request->account_no;
        $promoter->active = 1;
        $promoter->password = md5($promoter->id_number);

        $promoter->save();

        $data = [
            'm' => $promoter,
            ];


        Mail::send('mail.staff_registration', $data, function ($mail) use ($promoter) {


            $mail->from('lgsudan@quantiumservices.com', 'LG Incentives Portal');
            $mail->to($promoter->email, $promoter->first_name . ' ' . $promoter->last_name);
            $mail->subject('LG Merchant System Registration code');
        
            
        });

        return json_encode([
                'status' => 1,
                'message' => 'Promoter saved succesfully.',
                'url' => 'staff/promoters'
            ]);

        return json_encode($promoter);
    }


    public function edit($id)
    {
        return Promoter::findOrFail($id);
    }

    /*
    
    update the resourse

    */

    public function update(Request $request)
    {
        $promoter = Promoter::findOrFail($request->id);

        $validator = Validator::make($request->all(),$this->rules($request->id));

        if ($validator->fails()) {
            return json_encode(['status' => 0, 'errors' => $validator->errors()]);
        }

        $promoter->first_name = $request->first_name;
        $promoter->last_name = $request->last_name;
        $promoter->id_number = $request->id_number;
        $promoter->email = $request->email;
        $promoter->phone = $request->phone;
        $promoter->mobile_money = $request->mobile_money;
        $promoter->promoter_type_id = $request->promoter_type_id;
        $promoter->salary = $request->salary;
        $promoter->salary_level = $request->salary_level;
        $promoter->merchandiser_code = $request->merchandiser_code;
        $promoter->bank_code = $request->bank_code;
        $promoter->bank_branch_code = $request->bank_branch_code;
        $promoter->outlet_code = $request->outlet_code;
        $promoter->outlet_branch_code = $request->outlet_branch_code;
        $promoter->account_no = $request->account_no;

        $promoter->save();

        return json_encode([
                'status' => 1,
                'message' => 'Promoter updated succesfully.',
                'url' => 'staff/promoters'
            ]);


    }


    public function activate($id)
    {
    	$response = Promoter::toggle_active($id);

    	return json_encode($response);
    }

    public static function delete($id)
    {
    	$model = Promoter::findOrFail($id);

        if($model->delete()) {
            $response = [
                'status' => 1,
                'message' => 'Promoter delete succesfully.'
            ];
        } else {
            $response = [
                'status' => 0,
                'message' => 'Failed: Promoter was not deleted. Please try again.'
            ];
        }

        return json_encode($response);
    }


    public static function csv()
    {

        $file_name = 'promoters.csv';

        $csvHeader = [
            'Code',
            'Name',
            'ID Number',
            'Phone',
            'Mobile Money',
            'Email',
            'Salary',
            'Account Number',
            'Merchandiser',
            'Outlet',
            'Outlet Branch',
            'Bank',
            'Bank Branch',
            ];

        $csvData = array();

        $promoters = Promoter::active()->with('bank','bank_branch','outlet','outlet_branch')->get();

        foreach ($promoters as $key => $promoter) {
            $csvData[] = [
                    @$promoter->code,
                    @$promoter->first_name . ' ' . @$promoter->last_name,
                    @$promoter->id_number,
                    @$promoter->phone,
                    @$promoter->mobile_money,
                    @$promoter->email,
                    @$promoter->salary,
                    @$promoter->account_no,
                    @$promoter->merchandiser->first_name. ' ' .@$promoter->merchandiser->last_name,
                    @$promoter->outlet->title,
                    (@$promoter->sale_outlet_branch()) ? $promoter->sale_outlet_branch()->title : ' ',
                    @$promoter->bank->title,
                    @$promoter->bank_branch->title,
                ];
            
        }

        return Promoter::CSV($csvHeader,$csvData,$file_name);
    }


    private function rules($id=null)
    {
        return [
                'first_name' => 'required',
                'last_name' => 'required',
                'merchandiser_code' => 'required',
                'id_number' => 'required|numeric',
                'email' => 'required|email|unique:lg_promoters,email,'.$id,
                'phone' => 'required|numeric|unique:lg_promoters,phone,'.$id,
                'mobile_money' => 'required|numeric',
                'salary' => 'required|numeric',
                'outlet_code' => 'required',
                'outlet_branch_code' => 'required',
                'promoter_type_id' => 'required|numeric',
                'salary_level' => 'required|numeric',
                'merchandiser_code' => 'required',
                'bank_code' => 'required',
                'bank_branch_code' => 'required',
                'account_no' => 'required',
            ];
    }


    public function clear_imei($id)
    {
        $promoter = Promoter::findOrFail($id);

        $promoter->imei = '';

        $promoter->save();

        return json_encode([
            'status' => 1,
            'message' => 'imei cleared'
            ]);
    }

    
}
