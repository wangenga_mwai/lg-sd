<?php

namespace App\Http\Controllers;

use Auth;
use Mail;


use Illuminate\Http\Request;

use App\Http\Requests;

use Hash;
use Validator;

use App\User;

class UsersController extends Controller
{
    public function login()
    {
    	return view('users.login');
    }

    public function authenticate(Request $request)
    {
    	if(Auth::attempt(['username'=>$request->username, 'password' => $request->password, 'active' => 1])){
    		$response = [
    				'status' => 'ok',
    				'message' => 'Login successful. Redirecting...',
    				'url' => session('url.intended')
    			];
    	} else {
    		$response = [
    				'status' => 'error',
    				'message' => 'Username or password is incorrect.'
    			];
    	}
        return ($request->ajax())? json_encode($response): redirect('/');
    	return json_encode($response);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
                'register_username' => 'required|min:6|unique:lg_users,username',
                'register_email' => 'required|unique:lg_users,email',
                'register_password' => 'required|confirmed|min:6',
                'register_password_confirmation' => 'required',
            ]);

        if ($validator->fails()) {
            return json_encode(['status' => 'fail', 'errors' => $validator->errors()]);
        }

        $user = new User;

        $user->username = $request->register_username;
        $user->email = $request->register_email;
        $user->password = Hash::make($request->register_password);
        $user->active = 0;

        $user->save();

        return json_encode([
                'status' => 'ok',
                'message' => 'You have successfullly registered. Wait for confirmation.'
            ]);
    }



    public function store_by_admin(Request $request)
    {
        $validator = Validator::make($request->all(),[
                'username' => 'required|min:6|unique:lg_users,username',
                'email' => 'required|unique:lg_users,email',
            ]);

        if ($validator->fails()) {
            return json_encode(['status' => 'fail', 'errors' => $validator->errors()]);
        }

        // return response()->json($request->all());

        $user = new User;

        $pass = str_random(8);

        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($pass);
        $user->admin = ($request->has('admin'))? 1: 0;
        $user->approver = ($request->has('approver'))? 1: 0;
        $user->fsm_approver = ($request->has('fsm_approver'))? 1: 0;
        $user->active = 1;

        if($user->save()){


            Mail::send('mail.register', ['password' => $pass, 'user' => $user], function($mail) use ($user){
                $mail->from('info@quantiumservises.com', 'Quantium Servises');

                $mail->to($user->email, $user->username);

                $mail->subject('LG user registration');
            });

            $response = [
                'status' => 1,
                'message' => 'You have successfullly registered a user an email will be sent to the email address.',
                'url' => '/users'
            ];
        } else {
            $response = [
                'status' => 0,
                'message' => 'Registration failed. Please try again.'
            ];
        }

        return json_encode($response);
    }



    public function logout()
    {
    	Auth::logout();
    	return redirect('/login');
    }


    public function user_permissions()
    {
        $data = [
            'users' => User::whereNotIn('id', [11])->get()
        ];


        return view('users.permissions', $data);
    }


    public function user_set_permission(Request $request)
    {
        $user = User::findOrFail($request->id);

        $user->{$request->type} = $request->value;


        if($user->save()){
            return response()->json(['status' => 1 , 'message' => 'User permission set.']);
        } else {
            return response()->json(['status' => 0 , 'message' => 'Failed: User permission was not set.']);
        }
    }




    public function show_change_password()
    {
        return view('users.change-password');
    }



    public function change_password(Request $request )
    {
        $validator = Validator::make($request->all(), [
                'email' => 'required',
                'old_password' => 'required',
                'password' => 'required|min:6',
                'password_confirm' => 'password_confirmation',
            ]);

        if ($validator->fails()) {
            return ($request->ajax() )?
                    json_encode(['status' => 0, 'errors' => $validator->errors()]):
                    back()->withErrors($validator)->withInput();
        }

        if(Auth::attempt(['email' => $request->email, 'password' => $request->old_password])){
            $user = Auth::user();

            $user->password = Hash::make($request->password);

            if($user->save()){
                $response = [
                    'status' => 1,
                    'message' => 'You have successfully changed your password.',
                    'url' => '/users'
                ];
            } else {
                $response = [
                    'status' => 0,
                    'errors' => ['Failed. Your password was not changed.']
                ];
            }
        } else {
            $response = [
                'status' => 0,
                'errors' => ['Email or password is incorrect!']
            ];
        }

        return json_encode($response);
    }
}
