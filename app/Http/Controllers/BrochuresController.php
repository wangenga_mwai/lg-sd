<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;


use App\Brochure;

class BrochuresController extends BaseController
{
    public function index()
    {
    	$data = [
    		'brochures' => $this->data(),
    		'old_start_date' => substr($this->dates[0], 0, 10),
            'old_end_date' => substr($this->dates[1], 0, 10),
    	];

    	return view('brochure.view', $data);
    }



    private function data()
    {
        return Brochure::whereBetween('created_at', $this->dates)
                    ->with('promoter', 'model.sub_category', 'model.category')
                    ->has('model')
                    ->get();
    }


    public function csv()
    {
        $headers = [
                'Date',
                'Promoter Code',
                'Promoter Name',
                'Email',
                'Category',
                'Sub-Category',
                'Model'
            ];

        $data = array();

        foreach ($this->data() as $key => $brochure) {
            $data[] = [
                    date('Y-m-d', strtotime($brochure->created_at)),
                    $brochure->promoter_code,
                    $brochure->promoter ? $brochure->promoter->first_name . ' ' . $brochure->promoter->last_name: '*',
                    $brochure->email,
                    $brochure->model->category ? $brochure->model->category->title: '*',
                    $brochure->model->sub_category ? $brochure->model->sub_category->title: '*',
                    $brochure->model ? $brochure->model->title: '*'
                ];
            
        }

        $file_name = 'brochure.csv';

        return Brochure::CSV($headers,$data,$file_name);
    }

    
}
