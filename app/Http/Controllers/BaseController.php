<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class BaseController extends Controller
{
    protected $year;
	protected $month;
	protected $week;
	protected $dates;
	protected $date;

	function __construct(Request $request)
	{
		$this->year = (isset($request->year) && $request->year != '')? $request->year: date('Y');
		$this->month = (isset($request->month) && $request->month != '')? $request->month: date('n');
		$this->week = (isset($request->week) && $request->week != '')? $request->week: date('W');
		$this->date = (isset($request->date) && $request->date != '')? $request->date: date('Y-m-d');
		// var_dump($request->month);


		$this->dates = [date('Y-m-d H:m:i',mktime(00, 00, 00, $this->month, 1, $this->year)),
	    		  		date('Y-m-t H:m:i',mktime(23, 59, 59, $this->month, 1, $this->year))];

	    if (isset($request->start_date) && $request->start_date != '') {
	    	$this->dates[0] = $request->start_date . ' 00:00:00';
	    }

	    if (isset($request->end_date) && $request->end_date != '') {
	    	$this->dates[1] = $request->end_date . ' 23:59:59';
	    }
	    


	}
}
