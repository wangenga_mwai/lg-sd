<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;
use App\Issue;
use App\PromoterLeave;
use App\MerchandiserLeave;
use App\PromoterLeaveRequest;
use App\MerchandiserLeaveRequest;
use App\PromoterLeaveProcessed;

use DB;
use Mail;
use Validator;

use App\Http\Requests;

class ManagementController extends Controller
{
    public function issues()
    {
    	return view('management.issues',['issues' => Issue::with('promoter')->orderBy('active')->orderBy('created_at')->get()]);
    }


    public function leave_merchandisers()
    {

    	
    	return view('management.leave',[
                'sub_title' => 'Merchandisers',
	    		'submenu' => 'Leave',
	    		'leaves' => $this->leave_merchandisers_data(),
    		]);
    }

    private function leave_merchandisers_data()
    {
        $leaves = MerchandiserLeave::where('year',$this->year)->with('person')->orderBy('merchandiser_code')->get();

        $data = array();
        foreach ($leaves as $key => $leave) {

            $leave['used_days'] = MerchandiserLeaveRequest::used_days($leave->year,$leave->merchandiser_code);
            $data[] = $leave;
        }

        return $data;
    }

    public function leave_promoters()
    {
    	DB::listen(function($query) {
            // echo $query->sql; echo ' - ';
            // $query->bindings
            // echo $query->time; echo '<br>';
        });

    	
    	return view('management.leave',[
	    		'sub_title' => 'Promoters',
                'submenu' => 'Leave',
	    		'leaves' => $this->leave_promoters_data(),
    		]);
    }

    private function leave_promoters_data()
    {
        $leaves = PromoterLeave::where('year',$this->year)->with('person')->orderBy('promoter_code')->get();

        $data = array();
        foreach ($leaves as $key => $leave) {
            
            $leave['used_days'] = PromoterLeaveProcessed::used_days($leave->year,$leave->promoter_code);
            $data[] = $leave;
        }
        return $data;
    }

    public function merchandisers_request(Request $request)
    {

        if (isset($request->leave) && $request->leave == '-1') {
            return redirect('leave/merchandisers');
        }
        $data = [
                'sub_title' => 'Merchandisers',
            ];
        if(isset($request->leave) && $request->leave != '0' ){


            $data['submenu'] = ($request->leave == '1')? 'Approved': 'Disapproved';
            $data['requests'] = MerchandiserLeaveRequest::where('status', $request->leave)->with('person','type')->get();
            
            return view('management.processed_leave',$data);
        }

        $data['submenu'] = 'Request';

        $data['requests'] = MerchandiserLeaveRequest::where('status', 0)->with('person','type')->get();

        return view('management.request',$data);
    }

    public function merchandisers_request_approve($id)
    {
        $data['request'] = MerchandiserLeaveRequest::where('status', 0)->where('id',$id)->with('person','type')->firstorFail();
        //set reason to title if no reason is set
        if(empty($data['request']->reason)){
            $data['request']->reason = $data['request']->type->title;
            $data['request']->save();
        }
        $data['sub_title'] = 'Merchandiser';
        $data['submenu'] = 'Merchandiser';

        return view('management.approve_form',$data);
    }

    public function approve_merchandiser(Request $request)
    {
        // return json_encode($request->all());
        $validator = Validator::make($request->all(),$this->rules());

        if($validator->fails()){
            return json_encode(['status'=>0,'errors'=>$validator->errors()]);
        }

        $leave = MerchandiserLeaveRequest::findOrfail($request->id);

        if($request->status === 'approve')
            $status = 1;
        elseif ($request->status === 'disapprove')
            $status = 2;
        else
            $status = 0;

        $leave->status = $status;
        $leave->reason = $request->reason;

        $leave->save();

        if( $leave->leave_type_id == 1 ){
            $d = App\MerchandiserLeave::where('merchandiser_code',$leave->merchandiser_code)->where('year',$this->year)->first();


        }

        $this->send_leave_response($leave);

        

        $response = [
                'status' => 1,
                'message' => 'Merchandiser request ' . $request->status . 'd',
                'url' => '/leave-request/merchandisers?leave=0'
            ];

        return json_encode($response);

    }

    private function send_leave_response($leave)
    {
        $person = $leave->person;

        $data = [
                'leave' => $leave,
                'person' => $person
            ];

        try {

            Mail::send('mail.staff_leave_response', $data, function ($mail) use ($person) {
                $mail->from(env('MAIL_USERNAME'), 'LG Merchants System');
                $mail->to($person->email, $person->first_name . ' ' . $person->last_name);
                $mail->subject('LG Leave request'); 
            });
            
        } catch (\Exception $e) {
            
        }


    }


    public function promoters_request(Request $request)
    {

        if (isset($request->leave) && $request->leave == '-1') {
            return redirect('leave/promoters');
        }
        $data = [
                'sub_title' => 'Promoters',
            ];
        if(isset($request->leave) && $request->leave != '0' ){


            $data['submenu'] = ($request->leave == '1')? 'Approved': 'Disapproved';
            $data['requests'] = PromoterLeaveProcessed::where('status', $request->leave)->with('person','type')->get();
            
            return view('management.processed_leave',$data);
        }

        $data['submenu'] = 'Request';

        $data['requests'] = PromoterLeaveRequest::where('status', 0)->with('person','type')->get();

        return view('management.request',$data);
    }

    public function promoters_request_approve($id)
    {
        $data['request'] = PromoterLeaveRequest::where('status', 0)->where('id',$id)->with('person','type')->firstorFail();

        //set reason to title if no reason is set
        if(empty($data['request']->reason)){
            $data['request']->reason = $data['request']->type->title;
            $data['request']->save();
        }
        $data['sub_title'] = 'Promoter';
        $data['submenu'] = 'Promoter';

        return view('management.approve_form',$data);
    }

    public function approve_promoter(Request $request)
    {
        // return json_encode($request->all());
        $validator = Validator::make($request->all(),$this->rules());

        if($validator->fails()){
            return json_encode(['status'=>0,'errors'=>$validator->errors()]);
        }

        $leave = PromoterLeaveRequest::findOrfail($request->id);

        if($request->status === 'approve'){
            $result = $leave->toArray();
            unset($result['id']);
            $leave->delete();
            $leave = PromoterLeaveProcessed::create($result);
            $status = 1;
        }
            
        elseif ($request->status === 'disapprove'){
            $result = $leave->toArray();
            unset($result['id']);
            $leave->delete();
            $leave = PromoterLeaveProcessed::create($result);
            $status = 2;
        }
        else{
            $status = 0;
        }

        $leave->status = $status;
        $leave->reason = $request->reason;

        $leave->save();
        try{
            $this->send_leave_response($leave);
        } 
        catch(\Exception $e){
            // catch code
        }
                

        $response = [
                'status' => 1,
                'message' => 'Promoter request ' . $request->status . 'd',
                'url' => '/leave-request/promoters?leave=0'
            ];

        return json_encode($response);

    }

    public function csv($person)
    {
        if($person == 'merchandisers'){
            return App\Merchandiser::leave_csv($this->leave_merchandisers_data(),$this->year);
        }

        if($person == 'promoters'){
            return App\Promoter::leave_csv($this->leave_promoters_data(),$this->year);
        }

        return show_404();
    }


    private function rules()
    {
        return [
                'reason' => 'required',
                'status' => 'required'
            ];
    }
}