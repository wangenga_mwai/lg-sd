<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\OutletBranch;

use Validator;

class OutletBranchesController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),$this->rules());

        if ($validator->fails()) {
            return json_encode(['status'=>0,'errors'=>$validator->errors()]);
        }

        $branch = new OutletBranch;

        $branch->code = $branch->generate_code(['outlet_code',$request->outlet_code]);
        $branch->title = $request->title;
        $branch->outlet_code = $request->outlet_code;
        $branch->active = 1;

        $branch->save();

        return json_encode([
                'status' => 1,
                'message' => 'Outlet branch saved succesfully.',
                'url' => '/outlet/' . $request->outlet_code . '/branches'
            ]);

        return json_encode($branch);
    }


    public function edit($id)
    {
    	return OutletBranch::findOrFail($id);
    }


    public function update(Request $request)
    {
        $branch = OutletBranch::findOrFail($request->id);

        $validator = Validator::make($request->all(),$this->rules($request->id));

        if ($validator->fails()) {
            return json_encode(['status' => 0, 'errors' => $validator->errors()]);
        }

        $branch->title = $request->title;

        $branch->save();

        return json_encode([
                'status' => 1,
                'message' => 'OutletBranch updated succesfully.',
                'url' => '/outlet/' . $request->outlet_code . '/branches'
            ]);


    }



    public function activate($id)
    {
    	$response = OutletBranch::toggle_active($id);

    	return json_encode($response);
    }


    public function delete($id)
    {
        $branch = OutletBranch::findOrFail($id);

        if($branch->delete()){
            $response = [
                'status' => 1,
                'message' => 'Outlet branch deleted succesful.'
                ];
        }else {
            $response = [
                'status' => 0,
                'message' => 'Failed: Outlet branch was not deleted'
                ];
        }

        return json_encode($response);
    }



    private function rules($id=null)
    {
        return [
                'title' => 'required',
            ];
    }
}
