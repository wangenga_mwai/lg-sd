<?php

namespace App\Http\Controllers;

use App\Models;
use App;
use App\StockUnit;

use Validator;

use Illuminate\Http\Request;

use App\Http\Requests;

class ModelsController extends Controller
{
	public function index()
	{

		$data = [
				'models' => Models::with('category','sub_category','group','stock_unit')->get(),
				'categories' => App\Category::get(),
    			'sub_categories' => App\SubCategory::get(),
    			'groups' => App\Group::get()
			];
    	return view('settings.models',$data);
		
	}


	public function store(Request $request)
    {
        $validator = Validator::make($request->all(),$this->rules());

        if ($validator->fails()) {
            return json_encode(['status'=>0,'errors'=>$validator->errors()]);
        }

        $model = new Models;

        $model->title = $request->title;
        $model->category_id = $request->category_id;
        $model->sub_category_id = $request->sub_category_id;
        $model->group_id = $request->group_id;
        $model->description = $request->description;
        $model->active = 1;

        $model->save();

        return json_encode([
                'status' => 1,
                'message' => 'Model saved succesfully.',
                'url' => '/models'
            ]);

        return json_encode($model);
    }


    public function edit($id)
    {
        $model = Models::where('id',$id)->with('stock_unit')->firstorFail();

        $response = [
                'id' => $model->id,
                'title' => $model->title,
                'category_id' => $model->category_id,
                'sub_category_id' => $model->sub_category_id,
                'group_id' => $model->group_id,
                'description' => $model->description,
                'price' => ($model->stock_unit)? $model->stock_unit->price: '',
                'commission' => ($model->stock_unit)? $model->stock_unit->commission: '',
                'additional_commission' => ($model->stock_unit)? $model->stock_unit->additional_commission: '',
            ];

        return response()->json($response);
    	return Models::findOrFail($id);
    }


    public function update(Request $request)
    {
        $model = Models::findOrFail($request->id);

        $rules = array_merge($this->rules(),[
                'price' => 'required|numeric',
                'commission' => 'required|numeric',
                'additional_commission' => 'required|numeric',
            ]);

        $validator = Validator::make($request->all(),$this->rules($request->id));

        if ($validator->fails()) {
            return json_encode(['status' => 0, 'errors' => $validator->errors()]);
        }

        $model->title = $request->title;
        $model->category_id = $request->category_id;
        $model->sub_category_id = $request->sub_category_id;
        $model->group_id = $request->group_id;
        $model->description = $request->description;

        $stock_unit = StockUnit::firstOrNew(['id' => $request->id]);

        $stock_unit->price = $request->price;
        $stock_unit->commission = $request->commission;
        $stock_unit->additional_commission = $request->additional_commission;

        $model->save();
        $stock_unit->save();

        return json_encode([
                'status' => 1,
                'message' => 'Model updated succesfully.',
                'url' => '/models'
            ]);


    }



    public function activate($id)
    {
    	$response = Models::toggle_active($id);

    	return json_encode($response);
    }


    private function rules($id=null)
    {
        return [
                'title' => 'required|unique:lg_models,title,' . $id,
                'price' => 'required|numeric',
                'category_id' => 'required',
                'sub_category_id' => 'required',
                'group_id' => 'required',
                'additional_commission' => 'numeric',
            ];
    }
}
