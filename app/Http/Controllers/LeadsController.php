<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App;
use App\Lead;

use DB;

class LeadsController extends Controller
{
    public function index(Request $request)
    {

    	$leads =  Lead::with('promoter', 'statusRel')->where('status', $request->input('status', 1))->get();

    	$data = [
    			'leads' => $leads
    		];

    	return view('leads.view',$data);
    }


    public function promoters()
    {
    	$data = [
    		'promoters' => App\Promoter::active()
    								// ->withCount('leads')
    								// ->has('leads')
    								->with('leads.statusRel')
    								->with(['lead_count' => function($q){
    									$q->where('status', 2);
    								}])
    								->get()
    	];

    	// return $data;


    	return view('leads.promoters', $data);
    }


    public function promoter_leads(Request $request, $code)
    {

    	$status = $request->input('status', 1);
    	$promoter =  App\Promoter::where('code', $code)
    					->with('leads.statusRel')
    					->with(['leads' => function($q) use($status ) {
    						$q->where('status', $status);
    					}])
    					// ->whereHas('leads', function($q) use($request ) {
    					// 	$q->where('status', $request->input('status', 1));
    					// })
    					// ->has('leads')

    					
    					->first();

    	$data = [
    			'promoter' => $promoter
    		];
// return $data;
    	return view('leads.promoter_leads', $data);
    }
}
