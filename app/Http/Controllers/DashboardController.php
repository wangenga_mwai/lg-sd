<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;
use App\Sale;
use DB;
use App\Http\Requests;

use Validator;
use Mail;


class DashboardController extends Controller
{

    public function index()
    {
        
        $data = [
                'merchandisers' => App\Merchandiser::where('active',1)->get(),
                'promoters' => App\Promoter::where('active',1)->get(),
                'sales' => App\Sale::sum(),
                'transactions' => App\Sale::active()->count()
            ];
    	return view('dashboard.dashboard',$data);
    }

    public function chart()
    {
        $sales = App\Sale::whereBetween('created_at',[$this->from,$this->to])->get();

        $sub_categories = App\SubCategory::active()->with(['sales' => function($q) {
                                            $q->whereBetween('created_at',[$this->from,$this->to]);
                                            $q->active();
                                        }])->get();

        foreach ($sub_categories as $key => $sub_category) {
            $sub_category['total'] = 0;
            foreach ($sub_category->sales as $key => $sale) {
                $sub_category['total'] += $sale->amount;
            }
        }

        // return json_encode($sub_categories);

        $sub_categories_sales = array();

        // echo Sale::total_sales_per_day($this->from); exit;

        $chart['labels'] = ['Mon','Tue','Wen','Thu','Fri','Sat','Sun'];
        foreach ($sub_categories as $key => $sub_category) {
            $chart['datasets'][] = [
                'label' => $sub_category->title,
                'fillColor' => $sub_category->fillColor,
                'strokeColor' => $sub_category->strokeColor,
                'pointColor' => $sub_category->pointColor,
                'pointStrokeColor' => $sub_category->pointStrokeColor,
                'pointHighlightFill' => $sub_category->pointHighlightFill,
                'pointHighlightStroke' => $sub_category->pointHighlightStroke,
                'data' => [
                        Sale::daily_sales_per_sub_category($this->from,$sub_category->id),
                        Sale::daily_sales_per_sub_category(date('Y-m-d',strtotime($this->from . ' +1 day')),$sub_category->id),
                        Sale::daily_sales_per_sub_category(date('Y-m-d',strtotime($this->from . ' +2 days')),$sub_category->id),
                        Sale::daily_sales_per_sub_category(date('Y-m-d',strtotime($this->from . ' +3 days')),$sub_category->id),
                        Sale::daily_sales_per_sub_category(date('Y-m-d',strtotime($this->from . ' +4 days')),$sub_category->id),
                        Sale::daily_sales_per_sub_category(date('Y-m-d',strtotime($this->from . ' +5 days')),$sub_category->id),
                        Sale::daily_sales_per_sub_category(date('Y-m-d',strtotime($this->from . ' +6 days')),$sub_category->id),
                    ]
            ];
            
        }

        $data = [
                'chart' => json_encode($chart)
            ];

        return view('dashboard.chart',$data);

        return $chart;
    }

    public function all()
    {
    	return view('layouts.master',['title'=>'Quantium']);
    }


    public function display_share()
    {
        $data = [
                'sub_categories' => App\SubCategory::active()->get(),
                'brands' => App\Brand::active()->get(),
                'outlets' => App\Outlet::active()->get(),
                'shares' => App\DisplayShare::where('year',$this->year)
                                            ->where('week', $this->week)
                                            ->get(),
                'old_year' => $this->year,
                'old_week' => $this->week,
                'available_years' => App\DisplayShare::select('year')->groupBy('year')->get()
            ];
                // return App\DisplayShare::where('year',$this->year)->where('week', $this->week)->get();

        return view('dashboard.display_share',$data);
    }

    public function branch_display_share($code)
    {
        $data = [
                'outlet' => App\Outlet::where('code',$code)->firstorFail(),
                'sub_categories' => App\SubCategory::active()->get(),
                'brands' => App\Brand::active()->get(),
                'outlet_branches' => App\OutletBranch::where('outlet_code',$code)->active()->get(),
                'shares' => App\DisplayShare::where('outlet_code',$code)->where('year',$this->year)->where('week', $this->week)->get(),
                'old_year' => $this->year,
                'old_week' => $this->week
            ];
        return view('dashboard.branch_display_share',$data);
    }

    public function messages()
    {

        $data = [
                'promoters' => App\Promoter::active()->get()
            ];
        return view('dashboard.messages', $data);
    }

    public function post_messages(Request $request)
    {
        $validator = Validator::make($request->all(),[
                    'subject' => 'required',
                    'body' => 'required',
            ]);

        if ($validator->fails()) {
            return json_encode(['status' => 0, 'errors' => $validator->errors()]);
        }

        if(isset($request->all) && $request->all == true){
            $promoters = App\Promoter::active()->get();
        } else {
            $promoters = App\Promoter::whereIn('code',$request->promoters)->get();
        }

        $count = 0;

        foreach ($promoters as $key => $promoter) {
            $data = [
                'body' => $request->body,
                    ];

            $subject = $request->subject;
            Mail::queue('mail.message',$data,function($mail) use ($promoter, $subject){
                $mail->from('lgsudan@quantiumservices.com', 'LG Incentives Portal');
                $mail->to($promoter->email, $promoter->first_name . ' ' . $promoter->last_name);
                $mail->subject($subject);
            });

            $message = new App\Message;

            $message->subject = $request->subject;
            $message->message = $request->body;
            $message->promoter_code = $promoter->code;

            $message->save();

            $count++;


        }

        return json_encode(['status' => 1, 'message' => 'Message sent to ' . $count . ' promoters']);
    }
}
