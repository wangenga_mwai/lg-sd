<?php

namespace App\Http\Controllers;

use App\Category;

use Validator;

use Illuminate\Http\Request;

use App\Http\Requests;

class CategoriesController extends Controller
{
    public function index()
    {
    	return view('categories.view',['categories' => Category::get()]);
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),$this->rules());

        if ($validator->fails()) {
            return json_encode(['status'=>0,'errors'=>$validator->errors()]);
        }

        $category = new Category;

        $category->title = $request->title;
        $category->active = 1;

        $category->save();

        return json_encode([
                'status' => 1,
                'message' => 'Category saved succesfully.',
                'url' => '/categories'
            ]);

        return json_encode($category);
    }


    public function edit($id)
    {
    	return Category::findOrFail($id);
    }


    public function update(Request $request)
    {
        $category = Category::findOrFail($request->id);

        $validator = Validator::make($request->all(),$this->rules($request->id));

        if ($validator->fails()) {
            return json_encode(['status' => 0, 'errors' => $validator->errors()]);
        }

        $category->title = $request->title;

        $category->save();

        return json_encode([
                'status' => 1,
                'message' => 'Category updated succesfully.',
                'url' => '/categories'
            ]);


    }



    public function activate($id)
    {
    	$response = Category::toggle_active($id);

    	return json_encode($response);
    }


    private function rules($id=null)
    {
        return [
                'title' => 'required',
            ];
    }
}
