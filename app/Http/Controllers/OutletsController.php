<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Outlet;

use Validator;

use App\Http\Requests;

class OutletsController extends Controller
{
    public function index($code=0)
    {
    	if($code !== 0){
    		return Outlet::where('code',$code)->firstorFail()->branches;
    	}

    	$data = [
    			'outlets' => Outlet::with('branches')->get()
    		];

    	return view('outlets.view',$data);
    }



    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),$this->rules());

        if ($validator->fails()) {
            return json_encode(['status'=>0,'errors'=>$validator->errors()]);
        }

        $outlet = new Outlet;

        $outlet->code = $outlet->generate_code();
        $outlet->title = $request->title;
        $outlet->active = 1;

        $outlet->save();

        return json_encode([
                'status' => 1,
                'message' => 'Outlet saved succesfully.',
                'url' => '/outlets'
            ]);

        return json_encode($outlet);
    }


    public function edit($id)
    {
    	return Outlet::findOrFail($id);
    }


    public function update(Request $request)
    {
        $outlet = Outlet::findOrFail($request->id);

        $validator = Validator::make($request->all(),$this->rules($request->id));

        if ($validator->fails()) {
            return json_encode(['status' => 0, 'errors' => $validator->errors()]);
        }

        $outlet->title = $request->title;

        $outlet->save();

        return json_encode([
                'status' => 1,
                'message' => 'Outlet updated succesfully.',
                'url' => '/outlets'
            ]);


    }

    public function branches($code)
    {
        $data = [
                'outlet' => Outlet::whereCode($code)->with('branches')->firstorFail()
            ];

        return view('outlets.branches',$data);
    }



    public function activate($id)
    {
    	$response = Outlet::toggle_active($id);

    	return json_encode($response);
    }

    public function delete($id)
    {
        $outlet = Outlet::findOrFail($id);

        if($outlet->delete()){
            $response = [
                'status' => 1,
                'message' => 'Outlet deleted succesful.'
                ];
        }else {
            $response = [
                'status' => 0,
                'message' => 'Failed: Outlet was not deleted'
                ];
        }

        return json_encode($response);
    }


    private function rules($id=null)
    {
        return [
                'title' => 'required',
            ];
    }
}
