<?php

namespace App\Http\Controllers;

use App\Group;
use App;

use Validator;

use Illuminate\Http\Request;

use App\Http\Requests;

class GroupsController extends Controller
{
    public function index()
    {

    	$data = [
    			'groups' => Group::with('sub_category.category')->get(),
    			'categories' => App\Category::get(),
    			'sub_categories' => App\SubCategory::get()

    		];
    	return view('settings.groups',$data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),$this->rules());

        if ($validator->fails()) {
            return json_encode(['status'=>0,'errors'=>$validator->errors()]);
        }

        $group = new Group;

        $group->title = $request->title;
        $group->category_id = $request->category_id;
        $group->sub_category_id = $request->sub_category_id;
        $group->active = 1;

        $group->save();

        return json_encode([
                'status' => 1,
                'message' => 'Group saved succesfully.',
                'url' => '/groups'
            ]);

        return json_encode($group);
    }


    public function edit($id)
    {
    	return Group::findOrFail($id);
    }


    public function update(Request $request)
    {
        $group = Group::findOrFail($request->id);

        $validator = Validator::make($request->all(),$this->rules($request->id));

        if ($validator->fails()) {
            return json_encode(['status' => 0, 'errors' => $validator->errors()]);
        }

        $group->title = $request->title;
        $group->category_id = $request->category_id;
        $group->sub_category_id = $request->sub_category_id;

        $group->save();

        return json_encode([
                'status' => 1,
                'message' => 'Group updated succesfully.',
                'url' => '/groups'
            ]);


    }



    public function activate($id)
    {
    	$response = Group::toggle_active($id);

    	return json_encode($response);
    }


    private function rules($id=null)
    {
        return [
                'title' => 'required',
                'category_id' => 'required',
                'sub_category_id' => 'required',
            ];
    }
}
