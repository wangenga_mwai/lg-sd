<?php

namespace App\Http\Controllers;

use App\SubCategory;
use App;

use Validator;

use Illuminate\Http\Request;

use App\Http\Requests;

class SubCategoriesController extends Controller
{
    public function index()
    {
    	$data = [
    			'sub_categories'=>SubCategory::with('category')->get(),
    			'categories' => App\Category::get()
    		];
    	return view('settings.sub_categories', $data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),$this->rules());

        if ($validator->fails()) {
            return json_encode(['status'=>0,'errors'=>$validator->errors()]);
        }

        $sub_category = new SubCategory;

        $sub_category->title = $request->title;
        $sub_category->category_id = $request->category_id;
        $sub_category->active = 1;

        $sub_category->save();

        return json_encode([
                'status' => 1,
                'message' => 'Sub Category saved succesfully.',
                'url' => '/subcategories'
            ]);

        return json_encode($sub_category);
    }


    public function edit($id)
    {
    	return SubCategory::findOrFail($id);
    }


    public function update(Request $request)
    {
        $sub_category = SubCategory::findOrFail($request->id);

        $validator = Validator::make($request->all(),$this->rules($request->id));

        if ($validator->fails()) {
            return json_encode(['status' => 0, 'errors' => $validator->errors()]);
        }

        $sub_category->title = $request->title;
        $sub_category->category_id = $request->category_id;

        $sub_category->save();

        return json_encode([
                'status' => 1,
                'message' => 'Sub category updated succesfully.',
                'url' => '/subcategories'
            ]);


    }



    public function activate($id)
    {
    	$response = SubCategory::toggle_active($id);

    	return json_encode($response);
    }


    private function rules($id=null)
    {
        return [
                'title' => 'required',
                'category_id' => 'required',
            ];
    }
}
