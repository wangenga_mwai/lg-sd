<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;
use App\Merchandiser;

use CSV;
Use Mail;
use Validator;

use App\Http\Requests;

class MerchandisersController extends Controller
{

    # display all the resources
    public function index()
    {
    	$data = [
    		'merchandisers' => Merchandiser::get(),
    		'banks' => App\Bank::get()
    		];

    	return view('merchandisers.view',$data);
    }

    # show a specific resource

    public function show($id)
    {
        $m = Merchandiser::where('id',$id)->with('bank','bank_branch')->firstorFail();
        return view('merchandisers.show', ['merchandiser' => $m]);
    }

    # add a new resource

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),$this->rules());

        if ($validator->fails()) {
            return json_encode(['status'=>0,'errors'=>$validator->errors()]);
        }

        $m = new Merchandiser;

        $m->code = $m->generate_code();
        $m->first_name = $request->first_name;
        $m->last_name = $request->last_name;
        $m->id_number = $request->id_number;
        $m->email = $request->email;
        $m->phone = $request->phone;
        $m->mobile_money = $request->mobile_money;
        $m->salary = $request->salary;
        $m->salary_level = $request->salary_level;
        $m->bank_code = $request->bank_code;
        $m->bank_branch_code = $request->bank_branch_code;
        $m->account_no = $request->account_no;
        $m->active = 1;
        $m->password = md5($m->id_number);

        $m->save();

        // $to = $request->email;

        $data = [
            'm' => $m,
            ];

        Mail::send('mail.staff_registration', $data, function ($mail) use ($m) {


            $mail->from('lgsudan@quantiumservices.com', 'LG Incentives Portal');
            $mail->to($m->email, $m->first_name . ' ' . $m->last_name);
            $mail->subject('LG Merchant System Registration code');
        
            
        });

        return json_encode([
                'status' => 1,
                'message' => 'Merchandiser saved succesfully.',
                'url' => 'staff/merchandisers'
            ]);



        return json_encode($m);
    }


    public function edit($id)
    {
    	return Merchandiser::findOrFail($id);
    }

    /*
    
    update the resourse

    */

    public function update(Request $request)
    {
        $m = Merchandiser::findOrFail($request->id);

        $validator = Validator::make($request->all(),$this->rules($request->id));

        if ($validator->fails()) {
            return json_encode(['status' => 0, 'errors' => $validator->errors()]);
        }

        $m->first_name = $request->first_name;
        $m->last_name = $request->last_name;
        $m->id_number = $request->id_number;
        $m->email = $request->email;
        $m->phone = $request->phone;
        $m->mobile_money = $request->mobile_money;
        $m->salary = $request->salary;
        $m->salary_level = $request->salary_level;
        $m->bank_code = $request->bank_code;
        $m->bank_branch_code = $request->bank_branch_code;
        $m->account_no = $request->account_no;

        $m->save();

        return json_encode([
                'status' => 1,
                'message' => 'Merchandiser updated succesfully.',
                'url' => 'staff/merchandisers'
            ]);


    }


    public function activate($id)
    {
    	$response = Merchandiser::toggle_active($id);

    	return json_encode($response);
    }

    public static function delete($id)
    {
    	$model = Merchandiser::findOrFail($id);

        if($model->delete()) {
            $response = [
                'status' => 1,
                'message' => 'Merchandiser delete succesfully.'
            ];
        } else {
            $response = [
                'status' => 0,
                'message' => 'Failed: Merchandiser was not deleted. Please try again.'
            ];
        }

        return json_encode($response);
    }


    public static function csv()
    {

        $file_name = 'Merchandisers.csv';

        $csvHeader = [
            'Code',
            'Name',
            'ID Number',
            'Phone',
            'Mobile Money',
            'Email',
            'Salary',
            'Account Number',
            'Bank',
            'Branch',
            ];

        $csvData = array();

        $merchandisers = Merchandiser::active()->with('bank','bank_branch')->get();

        foreach ($merchandisers as $key => $m) {
            $csvData[] = [
                    @$m->code,
                    @$m->first_name . ' ' . @$m->last_name,
                    @$m->id_number,
                    @$m->phone,
                    @$m->mobile_money,
                    @$m->email,
                    @$m->salary,
                    @$m->account_no,
                    @$m->bank->title,
                    @$m->bank_branch->title,
                ];
            
        }

        return Merchandiser::CSV($csvHeader,$csvData,$file_name);
    }



    public function clear_imei($id)
    {
        $merchandiser = Merchandiser::findOrFail($id);

        $merchandiser->imei = '';

        $merchandiser->save();

        return json_encode([
            'status' => 1,
            'message' => 'imei cleared'
            ]);
    }


    

    private function rules($id=null)
    {
        return [
                'first_name' => 'required',
                'last_name' => 'required',
                'id_number' => 'required|numeric',
                'email' => 'required|email|unique:lg_merchandisers,email,'.$id,
                'phone' => 'required|numeric|unique:lg_merchandisers,phone,'.$id,
                'mobile_money' => 'required|numeric',
                'salary' => 'required|numeric',
                'salary_level' => 'required|numeric',
                'bank_code' => 'required',
                'bank_branch_code' => 'required',
                'account_no' => 'required',
            ];
    }

    
}
