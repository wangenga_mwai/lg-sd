<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Customer;

use Validator;

class CustomersController extends BaseController
{
    public function index(Request $request, $code=0)
    {
    	if($code !== 0){
    		return Customer::where('code',$code)->firstorFail()->branches;
    	}

        $data = array();

        if(isset($request->phone) && $request->phone == 1) {
            $customers = Customer::where('phone', '<>', '')->get();
        } elseif(isset($request->phone) && $request->phone == 0) {
            $customers = Customer::where('phone', '')->get();

        } elseif(isset($request->email) && $request->email == 1) {
            $customers = Customer::where('email', 'like', '%@%.%')->get();
        } elseif(isset($request->email) && $request->email == 0) {
            $customers = Customer::where('email', 'NOT like', '%@%.%')->get();
        } else {
            $amounts = [0, 1000000000000000];
            $purchases = [0, 1000000000000000];

            if (isset($request->start_amount) && $request->start_amount != '') {
                $amounts[0] = $request->start_amount;
                $data['old_start_amount'] = $request->start_amount;
            }

            if (isset($request->end_amount) && $request->end_amount != '') {
                $amounts[1] = $request->end_amount;
                $data['old_end_amount'] = $request->end_amount;
            }


            if (isset($request->start_number) && $request->start_number != '') {
                $purchases[0] = $request->start_number;
                $data['old_start_number'] = $request->start_number;
            }

            if (isset($request->end_number) && $request->end_number != '') {
                $purchases[1] = $request->end_number;
                $data['old_end_number'] = $request->end_number;
            }


            $customers = Customer::whereBetween('created_at', $this->dates)
                                ->whereBetween('value_of_purchases', $amounts)
                                ->whereBetween('no_of_purchases', $purchases)
                                ->get();
        }

    	$data = [
    			'customers' => $customers,
                'old_start_date' => substr($this->dates[0], 0, 10),
                'old_end_date' => substr($this->dates[1], 0, 10),
    		];

            // return $data;

    	return view('customers.view',$data);
    }



    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),$this->rules());

        if ($validator->fails()) {
            return json_encode(['status'=>0,'errors'=>$validator->errors()]);
        }

        $customer = new Customer;

        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->id_number = $request->id_number;
        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->active = 1;

        $customer->save();

        return json_encode([
                'status' => 1,
                'message' => 'Customer saved succesfully.',
                'url' => '/customers'
            ]);

        return json_encode($customer);
    }


    public function edit($id)
    {
    	return Customer::findOrFail($id);
    }


    public function update(Request $request)
    {
        $customer = Customer::findOrFail($request->id);

        $validator = Validator::make($request->all(),$this->rules($request->id));

        if ($validator->fails()) {
            return json_encode(['status' => 0, 'errors' => $validator->errors()]);
        }

        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->id_number = $request->id_number;
        $customer->phone = $request->phone;
        $customer->email = $request->email;

        $customer->save();

        return json_encode([
                'status' => 1,
                'message' => 'Customer updated succesfully.',
                'url' => '/customers'
            ]);


    }


    public function sales($id)
    {
        $data = [
                'customer' => Customer::whereId($id)->with('sales')->firstorFail()
            ];

        return view('customers.sales', $data);
    }



    public function activate($id)
    {
    	$response = Customer::toggle_active($id);

    	return json_encode($response);
    }


    public static function csv()
    {

        $file_name = 'customers.csv';

        $csvHeader = [
            'Name',
            'ID Number',
            'Phone',
            'Email',
            'Number of Purchases',
            'Value of Purchases'
            ];

        $csvData = array();

        $customers = Customer::active()->get();

        foreach ($customers as $key => $customer) {
            $csvData[] = [
                    $customer->first_name . ' ' . $customer->last_name,
                    $customer->id_number,
                    $customer->phone,
                    $customer->email,
                    $customer->no_of_purchases,
                    $customer->value_of_purchases,
                ];
            
        }

        return Customer::CSV($csvHeader,$csvData,$file_name);
    }


    private function rules($id=null)
    {
        return [
                'first_name' => 'required',
                'last_name' => 'required',
                'id_number' => 'required|numeric',
                'phone' => 'required|numeric',
                'email' => 'required|email|unique:lg_customers,email,'.$id,
            ];
    }
}
