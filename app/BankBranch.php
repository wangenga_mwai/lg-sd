<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankBranch extends Model
{
    protected $table = 'lg_bank_branches';

    protected $visible = ['code','bank_code','title'];
}
