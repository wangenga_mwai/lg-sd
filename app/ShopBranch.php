<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;

class ShopBranch extends BaseModel
{
    protected $table = 'lg_outlet_branches';

    public function shop()
    {
    	return $this->belongsTo('App\Shop','outlet_code','code');
    }
}
