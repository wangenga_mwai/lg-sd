<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;

class Outlet extends BaseModel
{
    protected $table = 'lg_outlets';

    protected static $code = 'LGSH';


    public function branches()
    {
    	return $this->hasMany('App\OutletBranch','outlet_code','code');
    }
}
