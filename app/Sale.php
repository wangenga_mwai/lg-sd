<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;

class Sale extends BaseModel
{
    protected $table = 'lg_sales';


    public function category()
    {
    	return $this->belongsTo('App\Category');
    }

    public function sub_category()
    {
    	return $this->belongsTo('App\SubCategory');
    }

    public function model()
    {
    	return $this->belongsTo('App\Models');
    }

    public function promoter()
    {
    	return $this->belongsTo('App\Promoter','promoter_code','code');
    }

    public function merchandiser()
    {
    	return $this->belongsTo('App\Merchandiser','merchandiser_code','code');
    }


    public function shop()
    {
    	return $this->belongsTo('App\Shop','outlet_code','code');
    }

    public function shop_branch()
    {
    	return $this->belongsTo('App\ShopBranch','outlet_branch_code','code');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public static function sum()
    {
        return static::active()->sum('amount');
    }

    public static function daily_sales_per_sub_category($date,$sub_category_id)
    {
        return static::whereBetween('created_at',[$date . ' 00:00:00',$date . ' 23:59:59'])
                       ->where('sub_category_id',$sub_category_id)
                       ->active()
                       ->sum('amount');
    }
}
