<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use CSV;

use DB;


class BaseModel extends Model
{


    public static function toggle_active($id)
    {
    	$model = static::findOrFail($id);

    	$model->active = ($model->active == 1)? 0: 1;

    	if ($model->save()) {
    		$response = [
    			'status' => 1,
    			'active' => $model->active,
    			'message' => ($model->active)? substr(get_called_class(), 4).' activated.': substr(get_called_class(), 4).' deactivated.',
    		];
    	} else {
    		$response = [
    			'status' => 0,
    			'message' => 'Failed: Please try again.',
    		];
    	}

    	return $response;
    }

    // define basic relationship that will run for more than one model
    // pretty cool

    public function outlet()
    {
        return $this->belongsTo('App\Outlet','outlet_code','code');
    }

    public function outlet_branch()
    {
        return $this->belongsTo('App\OutletBranch','outlet_branch_code','code');
    }

    public function sale_outlet_branch()
    {   
        $outlet_code = $this->outlet_code;
        $outlet_branch_code = $this->outlet_branch_code;
        return  OutletBranch::where('code',$outlet_branch_code)->where('outlet_code',$outlet_code)->first();
    }

    public function bank()
    {
        return $this->belongsTo('App\Bank','bank_code','code');
    }

    public function bank_branch()
    {
        return $this->belongsTo('App\BankBranch','bank_branch_code','code');
    }

    public static function branch_of_bank($bank,$branch)
    {     

        return BankBranch::where('bank_code',$bank)->where('code',$branch)->first();
    }

    public static function branch_of_outlet($outlet,$branch)
    {

        return OutletBranch::where('outlet_code',$outlet)->where('code',$branch)->first();
    }

    // definig scope
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public static function generate_code($filter = ['id',0])
    {
        

        $model = static::orderBy('code','desc')->where('active',1)->orWhere($filter[0],$filter[1])->first();

        $current = ($model)? substr($model->code, -3): '000';

        $current++;

        $next_code = ( strlen( $current ) == 1 ) ? '00' . $current : ( ( strlen( $current ) == 2 ) ? '0' . $current : $current );


        return static::$code . $next_code;
    }


    public static function used_days($year,$code)
    {
        return static::where('year',$year)->where(static::$person_code,$code)->where('status', 1)->where('leave_type_id',1)->sum('no_of_days');
    }

    // CSV

    public static function check_csv($data,$name,$date)
    {   
        if (array_key_exists('checks', $data)) {
            $data = $data['checks'];
        }
        
        $csvHeader = [
            'Date',
            $name,
            'Check in Time',
            'Check in Location',
            'Check out Time',
            'Check out Location'
            ];


        $csvData = array();
        foreach ($data as $key => $check) {

                $csvData[] = [
                    ($check->checkin)? $check->checkin->date: $check->check_date,
                    $check->code . ': ' . $check->first_name . ' ' . $check->last_name,
                    ($check->checkin)? date('H:m:i',strtotime($check->checkin->created_at)): 'Not checked in',
                    ($check->checkin)? $check->checkin->location: 'Not checked in',
                    ($check->checkout)? date('H:m:i',strtotime($check->checkout->created_at)): 'Not checked out',
                    ($check->checkout)? $check->checkout->location: 'Not Checked out'
                ];

            
        }

        $file_name = $name . ' check In - Out ' . $date . '.csv';


        return CSV::create($csvData, $csvHeader)->render($file_name);
    }

    public static function CSV($headers,$data,$file_name)
    {
        return CSV::create($data, $headers)->render($file_name);
    }

    public static function person_checks_csv($code, $data)
    {

        $name = strtolower( substr(get_called_class(), 4) );

        $csvHeader = [
            'Name',
            'Date',
            'Check in Time',
            'Check in Location',
            'Check out Time',
            'Check out Location'
            ];

        $csvData = array();


        foreach ($data as $key => $check) {

            $csvData[] = [
                $check->first_name . ' ' . $check->last_name,
                ($check->checkin)? $check->checkin->date: (($check->check_date) ? $check->check_date : '-') ,
                ($check->checkin)? date('H:m:i',strtotime($check->checkin->created_at)): 'Not checked in',
                ($check->checkin)? $check->checkin->location: 'Not checked in',
                ($check->checkout)? date('H:m:i',strtotime($check->checkout->created_at)): 'Not checked out',
                ($check->checkout)? $check->checkout->location: 'Not Checked out'
            ];
            $name = $check->first_name . ' ' . $check->last_name;
            
        }

        $file_name = $code . ': ' . $name . ' checks.csv';

        return static::CSV($csvHeader,$csvData,$file_name);
    }

    public static function leave_csv($data, $year)
    {
        $csvHeader = [
            'Name',
            'Set Leave Days',
            'Earned Leave Days',
            'Used Leave days',
            'Balance',
            ];

        $csvData = array();


        foreach ($data as $key => $leave) {
            $csvData[] = [
                    $leave->person->first_name . ' ' . $leave->person->last_name,
                    $leave->set_leave_days,
                    $leave->earned_leave_days,
                    $leave->used_days,
                    $leave->earned_leave_days - $leave->used_days,
                ];

            
        }

        $model_name = strtolower( substr(get_called_class(), 4) );

        $file_name = $year . ' ' . $model_name . ' leaves.csv';

        return static::CSV($csvHeader,$csvData,$file_name);
    }


    



    
}
