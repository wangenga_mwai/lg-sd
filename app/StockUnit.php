<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockUnit extends Model
{
    protected $table = 'lg_stock_units';
}
