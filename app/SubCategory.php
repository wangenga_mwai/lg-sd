<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;

class SubCategory extends BaseModel
{
    protected $table = 'lg_sub_categories';

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }

    public function sales()
    {
    	return $this->hasMany('App\Sale','sub_category_id');
    }
}
