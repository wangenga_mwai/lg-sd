<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchandiserCheck extends Model
{
    protected $table = 'lg_check_merchandisers';

    public function person()
    {
    	return $this->belongsTo('App\Merchandiser','merchandiser_code','code');
    }
}
