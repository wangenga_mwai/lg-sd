<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesTarget extends Model
{
    protected $table = 'lg_sales_targets';

    protected $fillable = ['month','year','promoter_code'];
}
