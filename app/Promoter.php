<?php

namespace App;

use DB;
use App\Sale;

// use Illuminate\Database\Eloquent\Model;

class Promoter extends BaseModel
{
    protected $table = 'lg_promoters';

    protected static $code = 'QS';

    public function sales_targets()
    {
    	return $this->hasMany('App\SalesTarget','promoter_code','code');
    }

    public function sales()
    {
    	return $this->hasMany('App\Sale','promoter_code','code');
    }

    public function leads()
    {
        return $this->hasMany('App\Lead','promoter_code','code');
    }

    public function merchandiser()
    {
        return $this->belongsTo('App\Merchandiser','merchandiser_code','code');
    }

    public function sales_sum($dates)
    {
    	return sale::with('category','model','sub_category','promoter.merchandiser','outlet','outlet_branch','customer')
                   ->where('promoter_code',$this->code)
    			   ->whereBetween('created_at',$dates)
                   ->where('active',1)
    			   ->sum('amount');
    }

    public function checks()
    {
        return $this->hasMany('App\PromoterCheck','promoter_code','code');
    }

    public function commission_details()
    {

        return $this->hasMany('App\CommissionDetail','promoter_code','code');
        
    }


    public function lead_count()
    {
        return $this->hasOne('App\Lead', 'promoter_code', 'code')
           ->selectRaw('promoter_code, count(*) as lead_count')
           ->groupBy('promoter_code');
    }

    public function getaItemAttribute()
    {
        if ( ! array_key_exists('lead_count', $this->relations)) {
           $this->load('lead_count');
        }

        $relation = $this->getRelation('lead_count');

        return ($relation) ? $relation->lead_count : null;
    }
}
