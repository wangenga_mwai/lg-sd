<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brochure extends BaseModel
{
    protected $table = 'lg_digital_bronchures'; 


    public function promoter()
    {
    	return $this->belongsTo('App\Promoter', 'promoter_code', 'code');
    }

    public function model()
    {
        return $this->belongsTo('App\Models');
    }
    
}
