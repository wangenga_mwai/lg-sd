<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;

class PromoterLeaveRequest extends BaseModel
{
    protected $table = 'lg_promoters_leaves_requests';

    protected static $person_code = 'promoter_code';

    public function person()
    {
    	return $this->belongsTo('App\Promoter','promoter_code','code');
    }

    public function type()
    {
    	return $this->belongsTo('App\LeaveType','leave_type_id');
    }
   
}
