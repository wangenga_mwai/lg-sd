<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoterCheck extends Model
{
    protected $table = 'lg_check_promoters';

    public function person()
    {
    	return $this->belongsTo('App\Promoter','promoter_code','code');
    }
}
