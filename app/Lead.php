<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lead extends BaseModel
{

	protected $table = 'lg_leads';


    public function promoter()
    {
    	return $this->belongsTo('App\Promoter','promoter_code','code');
    }


    public function statusRel()
    {
    	return $this->belongsTo('App\LeadStatus', 'status');
    }
}
