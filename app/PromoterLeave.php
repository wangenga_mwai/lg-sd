<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoterLeave extends Model
{
    protected $table = 'lg_promoters_leaves';

    public function person()
    {
    	return $this->belongsTo('App\Promoter','promoter_code','code');
    }

    // public function requests()
    // {
    // 	$this->hasMany('App\PromoterLeaveRequest','promoter_code','code');
    // }
}
