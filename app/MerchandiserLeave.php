<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchandiserLeave extends BaseModel
{
    protected $table = 'lg_merchandisers_leaves';

    public function person()
    {
    	return $this->belongsTo('App\Merchandiser','merchandiser_code','code');
    }
    
}
