<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;

class Customer extends BaseModel
{
    protected $table = 'lg_customers';


    public function sales()
    {
    	return $this->hasMany('App\Sale');
    }
}
