<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;

class Models extends BaseModel
{
    protected $table = 'lg_models';

    public function group()
    {
    	return $this->belongsTo('App\Group');
    }

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }

    public function sub_category()
    {
    	return $this->belongsTo('App\SubCategory');
    }

    public function stock_unit()
    {
    	return $this->hasOne('App\StockUnit','model_id');
    }
}
