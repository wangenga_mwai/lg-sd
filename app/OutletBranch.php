<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;

class OutletBranch extends BaseModel
{
    protected $table = 'lg_outlet_branches';

    protected static $code = '';

    public function shop()
    {
    	return $this->belongsTo('App\Outlet','outlet_code','code');
    }
}
