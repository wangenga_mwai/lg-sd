<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromoterLeaveProcessedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lg_promoters_leaves_processed', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('promoter_code');
            $table->integer('year');
            $table->integer('leave_type_id');
            $table->date('start_date');
            $table->date('end_date');
            $table->float('no_of_days');
            $table->string('reason');
            $table->integer('status');
            $table->integer('active');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lg_promoters_leaves_processed');
    }
}
