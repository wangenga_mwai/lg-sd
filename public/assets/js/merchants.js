$(function($){

	var container = $('#page_content');
	var page_content_inner = $('#page_content_inner');

	var tables = function() {
		$('.table-search-paging').DataTable({
			"paging":   true,
		    "ordering": false,
		    "info":     false,
		    'lengthMenu': [[50, 100, 200, -1], [50, 100, 200, 'All']]
		});

		$('.table-search').DataTable({
			"paging":   false,
		    "ordering": false,
		    "info":     false,
		    'lengthMenu': [[50, 100, 200, -1], [50, 100, 200, 'All']]
		});

	}


	var tabs = function() {

    	var UI = UIkit;

	    "use strict";

	    UI.component('tab', {

	        defaults: {
	            'target'    : '>li:not(.uk-tab-responsive, .uk-disabled)',
	            'connect'   : false,
	            'active'    : 0,
	            'animation' : false,
	            'duration'  : 200,
	            'swiping'   : true
	        },

	        boot: function() {

	            // init code
	            UI.ready(function(context) {

	                UI.$("[data-uk-tab]", context).each(function() {

	                    var tab = UI.$(this);

	                    if (!tab.data("tab")) {
	                        var obj = UI.tab(tab, UI.Utils.options(tab.attr("data-uk-tab")));
	                    }
	                });
	            });
	        },

	        init: function() {

	            var $this = this;

	            this.current = false;

	            this.on("click.uikit.tab", this.options.target, function(e) {

	                e.preventDefault();

	                if ($this.switcher && $this.switcher.animating) {
	                    return;
	                }

	                var current = $this.find($this.options.target).not(this);

	                current.removeClass("uk-active").blur();

	                $this.trigger("change.uk.tab", [UI.$(this).addClass("uk-active"), $this.current]);

	                $this.current = UI.$(this);

	                // Update ARIA
	                if (!$this.options.connect) {
	                    current.attr('aria-expanded', 'false');
	                    UI.$(this).attr('aria-expanded', 'true');
	                }
	            });

	            if (this.options.connect) {
	                this.connect = UI.$(this.options.connect);
	            }

	            // init responsive tab
	            this.responsivetab = UI.$('<li class="uk-tab-responsive uk-active"><a></a></li>').append('<div class="uk-dropdown uk-dropdown-small"><ul class="uk-nav uk-nav-dropdown"></ul><div>');

	            this.responsivetab.dropdown = this.responsivetab.find('.uk-dropdown');
	            this.responsivetab.lst      = this.responsivetab.dropdown.find('ul');
	            this.responsivetab.caption  = this.responsivetab.find('a:first');

	            if (this.element.hasClass("uk-tab-bottom")) this.responsivetab.dropdown.addClass("uk-dropdown-up");

	            // handle click
	            this.responsivetab.lst.on('click.uikit.tab', 'a', function(e) {

	                e.preventDefault();
	                e.stopPropagation();

	                var link = UI.$(this);

	                $this.element.children('li:not(.uk-tab-responsive)').eq(link.data('index')).trigger('click');
	            });

	            this.on('show.uk.switcher change.uk.tab', function(e, tab) {
	                $this.responsivetab.caption.html(tab.text());
	            });

	            this.element.append(this.responsivetab);

	            // init UIkit components
	            if (this.options.connect) {
	                this.switcher = UI.switcher(this.element, {
	                    'toggle'    : '>li:not(.uk-tab-responsive)',
	                    'connect'   : this.options.connect,
	                    'active'    : this.options.active,
	                    'animation' : this.options.animation,
	                    'duration'  : this.options.duration,
	                    'swiping'   : this.options.swiping
	                });
	            }

	            UI.dropdown(this.responsivetab, {"mode": "click"});

	            // init
	            $this.trigger("change.uk.tab", [this.element.find(this.options.target).not('.uk-tab-responsive').filter('.uk-active')]);

	            this.check();

	            UI.$win.on('resize orientationchange', UI.Utils.debounce(function(){
	                if ($this.element.is(":visible"))  $this.check();
	            }, 100));

	            this.on('display.uk.check', function(){
	                if ($this.element.is(":visible"))  $this.check();
	            });
	        },

	        check: function() {

	            var children = this.element.children('li:not(.uk-tab-responsive)').removeClass('uk-hidden');

	            if (!children.length) {
	                this.responsivetab.addClass('uk-hidden');
	                return;
	            }

	            var top          = (children.eq(0).offset().top + Math.ceil(children.eq(0).height()/2)),
	                doresponsive = false,
	                item, link, clone;

	            this.responsivetab.lst.empty();

	            children.each(function(){

	                if (UI.$(this).offset().top > top) {
	                    doresponsive = true;
	                }
	            });

	            if (doresponsive) {

	                for (var i = 0; i < children.length; i++) {

	                    item  = UI.$(children.eq(i));
	                    link  = item.find('a');

	                    if (item.css('float') != 'none' && !item.attr('uk-dropdown')) {

	                        if (!item.hasClass('uk-disabled')) {

	                            clone = item[0].outerHTML.replace('<a ', '<a data-index="'+i+'" ');

	                            this.responsivetab.lst.append(clone);
	                        }

	                        item.addClass('uk-hidden');
	                    }
	                }
	            }

	            this.responsivetab[this.responsivetab.lst.children('li').length ? 'removeClass':'addClass']('uk-hidden');
	        }
	    });

	}

    var loadContent = function(toLoad) {
    	container.prepend('<span id="loader" class="uk-icon-spin uk-icon-spinner"></span>');
		$('#loader').fadeIn('normal');
        container.load(toLoad,'',showNewContent);
    }
    var showNewContent = function() {
        if(window.location.hash.substr(1) == '/display-share')
        	tabs();

        if(window.location.hash.substr(1) == '/sales' || window.location.hash.substr(1) == '/check/promoters' || window.location.hash.substr(1) == '/models')
        	tables();

        $('#page_content_inner').show('normal',hideLoader);
    }
    var hideLoader = function() {
        $('#loader').fadeOut('normal').remove();
    }

    var swapClass = function(elem,from,to){
    	elem.removeClass(from).addClass(to);
    }

    var notice = function(msg,type){
        UIkit.notify({
            message : msg,
            status  : type,
            timeout : 5000,
            pos     : 'top-center'
        });
    }


    


    var fetchBankBranch = function(bank,branch){
    	var url = '/banks/'+bank;
    	var loader = $('.bank-branch-loader');

    	loader.removeClass('no-show').show();
    	$.get(url,'',function(response){
			var options = '<option value="">-- Select Branch --</option>';
			$.each(response,function(k,v){
				options += '<option value="'+v.code+'">'+v.title+'</option>';
			});

			elem = $('.bank_branch').html(options);
			if(branch !== undefined){
				elem.val(branch);
			}
    	}).fail(function(error,text,errorThrown){
    		notice(errorThrown,'danger');
    	}).always(function(){
    		loader.hide();
    	});
    }

    var fetchOutletBranch = function(outlet,branch){
    	var url = '/outlets/'+outlet;
    	var loader = $('.outlet-branch-loader');

    	loader.removeClass('no-show').show();
    	$.get(url,'',function(response){
			var options = '<option value="">-- Select Branch --</option>';
			$.each(response,function(k,v){
				options += '<option value="'+v.code+'">'+v.title+'</option>';
			});

			elem = $('.outlet_branch').html(options);
			if(branch !== undefined){
				elem.val(branch);
			}
    	}).fail(function(error,text,errorThrown){
    		notice(errorThrown,'danger');
    	}).always(function(){
    		loader.hide();
    	});
    }

    var populateForm = function(form,data){
    	$.each(data,function(k,v){
			form.find('[name="'+k+'"]').val(v);
			console.log(k);
		});

		if(data.bank_branch_code != undefined){
			fetchBankBranch(data.bank_code,data.bank_branch_code)
		}

		if(data.outlet_branch_code != undefined){
			fetchOutletBranch(data.outlet_code,data.outlet_branch_code)
		}
    }
    var Merchants = function(){

    	console.log('Merchants initialised');

  //   	$('.datepicker').kendoDatePicker({
		//   format: "yyyy-MM-d"
		// });


		container.on('click','.switcher',function(e){
			e.preventDefault();
		});

		tables();
		tabs();

		
		$( document ).ajaxError(function( event, request ) {

			if (request.status === 401){
				notice(request.responseText, 'danger');
				window.location.href = '/login';
			}
		});



		container.on('change','.year,.month',function(e){
	    	$('#set-btn').prop('disabled',false);
	    });

	    container.on('change','.bank',function(e){
	    	var $this = $(this);

	    	fetchBankBranch($this.val());
	    });

	    container.on('change','.outlet',function(e){
	    	var $this = $(this);

	    	fetchOutletBranch($this.val());
	    });

	    var menu_section = $('#menu_section'),
	    	menu_btn = menu_section.find('li a');

			// Check for hash value in URL
	    var hash = window.location.hash.substr(1);
	    var href = menu_btn.each(function(){
	    	var $this = $(this);
	        var href = $this.attr('href');
	        if(hash == href){
	            var toLoad = hash + ' #page_content_inner';
	            loadContent(toLoad);
	            // change menu
	            menu_section.find('li').removeClass('act_item').removeClass('current_section');
	            var li = $this.closest('li').addClass('act_item');
	            if(li.parent().parent().hasClass('submenu_trigger')){
			    	li.parent().show().parent().addClass('current_section').addClass('act_section');
			    } else {
			    	$this.closest('li').addClass('current_section');
			    }
	        } 
	    });
	     
	    menu_section.on('click','li a',function(e){
			e.preventDefault();

			var $this = $(this);

			if($this.closest('li').hasClass('submenu_trigger')){
				return false;
			}

			menu_section.find('li').removeClass('act_item').removeClass('current_section');
	     

		    var li = $this.closest('li').addClass('act_item');
		    if(li.parent().parent().hasClass('submenu_trigger')){
		    	li.parent().show().parent().addClass('current_section').addClass('act_section');
		    } else {
		    	$this.closest('li').addClass('current_section');
		    }
		    
		    var toLoad = $(this).attr('href')+' #page_content_inner';

		    $('#page_content_inner').hide('fast',loadContent(toLoad));
		    
		    window.location.hash = $this.attr('href');
		    return false;
	     
	    });

    	container.on('click','a.activate',function(e){
    		e.preventDefault();
    		
    		var $this = $(this),
    			url = $this.attr('href'),
    			l = $this.find('i'),
    			toClass = 'uk-icon-check';


			swapClass(l, 'uk-icon-check uk-icon-ban','uk-icon-circle-o-notch uk-icon-spin');

			$.get(url,function(response){
				console.log(response);
				var res = JSON.parse(response);

				if( res.status == 1 ){
					toClass = (res.active)? 'uk-icon-check': 'uk-icon-ban';

					if(res.active == undefined){
						$this.closest('td').append('<i class="uk-icon-ban"></i>');
						$this.remove();
					}
					
					notice(res.message,'success');
				}
			}).error(function(error,text,errorThrown){
            	notice(errorThrown,'danger');
            }).always(function(){
				swapClass(l,'uk-icon-circle-o-notch uk-icon-spin',toClass);
            });
    	});


        container.on('click','a.edit',function(e){
            e.preventDefault();

            var $this = $(this),
            	url = $this.attr('href'),
            	l = $this.find('i'),
            	modal = UIkit.modal('#edit-modal'),
            	form = $('#edit-form');

			swapClass(l,'uk-icon-edit','uk-icon-spin uk-icon-circle-o-notch');
            $.get(url,'',function(response){
            	var res = response;
				
				// populate form
				populateForm(form,res);

				console.log(response);

				modal.show();


            }).always(function(){
				swapClass(l,'uk-icon-spin uk-icon-circle-o-notch','uk-icon-edit');
            }).error(function(error,text,errorThrown){
            	notice(errorThrown,'danger');
            });

        });


    	container.on('click','a.view',function(e){
    		e.preventDefault();
    		
    		var $this = $(this),
    			href = $this.attr('href') + ' #page_content_inner .md-card',
    			l = $this.find('i'),
    			modal = UIkit.modal("#view-modal");

    		swapClass(l,'uk-icon-info','uk-icon-spin uk-icon-circle-o-notch');
    		$('#view-modal .uk-modal-body').load(href,'',function(response){
    			modal.show();
    			swapClass(l,'uk-icon-spin uk-icon-circle-o-notch','uk-icon-info');
    		});
    	});



    	$(document).on('click','a.view-on-container',function(e){
			e.preventDefault();

			var $this = $(this),
				href = $this.attr('href'),
				toLoad = href + ' #page_content_inner';

			loadContent(toLoad);
		});

		

        container.on('click','a.response',function(e){
        	e.preventDefault();

        	var $this = $(this),
        		href = $this.attr('href'),
        		toLoad = href + ' #approve-form',
        		l = $this.find('i'),
        		modal = UIkit.modal('#response-modal');

        	swapClass(l,'uk-icon-edit','uk-icon-spin uk-icon-circle-o-notch');
        	$('#response-modal .uk-modal-body').load(toLoad,'',function(response){
    			modal.show();
    			swapClass(l,'uk-icon-spin uk-icon-circle-o-notch','uk-icon-edit');
    		});
        });

        container.on('click','#approve-form [name="status"]',function(e){
        	e.preventDefault();


        	var $this = $(this),
        		form = $this.closest('form'),
        		data = form.serializeArray(),
				modal = UIkit.modal('#edit-modal'),
				l = Ladda.create($this[0]);


        	var options = {
				beforeSubmit:function(){
					l.start();
				},
				data: {
					status: $this.val()
				},
				success: function(response){
					var res = JSON.parse(response);

					if(res.status == 1){
						notice(res.message,'success');
						modal.hide();
						loadContent(res.url + ' #page_content_inner');
					}
					if(!$.isEmptyObject(res.errors)){
						$.each(res.errors,function(k,v){
							notice(v[0],'danger');
						});
					}
					console.log(response);
					l.stop();
				},
				error:function(error,text,errorThrown){
					notice(errorThrown,'danger');
					l.stop();
				}
			};

			form.ajaxSubmit(options);
        });

        container.on('submit','#edit-form',function(e){
			e.preventDefault();

			var form = $(this),
				modal = UIkit.modal('#edit-modal'),
				l = Ladda.create(form.find('button[type="submit"]')[0]);

			var options = {
				beforeSubmit:function(){
					l.start();
				},
				success: function(response){
					var res = JSON.parse(response);

					if(res.status == 1){
						notice(res.message,'success');
						modal.hide();
						loadContent(res.url + ' #page_content_inner');
					}
					if(!$.isEmptyObject(res.errors)){
						$.each(res.errors,function(k,v){
							console.log(v[0]);
							notice(v[0],'danger');
						});
					}
					console.log(response);
					l.stop();
				},
				error:function(error,text,errorThrown){
					notice(errorThrown,'danger');
					l.stop();
				}
			};

			form.ajaxSubmit(options);
		});

		container.on('submit','#add-form',function(e){
			e.preventDefault();
			console.log($(this).serialize())
			var form = $(this),
				modal = UIkit.modal('#add-modal'),
				l = Ladda.create(form.find('button[type="submit"]')[0]);

			var options = {
				beforeSubmit:function(){
					l.start()
				},
				success: function(response){
					var res = JSON.parse(response);

					if(res.status == 1){
						notice(res.message,'success');
						modal.hide();
						loadContent(res.url + ' #page_content_inner');
					}
					if(!$.isEmptyObject(res.errors)){
						$.each(res.errors,function(k,v){
							notice(v[0],'danger');
						});
					}
					console.log(response);
					l.stop();
				},
				error:function(error,text,errorThrown){
					notice(errorThrown,'danger');
					l.stop();
				}
			};

			form.ajaxSubmit(options)
		});

		container.on('submit','#filter-form',function(e){
			e.preventDefault();
			
			var form = $(this),
				data = form.serialize(),
				action = form.attr('action'),
				url = action + '?' + data,
				toLoad = url + ' #page_content_inner';

			
			loadContent(toLoad);
			console.log(url);

		});



		container.on('click','a.delete',function(e){
			e.preventDefault();
			var $this = $(this),
				tr = $this.closest('tr'),
				url = $this.attr('href'),
				l = $this.find('i'),
				_token = $('[name="_token"]').val();

			UIkit.modal.confirm("Are you sure you want to delete?", function(){
				swapClass(l,'uk-icon-trash-o','uk-icon-spin uk-icon-circle-o-notch');
				$.post(url,{_token:_token},function(response){
					var res = JSON.parse(response);

					console.log(response);

					if(res.status == 1){
						tr.remove();
						notice(res.message,'success');
					} else {
						notice(res.message,'danger');
					}
				}).error(function(error,text,errorThrown){
					notice(errorThrown,'danger');
				}).always(function(){
					swapClass(l,'uk-icon-spin uk-icon-circle-o-notch','uk-icon-trash-o');
				});
			});
		});


		container.on('click','a.view-checks',function(e){
			e.preventDefault();

			var $this = $(this),
				href = $this.attr('href'),
				data = $('#filter-form').serialize(),
				url = href + '?' + data,
				toLoad = url + ' #page_content_inner';

			modal.find('.modal-body').load(toLoad,'',function(response){
				modal.show();
			});
		});


		container.on('click','#leave-sub-menu-form button[type="submit"]',function(e){
			e.preventDefault();

			var $this = $(this),
				form = $this.closest('form'),
				data = $this.val(),
				action = form.attr('action'),
				url = action + '?leave=' + data,
				toLoad = url + ' #page_content_inner';

			
			loadContent(toLoad);
			console.log(url);

		});

		container.on('submit','#date-filter, #amount-filter, #purchases-filter',function(e){
			e.preventDefault();

			var form = $(this),
				data = form.serialize(),
				href = window.location.hash.substring(1),
				url = href + '?' + data,
				toLoad = url + ' #page_content_inner';

			loadContent(toLoad);
		});

		container.on('submit','#set-target-form',function(e){
			e.preventDefault();

			var $this = $(this),
				l = Ladda.create($this.find('button[type="submit"]')[0]);

			var options = {
				beforeSubmit: function(){
					l.start();
				},
				success: function(response){
					var res = JSON.parse(response);
					console.log(response);

					if(res.status == 1){
						notice(res.message,'success');
						l.stop();
						loadContent(res.url + ' #page_content_inner');
					}

					if(!$.isEmptyObject(res.errors)){
						$.each(res.errors,function (k,v) {
							notice(v[0],'danger');
						});
					}
				},
				error: function(error,text,errorThrown){
					notice(errorThrown,'danger');
				}
			}

			$this.ajaxSubmit(options);
		});


		container.on('change','#all_selector',function(e){
			e.preventDefault();

			var $this = $(this),
				promoterSelect = $('#promoters'); 

			if ($this.is(':checked')) {
				promoterSelect.prop('disabled',true);
			} else {
				promoterSelect.prop('disabled',false);
			}

			console.log($this.is(':checked'));
		});

		container.on('click','a.load',function(e){
			e.preventDefault();

			var $this = $(this),
				href = $this.attr('href'),
				toLoad = href + ' #page_content_inner';

			loadContent(toLoad);
		});


		container.on('change', '.checker', function(e){
			var $this = $(this),
				url = '/user/set-permission',
				data = {
					_token: $('[name="_token"]').val(),
					id: $this.data('id'),
					type: $this.data('type'), // this is the type of permission
					value: ($this.is(":checked"))? 1: 0
				}


			$.post(url, data, function(res){
				notice(res.message);
			}).fail(function(error, text, errorThrown){
				notice(errorThrown);
			});
		});





    }

    Merchants();
});