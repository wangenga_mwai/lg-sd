<?php 


	$title = 'Outlets';

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<h3 class="heading_b uk-margin-bottom">{{ $title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        @if(Auth::user()->admin)<button type="button" class="uk-button uk-button-primary" data-uk-modal="{target:'#add-modal'}">Add Outlet</button>@endif
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                <table class="uk-table uk-table-striped">
                	<thead>
                        <th>Code</th>
                        <th>Title</th>
                		<th>Branches</th>
                		@if(Auth::user()->admin)<th>Action</th>@endif
                	</thead>
                	<tbody>
                		@foreach($outlets as $key => $outlet)
                		<tr>
                            <td>{{ $outlet->code }}</td>
                            <td>{{ $outlet->title }}</td>
                			<td><a title="View branches" href="/outlet/{{ $outlet->code }}/branches" class="load"><span class="uk-badge uk-badge-notification uk-badge-blue">{{ count($outlet->branches) }}</span></a></td>
                			@if(Auth::user()->admin)
                            <td>
                                <a href="/outlet/activate/{{ $outlet->id }}" class="activate md-btn action-btn md-btn-warning"><i class="uk-icon-{{ ($outlet->active)? 'check': 'ban' }}" ></i></a>
                                <a href="/outlet/edit/{{ $outlet->id }}" class="edit md-btn action-btn md-btn-success" ><i class="uk-icon-edit" ></i></a>
                                <a href="/outlet/delete/{{ $outlet->id }}" class="delete md-btn action-btn md-btn-danger" ><i class="uk-icon-trash-o" ></i></a>
                            </td>
                            @endif
                		</tr>
                		@endforeach
                	</tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="uk-modal" id="edit-modal">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Edit Outlet</h3>
        </div>
        <form method="post" action="/outlet/edit" id="edit-form" class="uk-form uk-form-horizontal">
            {!! csrf_field() !!}
            <input type="hidden" name="id" value="" >
            <div class="uk-form-row">
                <label class="uk-form-label" for="code">Code</label>
                <div class="uk-form-controls">
                    <input type="text" name="code" disabled >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="title">Title</label>
                <div class="uk-form-controls">
                    <input type="text" name="title" >
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary ladda-button" data-style="expand-right"><span class="ladda-label">Save Changes</span></button>
            </div>
        </form>
    </div>
</div>
<div class="uk-modal" id="add-modal">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Add Outlet</h3>
        </div>
        <form method="post" action="/outlet/add" id="add-form" class="uk-form uk-form-horizontal">
            {!! csrf_field() !!}
            <div class="uk-form-row">
                <label class="uk-form-label" for="title">Title</label>
                <div class="uk-form-controls">
                    <input type="text" name="title" >
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary ladda-button" data-style="expand-right"><span class="ladda-label">Save</span></button>
            </div>
        </form>
    </div>
</div>
@endsection