<?php 


	$title = 'Customers';

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<h3 class="heading_b uk-margin-bottom">{{ $title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        <a href="/customer/csv" class="uk-button uk-button-primary" ><i class="uk-icon-file-excel-o"></i> Export</a>
        <div>
            <fieldset>
                <legend>Filters</legend>
                <a href="/customers?phone=1" class="load uk-button uk-button-primary" >With phone number</a>
                <a href="/customers?phone=0" class="load uk-button uk-button-primary" >Without phone number</a>
                <a href="/customers?email=1" class="load uk-button uk-button-primary" >With Email</a>
                <a href="/customers?email=0" class="load uk-button uk-button-primary" >Without Email</a>
                @include('inc.date_filter')
                <div class="uk-grid">
                    <div class="uk-width-1-2">
                        Number of Purchases
                        @include('inc.number_of_purchases_filter')
                    </div>
                    <div class="uk-width-1-2">
                        Amount
                        @include('inc.amount_filter')
                    </div>
                
                
                </div>
            </fieldset>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                <table class="uk-table uk-table-striped">
                	<thead>
                        <th>Name</th>
                		<th>ID Number</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Number of Purchases</th>
                        <th>Value of Purchases</th>
                		<th>Action</th>
                	</thead>
                	<tbody>
                		@foreach($customers as $key => $customer)
                		<tr>
                            <td>{{ $customer->first_name }} {{ $customer->last_name }} </td>
                            <td>{{ $customer->id_number }}</td>
                            <td>{{ $customer->phone }}</td>
                            <td>{{ $customer->email }}</td>
                            <td>{{ $customer->no_of_purchases }}</td>
                			<td>{{ number_format($customer->value_of_purchases) }}</td>
                			<td>
                                <a title="View sales" href="/customer/sales/{{ $customer->id }}" class="view md-btn action-btn md-btn-info"><i class="uk-icon-info" ></i></a>
                            </td>
                		</tr>
                		@endforeach
                	</tbody>
                </table>
            </div>
        </div>
    </div>
</div>
{{-- <div class="uk-modal" id="edit-modal">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Edit Customer</h3>
        </div>
        <form method="post" action="/customer/edit" id="edit-form" class="uk-form uk-form-horizontal">
            {!! csrf_field() !!}
            <input type="hidden" name="id" value="" >
            <div class="uk-form-row">
                <label class="uk-form-label" for="first_name">First Name</label>
                <div class="uk-form-controls">
                    <input type="text" name="first_name" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="last_name">Last Name</label>
                <div class="uk-form-controls">
                    <input type="text" name="last_name" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="id_number">ID Number</label>
                <div class="uk-form-controls">
                    <input type="text" name="id_number" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="phone">Phone</label>
                <div class="uk-form-controls">
                    <input type="text" name="phone" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="email">Email</label>
                <div class="uk-form-controls">
                    <input type="email" name="email" >
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary ladda-button" data-style="expand-right"><span class="ladda-label">Save Changes</span></button>
            </div>
        </form>
    </div>
</div>
<div class="uk-modal" id="add-modal">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Add Customer</h3>
        </div>
        <form method="post" action="/customer/add" id="add-form" class="uk-form uk-form-horizontal">
            {!! csrf_field() !!}
            <div class="uk-form-row">
                <label class="uk-form-label" for="first_name">First Name</label>
                <div class="uk-form-controls">
                    <input type="text" name="first_name" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="last_name">Last Name</label>
                <div class="uk-form-controls">
                    <input type="text" name="last_name" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="id_number">ID Number</label>
                <div class="uk-form-controls">
                    <input type="text" name="id_number" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="phone">Phone</label>
                <div class="uk-form-controls">
                    <input type="text" name="phone" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="email">Email</label>
                <div class="uk-form-controls">
                    <input type="email" name="email" >
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary ladda-button" data-style="expand-right"><span class="ladda-label">Save</span></button>
            </div>
        </form>
    </div>
</div> --}}
<div class="uk-modal" id="view-modal">
    <div class="uk-modal-dialog uk-modal-dialog-large">
        <div class="uk-modal-header">
            <a class="uk-modal-close uk-close uk-float-right"></a>
            <h3>Customer Sales</h3>
        </div>
        <div class="uk-modal-body uk-overflow-container">
        </div>
    </div>
</div>
@endsection