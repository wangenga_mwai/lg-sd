<?php 


	$title = 'Customer Sales';

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<h3 class="heading_b uk-margin-bottom">{{ $title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                <table class="uk-table uk-table-striped uk-table-bordered">
                    <thead>
                        <th>Date</th>
                        <th>Promoter</th>
                        <th>Merchandiser</th>
                        <th>Outlet</th>
                        <th>Branch</th>
                        <th>Category</th>
                        <th>Sub-Category</th>
                        <th>Model</th>
                        <th>Amount</th>
                    </thead>
                    <tbody>
                        @foreach( $customer->sales as $key => $sale )
                        <tr>
                            <td>{{ date('Y-m-d', strtotime($sale->updated_at)) }}</td>
                            <td>{{ $sale->promoter->first_name . ' ' . $sale->promoter->last_name }}</td>
                            <td>{{ $sale->merchandiser->first_name . ' ' . $sale->merchandiser->last_name }}</td>
                            <td>{{ ($sale->outlet)? $sale->outlet->title : '*' }}</td>
                            <td>{{ ($sale->outlet_branch)? $sale->outlet_branch->title : '*' }}</td>
                            <td>{{ $sale->category->title }}</td>
                            <td>{{ $sale->sub_category->title }}</td>
                            <td>{{ $sale->model->title }}</td>
                            <td class="number">{{ number_format($sale->amount) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection