<?php

	$title = 'User';
	$sub_title = 'Change Password';
?>
@extends('layouts.master')

@section('title', $title )

@section('content')
<div class="md-card" id="change-password">
	<div class="md-card-content large-padding">
        
        <form action="/user/change-password" method="post" id="add-form">
            {!! csrf_field() !!}
            <input type="hidden" name="email" value="{{ Auth::user()->email }}" />
            <div class="uk-form-row">
                <label for="email">Email</label>
                <input class="md-input" type="email" disabled value="{{ Auth::user()->email }}" />
            </div>
            <div class="uk-form-row">
                <label for="old_password">Old Password</label>
                <input class="md-input" type="password" id="old_password" name="old_password" autocomplete="off" />
            </div>
            <div class="uk-form-row">
                <label for="password">Password</label>
                <input class="md-input" type="password" id="password" name="password" autocomplete="off" />
            </div>
            <div class="uk-form-row">
                <label for="confirm_password">Confirm password</label>
                <input class="md-input" type="password" id="password_confirm" name="confirm_password" autocomplete="off" />
            </div>
            <div class="uk-margin-medium-top">
                <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large ladda-button" data-style="expand-right"><span class="ladda-label">Change password</span></button>
            </div>
        </form>
    </div>
</div>
@endsection