<?php 


	$title = 'Users Permissions';


?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<h3 class="heading_b uk-margin-bottom">{{ $title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        @if(Auth::user()->admin)<button type="button" class="uk-button uk-button-primary" data-uk-modal="{target:'#add-modal'}">Add User</button>@endif
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                <table class="uk-table uk-table-striped">
                	<thead>
                		<th>Username</th>
                		@if(Auth::user()->admin)
                		<th>Admin</th>
                		<th>Sales Approver</th>
                		<th>FSM Approver</th>
                		@endif
                	</thead>
                	<tbody>
                		@foreach($users as $key => $user)
                		<tr>
                			<td>{{ $user->username }}</td>
                            @if(Auth::user()->admin)
                            <td><input type="checkbox" data-switchery data-switchery-size="small" class="checker" data-id="{{ $user->id }}" data-type="admin" {{ ($user->admin)? 'checked': '' }} /></td>
                            <td><input type="checkbox" data-switchery data-switchery-size="small" class="checker" data-id="{{ $user->id }}" data-type="approver" {{ ($user->approver)? 'checked': '' }} /></td>
                            <td><input type="checkbox" data-switchery data-switchery-size="small" class="checker" data-id="{{ $user->id }}" data-type="fsm_approver" {{ ($user->fsm_approver)? 'checked': '' }} /></td>
                            @endif
                		</tr>
                		@endforeach
                	</tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="uk-modal" id="add-modal">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Add Category</h3>
        </div>
        <form method="post" action="/user" id="add-form" class="uk-form uk-form-horizontal">
            {!! csrf_field() !!}
            <div class="uk-form-row">
                <label class="uk-form-label" for="username">Username</label>
                <div class="uk-form-controls">
                    <input type="text" name="username" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="email">Email</label>
                <div class="uk-form-controls">
                    <input type="text" name="email" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label">Permissions</label>
                <div class="uk-form-controls">
                    <input type="checkbox" name="admin" value="admin" > <label>Admin</label><br>
                    <input type="checkbox" name="approver" value="approver"> <label>Approver</label><br>
                    <input type="checkbox" name="fsm_approver" value="fsm_approver"> <label>FSP Approver</label>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary ladda-button" data-style="expand-right"><span class="ladda-label">Save</span></button>
            </div>
        </form>
    </div>
</div>
{{ csrf_field() }}
@endsection