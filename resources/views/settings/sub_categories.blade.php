<?php

	$title = 'Sub-Categories';
?>
@extends('layouts.master')

@section('title',$title)

@section('content')
<h3 class="heading_b uk-margin-bottom">{{ $title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        @if(Auth::user()->admin)<button type="button" class="uk-button uk-button-primary" data-uk-modal="{target:'#add-modal'}">Add Sub category</button>@endif
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                <table class="uk-table uk-table-striped">
                	<thead>
                		<th>Title</th>
                		<th>Category</th>
                		@if(Auth::user()->admin)<th>Action</th>@endif
                	</thead>
                	<tbody>
                		@foreach($sub_categories as $key => $sub_category)
                		<tr>
                			<td>{{ $sub_category->title }}</td>
                			<td>{{ $sub_category->category->title }}</td>
                            @if(Auth::user()->admin)
                			<td>
                                <a href="/sub_category/activate/{{ $sub_category->id }}" class="activate md-btn action-btn md-btn-warning"><i class="uk-icon-{{ ($sub_category->active)? 'check': 'ban' }}" ></i></a>
                                <a href="/sub_category/edit/{{ $sub_category->id }}" class="edit md-btn action-btn md-btn-success" ><i class="uk-icon-edit" ></i></a>
                                <a href="/sub_category/delete/{{ $sub_category->id }}" class="delete md-btn action-btn md-btn-danger" ><i class="uk-icon-trash-o" ></i></a>
                            </td>
                            @endif
                		</tr>
                		@endforeach
                	</tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="uk-modal" id="edit-modal">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Edit Category</h3>
        </div>
        <form method="post" action="/sub_category/edit" id="edit-form" class="uk-form uk-form-horizontal">
            {!! csrf_field() !!}
            <input type="hidden" name="id" value="" >
            <div class="uk-form-row">
                <label class="uk-form-label" for="title">Title</label>
                <div class="uk-form-controls">
                    <input type="text" name="title" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="category">Cateogry</label>
                <div class="uk-form-controls">
                    <select name="category_id" class="category">
                        @foreach($categories as $category)
                        <option value="{{ $category->id }}" >{{ $category->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary ladda-button" data-style="expand-right"><span class="ladda-label">Save Changes</span></button>
            </div>
        </form>
    </div>
</div>
<div class="uk-modal" id="add-modal">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Add Category</h3>
        </div>
        <form method="post" action="/sub_category/add" id="add-form" class="uk-form uk-form-horizontal">
            {!! csrf_field() !!}
            <div class="uk-form-row">
                <label class="uk-form-label" for="title">Title</label>
                <div class="uk-form-controls">
                    <input type="text" name="title" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="category">Cateogry</label>
                <div class="uk-form-controls">
                    <select name="category_id" class="category">
                        @foreach($categories as $category)
                        <option value="{{ $category->id }}" >{{ $category->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary ladda-button" data-style="expand-right"><span class="ladda-label">Save</span></button>
            </div>
        </form>
    </div>
</div>
@endsection