<?php

    $title = 'Models';

    // echo json_encode($models);
    // echo json_encode($models[0]->sub_category->category);
    // die();
?>
@extends('layouts.master')

@section('title',$title)

@section('content')
<h3 class="heading_b uk-margin-bottom">{{ $title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        @if(Auth::user()->admin)<button type="button" class="uk-button uk-button-primary" data-uk-modal="{target:'#add-modal'}">Add Model</button>@endif
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                <table class="uk-table uk-table-striped uk-table-condensed table-search">
                    <thead>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Sub-Category</th>
                        <th>Group</th>
                        <th>Price</th>
                        <th>Commission</th>
                        <th>Additional Commission</th>
                        @if(Auth::user()->admin)<th>Action</th>@endif
                    </thead>
                    <tbody>
                        @foreach($models as $key => $model)
                        <tr>
                            <td>{{ $model->title }}</td>
                            <td>{{ (count($model->category)) ? $model->category->title : 'error'  }}</td>
                            <td>{{ (count($model->sub_category)) ? $model->sub_category->title : 'error' }}</td>
                            <td>{{ (count($model->group)) ? $model->group->title : 'error' }}</td>
                            <td class="number">{{ number_format(@$model->stock_unit->price) }}</td>
                            <td class="number">{{ number_format(@$model->stock_unit->commission, 2) }}</td>
                            <td class="number">{{ number_format(@$model->stock_unit->additional_commission) }}</td>
                            @if(Auth::user()->admin)
                            <td class="action">
                                <a href="/model/activate/{{ $model->id }}" class="activate md-btn action-btn md-btn-warning"><i class="uk-icon-{{ ($model->active)? 'check': 'ban' }}" ></i></a>
                                <a href="/model/edit/{{ $model->id }}" class="edit md-btn action-btn md-btn-success" ><i class="uk-icon-edit" ></i></a>
                                <a href="/model/delete/{{ $model->id }}" class="delete md-btn action-btn md-btn-danger" ><i class="uk-icon-trash-o" ></i></a>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="uk-modal" id="edit-modal">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Edit Model</h3>
        </div>
        <form method="post" action="/model/edit" id="edit-form" class="uk-form uk-form-horizontal">
            {!! csrf_field() !!}
            <input type="hidden" name="id" value="" >
            <div class="uk-form-row">
                <label class="uk-form-label" for="title">Title</label>
                <div class="uk-form-controls">
                    <input type="text" name="title" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="category">Cateogry</label>
                <div class="uk-form-controls">
                    <select name="category_id" class="category">
                        @foreach($categories as $category)
                        <option value="{{ $category->id }}" >{{ $category->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="sub_category">Sub cateogry</label>
                <div class="uk-form-controls">
                    <select name="sub_category_id" class="sub_category">
                        @foreach($sub_categories as $sub_category)
                        <option value="{{ $sub_category->id }}" >{{ $sub_category->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="group">Group</label>
                <div class="uk-form-controls">
                    <select name="group_id" class="group">
                        @foreach($groups as $group)
                        <option value="{{ $group->id }}" >{{ $group->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="price">Price</label>
                <div class="uk-form-controls">
                    <input type="text" name="price" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="commission">Commission</label>
                <div class="uk-form-controls">
                    <input type="text" name="commission" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="additional_commission">Additional Commission</label>
                <div class="uk-form-controls">
                    <input type="text" name="additional_commission" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="description">Description</label>
                <div class="uk-form-controls">
                    <textarea name="description" ></textarea>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary ladda-button" data-style="expand-right"><span class="ladda-label">Save Changes</span></button>
            </div>
        </form>
    </div>
</div>
<div class="uk-modal" id="add-modal">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Add Model</h3>
        </div>
        <form method="post" action="/model/add" id="add-form" class="uk-form uk-form-horizontal">
            {!! csrf_field() !!}
            <div class="uk-form-row">
                <label class="uk-form-label" for="title">Title</label>
                <div class="uk-form-controls">
                    <input type="text" name="title" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="category">Cateogry</label>
                <div class="uk-form-controls">
                    <select name="category_id" class="category">
                        @foreach($categories as $category)
                        <option value="{{ $category->id }}" >{{ $category->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="sub_category">Sub cateogry</label>
                <div class="uk-form-controls">
                    <select name="sub_category_id" class="sub_category">
                        @foreach($sub_categories as $sub_category)
                        <option value="{{ $sub_category->id }}" >{{ $sub_category->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="group">Group</label>
                <div class="uk-form-controls">
                    <select name="group_id" class="group">
                        @foreach($groups as $group)
                        <option value="{{ $group->id }}" >{{ $group->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="price">Price</label>
                <div class="uk-form-controls">
                    <input type="text" name="price" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="commission">Commission</label>
                <div class="uk-form-controls">
                    <input type="text" name="commission" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="additional_commission">Additional Commission</label>
                <div class="uk-form-controls">
                    <input type="text" name="additional_commission" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="description">Description</label>
                <div class="uk-form-controls">
                    <textarea name="description" ></textarea>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary ladda-button" data-style="expand-right"><span class="ladda-label">Save</span></button>
            </div>
        </form>
    </div>
</div>
@endsection