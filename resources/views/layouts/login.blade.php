<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="/assets/img/favicon.png">

    <title>{{env('COUNTRY_NAME')}} Merchant Login</title>

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>

    <!-- uikit -->
    <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css"/>

    <!-- altair admin login page -->
    <link rel="stylesheet" href="assets/css/login_page.css" />
    <link rel="stylesheet" href="assets/plugins/bootstrap-loading/lada.min.css" />
    <style type="text/css">
        html { 
          background: url(/assets/img/gallery/sudan.PNG) no-repeat center center fixed; 
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
        }
    </style>
</head>
<body class="login_page">

    <div class="login_page_wrapper">
        @yield('content')
    </div>
    <div class="clearfix"></div>
    <footer class="footer">
        Copyright &copy; {{ date('Y') }} LG. All rights reserved.
        <br>
        <a href="#" class="uk-text-warning">Privacy Policy.</a>
        <a href="#" class="uk-text-warning">Term and Conditions</a>
    </footer>

    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- altair core functions -->
    <script src="assets/js/altair_admin_common.min.js"></script>

    <!-- altair login page functions -->
    <script src="assets/js/pages/login.js"></script>
    <script src="assets/plugins/bootstrap-loading/lada.min.js"></script>

</body>
</html>