<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="/assets/img/favicon.png">

    <title>@yield('title') &middot; {{env('COUNTRY_NAME')}} Merchant System</title>


    <!-- uikit -->
    <link rel="stylesheet" href="/bower_components/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="/assets/icons/flags/flags.min.css" media="all">

    <!-- altair admin -->
    <link rel="stylesheet" href="/assets/css/main.css" media="all">
    <link rel="stylesheet" href="/assets/css/lada.min.css" media="all">
    <link rel="stylesheet" href="/assets/css/dataTables.uikit.min.css" media="all">
    <link rel="stylesheet" href="/assets/css/merchants.css" media="all">

    <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.js"></script>
        <script type="text/javascript" src="bower_components/matchMedia/matchMedia.addListener.js"></script>
    <![endif]-->
    <!-- google web fonts -->
    <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>

   <!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4PtMO2Ahqq5Kx753i3aaNZ0vwccsJlx0";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
</head>
<body class=" sidebar_main_open sidebar_main_swipe">
    <!-- main header -->
    <header id="header_main">
        <div class="header_main_content">
            <nav class="uk-navbar">
                                
                <!-- main sidebar switch -->
                <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                    <span class="sSwitchIcon"></span>
                </a>
                <div class="title">
                    <img src="{{env('COUNTRY_IMG')}}" class="flag">
                    <h2 class="">LG {{env('COUNTRY_NAME')}} Merchant System</h2>
                    
                </div>
                <div class="'uk-clearfix"></div>
                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                        <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                            <a href="#" class="user_action_image"><img class="md-user-image" src="/assets/img/avatars/avatar_101.png" alt=""/></a>
                            <div class="uk-dropdown uk-dropdown-small">
                                <ul class="uk-nav js-uk-prevent">
                                    <li><a href="/user/change-password" class="view-on-container">Change password</a></li>
                                    <li><a href="/logout">Logout</a></li>
                                </ul>
                            </div>
                        </li>
                        <li></li>
                    </ul>
                </div>
            </nav>
        </div>
    </header><!-- main header end -->
    <!-- main sidebar -->
    <aside id="sidebar_main">
        
        <div class="sidebar_main_header">
            <div class="sidebar_logo">
                <a href="/" class="sSidebar_hide"><img src="/assets/img/lg_top_img.png" alt="LG Logo" height="20" width="100"/></a>
                <a href="/" class="sSidebar_show"><img src="/assets/img/small_logo.png" alt="LG Logo" height="32" width="32"/></a>
            </div>
        </div>
        
        <div class="menu_section" id="menu_section">
            <ul>
                <li title="Dashboard" {{ ($title == 'Dashboard')? 'class=current_section': '' }}>
                    <a href="/dashboard">
                        <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                        <span class="menu_title">Dashboard</span>
                    </a>
                </li>
                <li title="Settings" >
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">&#xE8C0;</i></span>
                        <span class="menu_title">Settings</span>
                    </a>
                    <ul class="menus">
                        <li {{ ($title == 'Categories')? 'class=act_item': '' }}><a href="/categories">Categories</a></li>
                        <li {{ ($title == 'Sub-Categories')? 'class=act_item': '' }}><a href="/subcategories">Sub-Categories</a></li>
                        <li {{ ($title == 'Groups')? 'class=act_item': '' }}><a href="/groups">Groups</a></li>
                        <li {{ ($title == 'Models')? 'class=act_item': '' }}><a href="/models">Models</a></li>
                    </ul>
                </li>
                <li title="Sales">
                    <a href="/sales">
                        <span class="menu_icon"><i class="material-icons">&#xE8CC;</i></span>
                        <span class="menu_title">Sales</span>
                    </a>
                </li>
                <li title="Commissions">
                    <a href="/commissions">
                        <span class="menu_icon"><i class="material-icons">&#xE85C;</i></span>
                        <span class="menu_title">Commissions</span>
                    </a>
                </li>
                <li title="Sales targets">
                    <a href="/sales/targets">
                        <span class="menu_icon"><i class="material-icons">&#xE8E5;</i></span>
                        <span class="menu_title">Sales Target</span>
                    </a>
                </li>
                <li title="Staff" >
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">&#xE8D3;</i></span>
                        <span class="menu_title">Staff</span>
                    </a>
                    <ul class="menus">
                        <li {{ (isset($sub_title) && $sub_title == 'Merchandiser')? 'class=act_item': '' }}><a href="/staff/merchandisers">Merchandisers</a></li>
                        <li {{ (isset($sub_title) && $sub_title == 'Promoters')? 'class=act_item': '' }}><a href="/staff/promoters">Promoters</a></li>
                        {{-- <li {{ (isset($sub_title) && $sub_title == 'Employees')? 'class=act_item': '' }}><a href="/staff/employees">Employees</a></li>
                        <li {{ (isset($sub_title) && $sub_title == 'Trainers')? 'class=act_item': '' }}><a href="/staff/trainers">Trainers</a></li>
                        <li {{ (isset($sub_title) && $sub_title == 'Manegers')? 'class=act_item': '' }}><a href="/staff/manegers">Managers</a></li> --}}
                    </ul>
                </li>
                <li title="Staff Management" >
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">&#xE2C9;</i></span>
                        <span class="menu_title">Staff Management</span>
                    </a>
                    <ul class="menus">
                        <li {{ ($title == 'Issue Management')? 'class=act_item': '' }}><a href="/issue-management">Issue Management</a></li>
                    </ul>
                </li>
                <li title="Leave Management" >
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons"><i class="material-icons">&#xE549;</i>;</i></span>
                        <span class="menu_title">Leave Management</span>
                    </a>
                    <ul class="menus">
                        <li {{ (isset($sub_title) && $sub_title == 'Merchandiser')? 'class=act_item': '' }}><a href="/leave/merchandisers">Merchandisers</a></li>
                        <li {{ (isset($sub_title) && $sub_title == 'Promoters')? 'class=act_item': '' }}><a href="/leave/promoters">Promoters</a></li>
                    </ul>
                </li>
                <li title="Check In/Out" >
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">&#xE536;</i></span>
                        <span class="menu_title">Check In&#47;Out</span>
                    </a>
                    <ul class="menus">
                        <li {{ (isset($sub_title) && $sub_title == 'Merchandiser' && $title == 'Check In/Out')? 'class=act_item': '' }}><a href="/check/merchandisers">Merchandisers</a></li>
                        <li {{ (isset($sub_title) && $sub_title == 'Promoters' && $title == 'Check In/Out')? 'class=act_item': '' }}><a href="/check/promoters">Promoters</a></li>
                    </ul>
                </li>
                <li title="Display Share">
                    <a href="/display-share">
                        <span class="menu_icon"><i class="material-icons">&#xE1B1;</i></span>
                        <span class="menu_title">Display Share</span>
                    </a>
                </li>
                <li title="Outlets">
                    <a href="/outlets">
                        <span class="menu_icon"><i class="material-icons">&#xE7F1;</i></span>
                        <span class="menu_title">Outlets</span>
                    </a>
                </li>
                <li title="Customers">
                    <a href="/customers">
                        <span class="menu_icon"><i class="material-icons">&#xE7F2;</i></span>
                        <span class="menu_title">Customers</span>
                    </a>
                </li>
                @if(Auth::user()->admin)
                <li title="Messages">
                    <a href="/messages">
                        <span class="menu_icon"><i class="material-icons">&#xE85A;</i></span>
                        <span class="menu_title">Messages</span>
                    </a>
                </li>
                <li title="Leads">
                    <a href="/leads">
                        <span class="menu_icon"><i class="material-icons">&#xE889;</i></span>
                        <span class="menu_title">Leads</span>
                    </a>
                </li>
                <li title="Digital Brochure">
                    <a href="/brochures">
                        <span class="menu_icon"><i class="material-icons">&#xE86E;</i></span>
                        <span class="menu_title">Digital Brochure</span>
                    </a>
                </li>
                <li title="Users' Managment">
                    <a href="/users">
                        <span class="menu_icon"><i class="material-icons">&#xE7FC;</i></span>
                        <span class="menu_title">Users' Managment</span>
                    </a>
                </li>
                @endif
            </ul>
        </div>
    </aside><!-- main sidebar end -->

<div id="page_content">
    <div id="page_content_inner">
    @yield('content')
    </div>
</div>

    

    <!-- common functions -->
    <script src="/assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="/assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="/assets/js/altair_admin_common.min.js"></script>

    <script src="/assets/js/lada.min.js"></script>
    <script src="/assets/js/jquery.form.min.js"></script>
    <script src="/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js"></script>
    <script src="/assets/js/pages/forms_advanced.min.js"></script>
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/dataTables.uikit.min.js"></script>


    <script>
        $(function() {
            // enable hires images
            altair_helpers.retina_images();
            // fastClick (touch devices)
            if(Modernizr.touch) {
                FastClick.attach(document.body);
            }
        });
    </script>


    <div id="style_switcher">
        <div id="style_switcher_toggle"><i class="material-icons">&#xE8B8;</i></div>
        <div class="uk-margin-medium-bottom">
            <h4 class="heading_c uk-margin-bottom">Colors</h4>
            <ul class="switcher_app_themes" id="theme_switcher">
                <li class="app_style_default active_theme" data-app-theme="">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_a" data-app-theme="app_theme_a">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_b" data-app-theme="app_theme_b">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_c" data-app-theme="app_theme_c">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_d" data-app-theme="app_theme_d">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_e" data-app-theme="app_theme_e">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_f" data-app-theme="app_theme_f">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_g" data-app-theme="app_theme_g">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
            </ul>
        </div>
        <div class="uk-visible-large uk-margin-medium-bottom">
            <h4 class="heading_c">Sidebar</h4>
            <p>
                <input type="checkbox" name="style_sidebar_mini" id="style_sidebar_mini" data-md-icheck />
                <label for="style_sidebar_mini" class="inline-label">Mini Sidebar</label>
            </p>
        </div>
        <div class="uk-visible-large">
            <h4 class="heading_c">Layout</h4>
            <p>
                <input type="checkbox" name="style_layout_boxed" id="style_layout_boxed" data-md-icheck />
                <label for="style_layout_boxed" class="inline-label">Boxed layout</label>
            </p>
        </div>
    </div>

    <script>
        $(function() {
            var $switcher = $('#style_switcher'),
                $switcher_toggle = $('#style_switcher_toggle'),
                $theme_switcher = $('#theme_switcher'),
                $mini_sidebar_toggle = $('#style_sidebar_mini'),
                $boxed_layout_toggle = $('#style_layout_boxed'),
                $body = $('body');


            $switcher_toggle.click(function(e) {
                e.preventDefault();
                $switcher.toggleClass('switcher_active');
            });

            $theme_switcher.children('li').click(function(e) {
                e.preventDefault();
                var $this = $(this),
                    this_theme = $this.attr('data-app-theme');

                $theme_switcher.children('li').removeClass('active_theme');
                $(this).addClass('active_theme');
                $body
                    .removeClass('app_theme_a app_theme_b app_theme_c app_theme_d app_theme_e app_theme_f app_theme_g')
                    .addClass(this_theme);

                if(this_theme == '') {
                    localStorage.removeItem('altair_theme');
                } else {
                    localStorage.setItem("altair_theme", this_theme);
                }

            });

            // hide style switcher
            $document.on('click keyup', function(e) {
                if( $switcher.hasClass('switcher_active') ) {
                    if (
                        ( !$(e.target).closest($switcher).length )
                        || ( e.keyCode == 27 )
                    ) {
                        $switcher.removeClass('switcher_active');
                    }
                }
            });

            // get theme from local storage
            if(localStorage.getItem("altair_theme") !== null) {
                $theme_switcher.children('li[data-app-theme='+localStorage.getItem("altair_theme")+']').click();
            }


        // toggle mini sidebar

            // change input's state to checked if mini sidebar is active
            if((localStorage.getItem("altair_sidebar_mini") !== null && localStorage.getItem("altair_sidebar_mini") == '1') || $body.hasClass('sidebar_mini')) {
                $mini_sidebar_toggle.iCheck('check');
            }

            $mini_sidebar_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_sidebar_mini", '1');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_sidebar_mini');
                    location.reload(true);
                });


        // toggle boxed layout

            // change input's state to checked if mini sidebar is active
            if((localStorage.getItem("altair_layout") !== null && localStorage.getItem("altair_layout") == 'boxed') || $body.hasClass('boxed_layout')) {
                $boxed_layout_toggle.iCheck('check');
                $body.addClass('boxed_layout');
                $(window).resize();
            }

            // toggle mini sidebar
            $boxed_layout_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_layout", 'boxed');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_layout');
                    location.reload(true);
                });


        });
    </script>
    <!-- peity (small charts) -->
    <script src="/bower_components/peity/jquery.peity.min.js"></script>
    <script type="text/javascript" src="/assets/js/merchants.js"></script>
    
</body>
</html>