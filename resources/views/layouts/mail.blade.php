<!DOCTYPE html>
<html>
<head>
	<title>LG Merchants</title>
	<style type="text/css">
	body {
		background: #ffdada;
		font-family: georgia, 'Comic Sans MS', Arial;
		padding-top: 40px;
		padding-bottom: 40px;

	}
	.content, .img {
		margin: 0 auto;
		width: 600px;
		background: #ffffff;
		
	}
	.content{
		padding: 50px;
		border-radius: 0 0 20px 20px;
	}
	.header{
		margin: auto;
		text-align: center;
	}
	.title{
		background: #b12121;
		margin: 0 auto;
		width: 600px;
		border-radius: 20px 20px 0 0;
		padding: 1px 50px;
		color: #ffffff;
		position: relative;
	}
	.title small {
		float: right;
		position: absolute;
		top: 20px;
		right: 20px;
	}
	.img {
		border-bottom: 20px solid #ffdada;
		text-align: center;
		padding: 20px 50px
	}
	.sign-out {
		padding-top: 40px; 
	}
	footer{
		text-align: center;
		color: #b12121;
		padding-top: 20px; 
	}
	</style>
</head>
<body>
	<div class="title">
	<h3>LG Merchant System</h3>
	<small>For enquiries: 0800 - 545454</small>
	</div>
	<div class="img">
			
			<img src="{{ $message->embed(public_path().'/assets/img/lg_top_img.png') }}">
		</div>
<div class="content">
		
		
	<div class="body">
		@yield('content')
	</div>
	<div class="sign-out">
		<p>Thanks for your continued support towards LG.</p>

		<p>Thanks,<br>Head of In-store Management<br><b>Leney Varghese</b>.

		<p>This E-Mail has been sent automatically,<br>
		Please do not respond to this E-Mail.</p>
	</div>
	
	<footer>COPYRIGHT &copy; {{ date('Y') }} LG Electronics. All rights reserved.</footer>
</div>
</body>
</html>