<?php

$title = 'Display Share';

?>
@extends('layouts.master')

@section('content')
<h3 class="heading_b uk-margin-bottom">{{ $title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                <div class="date-filter">
                    <form method="get" class="uk-form uk-form-horizontal" id="date-filter">

                    <div class="uk-input-group">
                        <label>Year </label>
                        <select name="year">
                            @foreach($available_years as $key => $year)
                            <option value="{{ $year->year }}" {{ $year->year == $old_year ? 'selected': ''}}>{{ $year->year }}</option>
                            @endforeach
                        </select><br>
                        <label>Week </label>
                        <select name="week">
                            @for($week = 1; $week <= 53; $week++)
                            <option value="{{ $week }}" {{ $week == $old_week ? 'selected': ''}}>{{ $week }}</option>
                            @endfor
                        </select>
                        <span class="uk-input-group-addon">
                            <button type="submit" class="uk-button uk-button-primary">Filter</button>
                        </span>
                    </div>
                    </form>
                </div>
                <div class="uk-clearfix"></div>


                <div class="display-share">
                    <div class="md-card">
                        <div class="md-card-content">
                            <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#sub_category', animation:'slide-horizontal'}">
                            	@foreach($sub_categories as $key => $sub_category)
                                <li class="uk-width-1-{{ count($sub_categories) }} {{ ($key == 0)?'uk-active':'' }} "><a href="#">{{ $sub_category->title }}</a></li>
                            	@endforeach
                            </ul>
                            <ul id="sub_category" class="uk-switcher uk-margin">
                            	@foreach($sub_categories as $key => $sub_category)
                                <li>
                                    <table class="uk-table uk-table-striped">
                                        <thead>
                                            <tr>
                                                <th>Channel</th>
                                                @foreach($brands as $key => $brand)
                                                <th>{{ $brand->acronym }}</th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($outlets as $key => $outlet)
                                            <tr>
                                                <td><a href="/display-share/{{ $outlet->code }}?year={{ $old_year }}&week={{ $old_week }}" class="load">{{ $outlet->title }}: {{ $outlet->code }}</a></td>
                                                @foreach($brands as $key => $brand)
                                                <td><?php

                                                    // fortunately or unfortunately we have to open
                                                    // php tags and echo the one value that we need
                                                    // lets try
                                                    $total = 0;
                                                    foreach ($shares as $key => $share) {
                                                        if($share->outlet_code == $outlet->code
                                                            && $share->sub_category_id == $sub_category->id 
                                                            && $share->brand_id == $brand->id )

                                                                $total += $share->quantity;
                                                        
                                                    }

                                                    echo $total;

                                                ?></td>
                                                @endforeach
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </li>
                            	@endforeach
                            </ul>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
</div>


@endsection