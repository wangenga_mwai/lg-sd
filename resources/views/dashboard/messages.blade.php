<?php 


	$title = 'Messages';

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<h3 class="heading_b uk-margin-bottom">{{ $title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                <form method="post" action="/messages" id="message-form" class="uk-form uk-form-horizontal">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="" >
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="All">
                            <input type="checkbox" name="all" id="all_selector" value="true" >
                            Send to all
                        </label>
                        <label class="uk-form-label" for="promoters">Promoters</label>
                        <select name="promoters[]" id="promoters" multiple>
                            @foreach($promoters as $key => $promoter)
                            <option value="{{ $promoter->code }}" >{{ $promoter->first_name }} {{ $promoter->last_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="subject">Subject</label>
                        <div class="uk-form-controls">
                            <input type="text" name="subject" >
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="body">Body</label>
                        <div class="uk-form-controls">
                            <textarea name="body" ></textarea>
                        </div>
                    </div>
                    <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary ladda-button" data-style="expand-right"><span class="ladda-label">Save</span></button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection