<?php

$title = 'Branch Display Share';

?>
@extends('layouts.master')

@section('content')
<h3 class="heading_b uk-margin-bottom">{{ $title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">



                <div class="display-share">
                    <h4 class="heading_a uk-margin-bottom">{{ $outlet->title }}: {{ $outlet->code }}</h4>
                    <a href="/display-share" class="load"><i class="uk-icon-hand-o-left" ></i> Back</a>
                    <div class="md-card">
                        <div class="md-card-content">
                            <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#sub_category', animation:'slide-horizontal'}">
                            	@foreach($sub_categories as $key => $sub_category)
                                <li class="uk-width-1-{{ count($sub_categories) }} {{ ($key == 0)?'uk-active':'' }}"><a href="#">{{ $sub_category->title }}</a></li>
                            	@endforeach
                            </ul>
                            <ul id="sub_category" class="uk-switcher uk-margin">
                            	@foreach($sub_categories as $key => $sub_category)
                                <li>
                                    <table class="uk-table uk-table-striped">
                                        <thead>
                                            <tr>
                                                <th>Channel</th>
                                                @foreach($brands as $key => $brand)
                                                <th>{{ $brand->acronym }}</th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($outlet_branches as $key => $branch)
                                            <tr>
                                                <td>{{ $branch->title }}: {{ $branch->code }}</td>
                                                @foreach($brands as $key => $brand)
                                                <td><?php

                                                    // fortunately or unfortunately we have to open
                                                    // php tags and echo the one value that we need
                                                    // lets try
                                                    $total = 0;
                                                    foreach ($shares as $key => $share) {
                                                        if($share->outlet_branch_code == $branch->code
                                                            && $share->sub_category_id == $sub_category->id 
                                                            && $share->brand_id == $brand->id )

                                                                $total += $share->quantity;
                                                        
                                                    }

                                                    echo $total;

                                                ?></td>
                                                @endforeach
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </li>
                            	@endforeach
                            </ul>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
</div>


@endsection