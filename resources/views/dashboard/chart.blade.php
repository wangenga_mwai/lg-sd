<!DOCTYPE html>
<html>
<head>
	<title>Chart</title>
</head>
<body>
<div id="page_content_inner">
	<canvas id="myChart" width="840" height="350"></canvas>
</div>
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/Chart.js"></script>

<script type="text/javascript">
$(function(){

	var chartData = {!! $chart !!};

	var chartContainer = $("#myChart");
			
	// Chart.defaults.Line.legendTemplate = "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%>hg</li><%}%></ul>";
	if (chartContainer.length != 0) {

		
		// Chart.defaults.global.tooltipTemplate = "<%if (label){%><span style=\"background-color:blue\"><%=label%></span>:- <%}%><%= value %>";
		// Get context with jQuery - using jQuery's .get() method.
		var ctx = chartContainer.get(0).getContext("2d");
		// This will get the first returned node in the jQuery collection.
		var myNewChart = new Chart(ctx,{
			xAxisID: 'Day',
			yAxisID: 'Amount SDG',
		}).Line(chartData);
		
	}
});
</script>
</body>
</html>