<?php

$title = 'Dashboard';

?>
@extends('layouts.master')

@section('title',$title)
@section('content')
<h3 class="heading_b uk-margin-bottom">{{ $title }}</h3>
<div class="md-card">
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">

                <!-- statistics (small charts) -->
	            <div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler " data-uk-sortable data-uk-grid-margin>
	                <div>
	                    <div class="md-card">
	                        <div class="md-card-content">
	                            <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_visitors peity_data">5,3,9,6,5,9,7</span></div>
	                            <span class="uk-text-muted cd-title">Merchandisers</span>
	                            <h2 class="uk-margin-remove"><span class="countUpMe">{{ count($merchandisers) }}<noscript>12456</noscript></span></h2>
	                        </div>
	                    </div>
	                </div>
	                <div>
	                    <div class="md-card">
	                        <div class="md-card-content">
	                            <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">64/100</span></div>
	                            <span class="uk-text-muted cd-title">Promoters</span>
	                            <h2 class="uk-margin-remove"><span class="countUpMe">{{ count($promoters) }}<noscript>64</noscript></span></h2>
	                        </div>
	                    </div>
	                </div>
	                <div>
	                    <div class="md-card">
	                        <div class="md-card-content">
	                            <span class="uk-text-muted cd-title">Transactions</span>
	                            <h2 class="uk-margin-remove"><span class="countUpMe">{{ $transactions }}<noscript>64</noscript></span></h2>
	                        </div>
	                    </div>
	                </div>
	                <div>
	                    <div class="md-card">
	                        <div class="md-card-content">
	                            <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_sale peity_data">5,3,9,6,5,9,7,3,5,2</span></div>
	                            <span class="uk-text-muted cd-title">Sale</span>
	                            <h2 class="uk-margin-remove">{{env('COUNTRY_CURRENCY')}} <span class="countUpMe">{{ number_format($sales) }}<noscript>142384</noscript></span></h2>
	                        </div>
	                    </div>
	                </div>

	            </div>


            </div>
            <div class="uk-overflow-container">
				<div class="uk-text-center" style="margin-top: 20px;"><h3>Products sub-category sales per week chart</h3></div>
				<iframe src="/dashboard/chart"  width="880" height="380">
				  <p>Your browser does not support iframes. <a class="" href="/dashboard/chart">here</a></p>
				</iframe>
			</div>
        </div>
    </div>
</div>
@endsection