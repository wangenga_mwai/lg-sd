<div class="amount-filter">
	<form method="get" class="uk-form uk-form-horizontal" id="amount-filter">

	<div class="uk-input-group">
		<input class="md-input" name="start_amount" value="{{ $old_start_amount or ''}}" placeholder="Start Amount" type="number">
		<input class="md-input" name="end_amount" value="{{ $old_end_amount or ''}}" placeholder="End Amount" type="number">
		<span class="uk-input-group-addon">
			<button type="submit" class="uk-button uk-button-primary">Filter</button>
		</span>
    </div>
	</form>
</div>
<div class="uk-clearfix"></div>