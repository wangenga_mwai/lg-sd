<div class="date-filter">
	<form method="get" class="uk-form uk-form-horizontal" id="date-filter">

	<div class="uk-input-group">
		<input class="md-input" name="start_date" value="{{ $old_start_date or ''}}" placeholder="Start date" type="text" id="date-filter" data-uk-datepicker="{format:'YYYY-MM-DD'}">
		<input class="md-input" name="end_date" value="{{ $old_end_date or ''}}" placeholder="End date" type="text" id="date-filter" data-uk-datepicker="{format:'YYYY-MM-DD'}">
		<span class="uk-input-group-addon">
			<button type="submit" class="uk-button uk-button-primary">Filter</button>
		</span>
    </div>
	</form>
</div>
<div class="uk-clearfix"></div>