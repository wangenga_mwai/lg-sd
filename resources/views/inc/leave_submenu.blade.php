<div class="leave_submenu">
	<form class="uk-form" id="leave-sub-menu-form" method="get" action="/leave-request/{{ strtolower($sub_title) }}">
		<button type="submit" class="uk-button uk-button-primary" {{ ($submenu == 'Leave')? 'disabled': '' }} name="leave" value="-1">Leave</button>
		<button type="submit" class="uk-button uk-button-primary" {{ ($submenu == 'Request')? 'disabled': '' }} name="leave" value="0">Requests</button>
		<button type="submit" class="uk-button uk-button-primary" {{ ($submenu == 'Approved')? 'disabled': '' }} name="leave" value="1">Approved</button>
		<button type="submit" class="uk-button uk-button-primary" {{ ($submenu == 'Disapproved')? 'disabled': '' }} name="leave" value="2">Disapproved</button>
	</form>
</div>