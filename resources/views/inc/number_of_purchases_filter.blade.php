<div class="purchases-filter">
	<form method="get" class="uk-form uk-form-horizontal" id="purchases-filter">

	<div class="uk-input-group">
		<input class="md-input" name="start_number" value="{{ $old_start_number or ''}}" placeholder="Start Number" type="number">
		<input class="md-input" name="end_number" value="{{ $old_end_number or ''}}" placeholder="End Number" type="number">
		<span class="uk-input-group-addon">
			<button type="submit" class="uk-button uk-button-primary">Filter</button>
		</span>
    </div>
	</form>
</div>
<div class="uk-clearfix"></div>