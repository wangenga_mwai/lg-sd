<?php 
// var_dump($dates); die();
// echo json_encode($promoters); die();
    $title = 'Check In/Out';
	$sub_title = $sub_title;

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<h3 class="heading_b uk-margin-bottom uk-width-2-5">{{ $title }} &#45; {{ $sub_title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                @if(count($checks) > 0)
                <a href="/checks/csv/{{ strtolower($sub_title) }}?start_date={{ $start_date }}&end_date={{ $end_date }}" class="uk-button uk-button-primary" ><i class="uk-icon-file-excel-o"></i> Export</a>
                @endif
                <div class="date-filter">
                    <form method="get" class="uk-form uk-form-horizontal" id="date-filter">

                    <div class="uk-input-group">
                        <input class="md-input" name="start_date" value="{{ $start_date or ''}}" placeholder="From" type="text" id="date-filter" data-uk-datepicker="{format:'YYYY-MM-DD'}">
                        <input class="md-input" name="end_date" value="{{ $end_date or ''}}" placeholder="To" type="text" id="date-filter" data-uk-datepicker="{format:'YYYY-MM-DD'}">
                        <span class="uk-input-group-addon">
                            <button type="submit" class="uk-button uk-button-primary">Filter</button>
                        </span>
                    </div>
                    </form>
                </div>
                <div class="uk-clearfix"></div>
                <table class="uk-table uk-table-striped table-search">
                	<thead>
                        <tr>
                            <th rowspan="2">Date</th>
                            <th rowspan="2">{{ $sub_title }}</th>
                            <th colspan="2">Check In</th>
                            <th colspan="2">Check Out</th>
                            <th rowspan="2">Record</th>
                        </tr>
                        <tr>
                            <th>Time</th>
                            <th>Location</th>
                            <th>Time</th>
                            <th>Location</th>
                        </tr>
                	</thead>
                	<tbody>
                        @foreach($checks as $key => $check)
                        <tr>
                            <td>{{ ($check->checkin)? $check->checkin->date: (($check->check_date) ? $check->check_date : '-') }}</td>
                            <td>{{ $check->code }}: {{ $check->first_name }} {{ $check->last_name }}</td>
                            <td>{!! ($check->checkin)? substr($check->checkin->created_at,11): '<span class="uk-badge uk-badge-danger">Not checked in</span>' !!}</td>
                            <td>{!! ($check->checkin)? '<a href="https://www.google.com/maps?q=' .  $check->checkin->latitude . '+' . $check->checkin->longitude . '" target="_blank">' . $check->checkin->location . '</a>': '<span class="uk-badge uk-badge-danger">Not checked in</span>' !!}</td>
                            <td>{!! ($check->checkout)? substr($check->checkout->created_at,11): '<span class="uk-badge uk-badge-danger">Not checked out</span>' !!}</td>
                            <td>{!! ($check->checkout)? '<a href="https://www.google.com/maps?q=' .  $check->checkout->latitude . '+' . $check->checkout->longitude . '" target="_blank">' . $check->checkout->location . '</a>': '<span class="uk-badge uk-badge-danger">Not checked out</span>' !!}</td>
                            <td><a href="/check/{{ strtolower($sub_title) }}/{{ $check->code }}?start_date={{ $start_date }}&end_date={{ $end_date }}" class="view md-btn action-btn md-btn-info"><i class="uk-icon-info" ></i></a></td>
                        </tr>
                        @endforeach
                	</tbody>
                </table>
                {{ $checks->links() }}
            </div>
        </div>
    </div>
</div>
<div class="uk-modal" id="view-modal">
    <div class="uk-modal-dialog uk-modal-dialog-large">
        <div class="uk-modal-header">
            <div class="uk-modal-body"></div>
        </div>

    </div>
</div>
@endsection