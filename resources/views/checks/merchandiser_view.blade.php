<?php 
// var_dump($dates); die();
// echo json_encode($checks); die();
    $title = 'Check In/Out';
	$sub_title = $sub_title;

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<h3 class="heading_b uk-margin-bottom uk-width-2-5">{{ $title }} &#45; {{ $sub_title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                @if(count($checks) > 0)
                <a href="/checks/csv/{{ strtolower($sub_title) }}?start_date={{ $old_start_date }}&end_date={{ $old_end_date }}" class="uk-button uk-button-primary" ><i class="uk-icon-file-excel-o"></i> Export</a>
                @endif
                @include('inc.date_filter')
                <table class="uk-table uk-table-striped">
                	<thead>
                        <tr>
                            <th>{{ $sub_title }}</th>
                            <th>Check In</th>
                            <th>Check Out</th>
                            <th>Record</th>
                        </tr>
                	</thead>
                	<tbody>
                        @foreach($checks as $key => $check)
                        <tr>
                            <td>{{ $check->code }}: {{ $check->first_name }} {{ $check->last_name }}</td>
                            <td>{{ count($check->checkin) }}</td>
                            <td>{{ count($check->checkout) }}</td>
                            <td><a href="/check/{{ strtolower($sub_title) }}/{{ $check->code }}?start_date={{ $old_start_date }}&end_date={{ $old_end_date }}" class="view md-btn action-btn md-btn-info"><i class="uk-icon-info" ></i></a></td>
                        </tr>
                        @endforeach
                	</tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="uk-modal" id="view-modal">
    <div class="uk-modal-dialog uk-modal-dialog-large">
        <div class="uk-modal-header">
            <div class="uk-modal-body"></div>
        </div>

    </div>
</div>
@endsection