<?php 
// var_dump($dates); die();
// echo json_encode($promoters); die();
    $title = 'Check In/Out';
    $sub_title = $sub_title;

?>
@extends('layouts.master')

@section('title', $title)

@section('content')

<h3 class="heading_b uk-margin-bottom">{{ $title }} &#45; {{ $sub_title }}</h3>
<div class="md-card">
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                @if(count($checks) > 0)
                <h3>{{ (@$checks[0]->code)?$checks[0]->code .': ' . $checks[0]->first_name . ' ' . $checks[0]->last_name: $checks->first()->first_name }}</h3>
                <a href="/checks/{{ strtolower($sub_title) }}/{{ (@$checks[0]->code)? $checks[0]->code : $checks->first()->code }}/csv?start_date={{ $start_date }}&end_date={{ $end_date }}" class="uk-button uk-button-primary" ><i class="uk-icon-file-excel-o"></i> Export</a>

                <table class="uk-table uk-table-striped">
                	<thead>
                        <tr>
                            <th rowspan="2">Date</th>
                            <th colspan="2">Check In</th>
                            <th colspan="2">Check Out</th>
                        </tr>
                        <tr>
                            <th>Time</th>
                            <th>Location</th>
                            <th>Time</th>
                            <th>Location</th>
                        </tr>
                	</thead>
                	<tbody> 
                        @foreach($checks as $key => $check)
                        <tr>
                            <td>{{ ($check['checkin'])? $check['checkin']->date: (($check->check_date) ? $check->check_date : '-') }}</td>
                            <td>{!! ($check['checkin'])? substr($check['checkin']->created_at,11): '<span class="uk-badge uk-badge-danger">Not checked in</span>' !!}</td>
                            <td>{!! (@$check['checkin'])? '<a href="https://www.google.com/maps?q=' .  $check['checkin']->latitude . '+' . $check['checkin']->longitude . '" target="_blank">' . @$check['checkin']->location . '</a>': '<span class="uk-badge uk-badge-danger">Not checked in</span>' !!}</td>
                            <td>{!! (@$check['checkout'])? substr(@$check['checkout']->created_at,11): '<span class="uk-badge uk-badge-danger">Not checked out</span>' !!}</td>
                            <td>{!! (@$check['checkout'])? '<a href="https://www.google.com/maps?q=' .  $check['checkout']->latitude . '+' . $check['checkout']->longitude . '" target="_blank">' . @$check['checkout']->location . '</a>': '<span class="uk-badge uk-badge-danger">Not checked out</span>' !!}</td>
                        </tr>
                        @endforeach
                	</tbody>
                </table>
                {{ $checks->links() }}
                @else
                <p>No records found.</p>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection