<?php 
// var_dump($dates); die();
// echo json_encode($promoters); die();
    $title = 'Brochure';
    $sub_title = 'Brochure';
?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<h3 class="heading_b uk-margin-bottom uk-width-2-5">{{ $sub_title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        @if(count($brochures) > 0)
        <a href="/brochures/csv?start_date={{ $old_start_date }}&end_date={{ $old_end_date }}" class="uk-button uk-button-primary" ><i class="uk-icon-file-excel-o"></i> Export</a>
        @endif
        @include('inc.date_filter')
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                <table class="uk-table uk-table-striped">
                    <thead>
                        <th>Date</th>
                        <th>Code</th>
                        <th>Promoter</th>
                        <th>Email</th>
                        <th>Category</th>
                        <th>Sub-Category</th>
                        <th>Model</th>
                    </thead>
                    <tbody>
                        @foreach($brochures as $key => $brochure)
                        <tr>
                            <td>{{ date('Y-m-d', strtotime($brochure->created_at)) }}</td>
                            <td>{{ $brochure->promoter_code }}</td>
                            <td>{{ $brochure->promoter ? $brochure->promoter->first_name . ' ' . $brochure->promoter->last_name: '*' }}</td>
                            <td>{{ $brochure->email }}</td>
                            <td>{{ $brochure->model->category ? $brochure->model->category->title: '*' }}</td>
                            <td>{{ $brochure->model->sub_category ? $brochure->model->sub_category->title: '*' }}</td>
                            <td>{{ $brochure->model ? $brochure->model->title: '*' }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="uk-modal" id="view-modal">
    <div class="uk-modal-dialog uk-modal-dialog-large">
        <div class="uk-modal-header">
            <div class="uk-modal-body"></div>
        </div>

    </div>
</div>
@endsection()