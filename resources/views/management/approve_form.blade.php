<?php 
// var_dump($dates); die();
// echo json_encode($promoters); die();
    $title = 'Leave request approval';
    $sub_title = $sub_title;

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<h3 class="heading_b uk-margin-bottom uk-width-2-5">{{ $title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            @include('inc.leave_submenu')
            <div class="uk-width-1-1">
                <form method="post" action="/leave-request/{{ strtolower($sub_title) }}" id="approve-form" class="uk-form uk-form-horizontal">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{{ $request->id }}">
                    <fieldset>
                    <div class="uk-form-row">
                        <label class="uk-form-label">{{ $sub_title }}</label>
                        <div class="uk-form-controls">
                            {{ ($request->person)?$request->person->code .': ' . $request->person->first_name . ' ' . $request->person->last_name: '*' }}
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">Leave type</label>
                        <div class="uk-form-controls">
                            {{ $request->type->title }}
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">From</label>
                        <div class="uk-form-controls">
                            {{ $request->start_date }}
                        </div>
                    </div> 
                    <div class="uk-form-row">
                        <label class="uk-form-label">To</label>
                        <div class="uk-form-controls">
                            {{ $request->end_date }}
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">No. of days</label>
                        <div class="uk-form-controls">
                            {{ $request->no_of_days }}
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label">Reason</label>
                        <div class="uk-form-controls">
                            <textarea name="reason" placeholder="Reason"></textarea>
                        </div>
                    </div>   
                    <!-- <div class="uk-modal-footer uk-text-right"> -->
                        <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                        <button type="submit" name="status" value="approve" class="md-btn md-btn-flat md-btn-flat-primary ladda-button" data-style="expand-right"><span class="ladda-label">Approve</span></button>
                        <button type="submit" name="status" value="disapprove" class="md-btn md-btn-flat md-btn-flat-primary ladda-button" data-style="expand-right"><span class="ladda-label">Disapprove</span></button>
                    <!-- </div> -->
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection