<?php 
// var_dump($dates); die();
// echo json_encode($promoters); die();
    $title = 'Leave Request';
	$sub_title = $sub_title;

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<h3 class="heading_b uk-margin-bottom uk-width-2-5">{{ $title }} &#45; {{ $sub_title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            @include('inc.leave_submenu')
            <div class="uk-width-1-1">
                @if(count($requests) > 0)
                <table class="uk-table uk-table-striped">
                	<thead>
                        <th>Date</th>
                        <th>{{ $sub_title }}</th>
                        <th>Type</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Days</th>
                        <th>Action</th>
                	</thead>
                	<tbody>
                        @foreach($requests as $key => $request)
                        <tr>
                            <td>{{ $request->created_at }}</td>
                            <td>{{ ($request->person)?$request->person->code .': ' . $request->person->first_name . ' ' . $request->person->last_name: '*' }}</td>
                            <td>{{ $request->type->title }}</td>
                            <td>{{ $request->start_date }}</td>
                            <td>{{ $request->end_date }}</td>
                            <td>{{ $request->no_of_days }}</td>
                            <td><a href="/leave-request/{{ strtolower($sub_title) }}/{{ $request->id }}" class="response md-btn md-btn-success"><i class="uk-icon-edit" ></i></a></td>
                        </tr>
                        @endforeach
                	</tbody>
                </table>
                @else
                <p>No data found</p>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="uk-modal" id="response-modal">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Request response (Approve/Disapprove)</h3>
        </div>
        <div class="uk-modal-body"></div>
    </div>
</div>
@endsection