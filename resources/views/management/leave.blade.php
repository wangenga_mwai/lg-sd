<?php 
// var_dump($leaves); die();
// echo json_encode($leaves); die();
    $title = 'Leave Management';
	$sub_title = $sub_title;

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<h3 class="heading_b uk-margin-bottom uk-width-2-5">{{ $title }} <small>&#40;{{ $sub_title }}&#41;</small></h3>

<div class="md-card">
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                @include('inc.leave_submenu')
                <br><a href="/leave/{{ strtolower($sub_title) }}/csv" class="uk-button uk-button-primary" ><i class="uk-icon-file-excel-o"></i> Export</a>
                <table class="uk-table uk-table-striped">
                	<thead>
                        <th>{{ $sub_title }}</th>
                        <th>Set Leave Days</th>
                        <th>Earned Leave days</th>
                        <th>Used Leave days</th>
                        <th>Balance</th>
                	</thead>
                	<tbody>
                        @foreach($leaves as $key => $leave)
                        @if($leave->person)
                        <tr>
                            <td>{{ ($leave->person)?$leave->person->code .': ' . $leave->person->first_name . ' ' . $leave->person->last_name: '*' }}</td>
                            <td>{{ $leave->set_leave_days }}</td>
                            <td>{{ $leave->earned_leave_days }}</td>
                            <td>{{ $leave->used_days }}</td>
                            <td>{{ $leave->earned_leave_days - $leave->used_days }}</td>
                        </tr>
                        @endif
                        @endforeach
                	</tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection()