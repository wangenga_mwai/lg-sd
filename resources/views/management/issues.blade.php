<?php 
// var_dump($dates); die();
// echo json_encode($promoters); die();
    $title = 'Issue Management';
	$sub_title = '';

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<h3 class="heading_b uk-margin-bottom uk-width-2-5">{{ $title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                <table class="uk-table uk-table-striped">
                	<thead>
                        <th>Date</th>
                        <th>Promoter</th>
                        <th>Issue ID</th>
                        <th>Issue</th>
                        <th>Status</th>
                	</thead>
                	<tbody>
                        @foreach($issues as $key => $issue)
                        <tr>
                            <td>{{ $issue->created_at }}</td>
                            <td>{{ ($issue->promoter)?$issue->promoter->code .': ' . $issue->promoter->first_name . ' ' . $issue->promoter->last_name: '*' }}</td>
                            <td>{{ $issue->issue_id }}</td>
                            <td>{{ $issue->issue }}</td>
                            <td><span class="uk-badge uk-badge-{{ ($issue->active)? 'success':'danger' }}">{{ ($issue->active)? 'Solved':'Pending' }}</span></td>
                        </tr>
                        @endforeach
                	</tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection()