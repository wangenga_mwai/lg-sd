<?php 
// var_dump($dates); die();
// echo json_encode($promoters); die();
    $title = 'Staff';
	$sub_title = 'Promoter';
?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<h3 class="heading_b uk-margin-bottom uk-width-2-5">{{ $sub_title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        @if(Auth::user()->admin)<button type="button" class="uk-button uk-button-primary" data-uk-modal="{target:'#add-modal'}">Add Promoter</button>@endif
        @if(count($promoters) > 0)
        <a href="/promoters/csv" class="uk-button uk-button-primary" ><i class="uk-icon-file-excel-o"></i> Export</a>
        @endif
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                <table class="uk-table uk-table-striped">
                	<thead>
                        <th>Code</th>
                        <th>Name</th>
                        <th>PP. &#47; ID No.</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Merchandiser</th>
                        <th>Outlet</th>
                        <th>Outlet Branch</th>
                        <th>Current Log</th>
                		<th>Action</th>
                	</thead>
                	<tbody>
                        @foreach($promoters as $key => $p)
                        <tr>
                            <td>{{ @$p->code }}</td>
                            <td>{{ @$p->first_name . ' ' . @$p->last_name }}</td>
                            <td>{{ @$p->id_number }}</td>
                            <td>{{ @$p->phone }}</td>
                            <td>{{ @$p->email }}</td>
                            <td>{{ @$p->merchandiser->first_name. ' ' .@$p->merchandiser->last_name}}</td>
                            <td>{{ @$p->outlet->title}}</td>
                            <td>{{ @$p->sale_outlet_branch()->title}}</td>
                            <td>
                                @if($p->imei != '' && Auth::user()->admin)
                                <a href="/promoters/{{ $p->id }}/clear-imei" class="activate md-btn action-btn md-btn-info"><i class="uk-icon-terminal" ></i></a>
                                @elseif($p->imei != '' && !Auth::user()->admin)
                                <i class="uk-icon-check"></i>
                                @else
                                <i class="uk-icon-ban"></i>
                                @endif
                            </td>
                            <td class="action-tab">
                                <a href="/promoters/view/{{ $p->id }}" class="view md-btn action-btn md-btn-info"><i class="uk-icon-info" ></i></a>
                                @if(Auth::user()->admin)
                                <a href="/promoters/edit/{{ $p->id }}" class="edit md-btn action-btn md-btn-success"><i class="uk-icon-edit" ></i></a>
                                <a href="/promoters/activate/{{ $p->id }}" class="activate md-btn action-btn md-btn-warning"><i class="uk-icon-{{ ($p->active)? 'check': 'ban' }}" ></i></a>
                                <a href="/promoters/delete/{{ $p->id }}" class="delete md-btn action-btn md-btn-danger" ><i class="uk-icon-trash-o" ></i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                	</tbody>
                </table>
            </div>
            <div class="uk-width-1-1 uk-grid-margin">
                {{$promoters->links()}}
            </div>
        </div>
    </div>
</div>
<div class="uk-modal" id="edit-modal">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Edit Promoter</h3>
        </div>
        <form method="post" action="/promoters/edit" id="edit-form" class="uk-form uk-form-horizontal">
            {!! csrf_field() !!}
            <input type="hidden" name="id" value="" >
            <div class="uk-form-row">
                <label class="uk-form-label" for="first_name">First name</label>
                <div class="uk-form-controls">
                    <input type="text" name="first_name" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="last_name">Last name</label>
                <div class="uk-form-controls">
                    <input type="text" name="last_name" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="id_number">ID Number</label>
                <div class="uk-form-controls">
                    <input type="number" name="id_number" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="email">Email</label>
                <div class="uk-form-controls">
                    <input type="email" name="email" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="merchandiser">Mechandiser</label>
                <div class="uk-form-controls">
                    <select name="merchandiser_code" class="merchandiser">
                        @foreach($merchandisers as $merchandiser)
                        <option value="{{ $merchandiser->code }}" >{{ $merchandiser->first_name }} {{ $merchandiser->last_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="outlet">Outlet</label>
                <div class="uk-form-controls">
                    <select name="outlet_code" class="outlet">
                        <option value="" >-- Outlets --</option>
                        @foreach($outlets as $outlet)
                        <option value="{{ @$outlet->code }}" >{{ @$outlet->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="outlet">Outlet Branch</label>
                <div class="uk-form-controls">
                    <select name="outlet_branch_code" class="outlet_branch">
                        <option value="" >Select a outlet</option>
                    </select>
                    <i class="uk-icon-refresh uk-icon-spin outlet-branch-loader"></i>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="promoter_type_id">Department</label>
                <div class="uk-form-controls">
                    <select name="promoter_type_id">
                        <option value="">-- Depertment --</option>
                        <option value="1">HA/HE</option>
                        <option value="2">Mobile</option>
                    </select>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="phone">Phone number</label>
                <div class="uk-form-controls">
                    <input type="text" name="phone" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="mobile_money">Mobile money no.</label>
                <div class="uk-form-controls">
                    <input type="text" name="mobile_money" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="salary">Salary</label>
                <div class="uk-form-controls">
                    <input type="number" name="salary" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="salary_level">Salary level</label>
                <div class="uk-form-controls">
                    <select name="salary_level">
                    <option value="">-- Salary Level --</option>
                        @for($i = 1; $i <= 5; $i++)
                        <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                    </select>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="bank">Bank</label>
                <div class="uk-form-controls">
                    <select name="bank_code" class="bank">
                        @foreach($banks as $bank)
                        <option value="{{ $bank->code }}" >{{ $bank->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="bank">Bank Branch</label>
                <div class="uk-form-controls">
                    <select name="bank_branch_code" class="bank_branch">
                        <option value="" >Select a bank</option>
                    </select>
                    <i class="uk-icon-refresh uk-icon-spin bank-branch-loader"></i>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="account_no">Account number</label>
                <div class="uk-form-controls">
                    <input type="number" name="account_no" >
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary ladda-button" data-style="expand-right"><span class="ladda-label">Save Changes</span></button>
            </div>
        </form>
    </div>
</div>
<div class="uk-modal" id="add-modal">
    <div class="uk-modal-dialog">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Add Promoter</h3>
        </div>
        <form method="post" action="/promoters/add" id="add-form" class="uk-form uk-form-horizontal">
            {!! csrf_field() !!}
            <div class="uk-form-row">
                <label class="uk-form-label" for="first_name">First name</label>
                <div class="uk-form-controls">
                    <input type="text" name="first_name" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="last_name">Last name</label>
                <div class="uk-form-controls">
                    <input type="text" name="last_name" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="merchandiser">Mechandiser</label>
                <div class="uk-form-controls">
                    <select name="merchandiser_code" class="merchandiser">
                        @foreach($merchandisers as $merchandiser)
                        <option value="{{ $merchandiser->code }}" >{{ $merchandiser->first_name }} {{ $merchandiser->last_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="id_number">ID Number</label>
                <div class="uk-form-controls">
                    <input type="number" name="id_number" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="email">Email</label>
                <div class="uk-form-controls">
                    <input type="email" name="email" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="outlet">Outlet</label>
                <div class="uk-form-controls">
                    <select name="outlet_code" class="outlet">
                        <option value="" >-- Outlets --</option>
                        @foreach($outlets as $outlet)
                        <option value="{{ $outlet->code }}" >{{ $outlet->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="outlet">Outlet Branch</label>
                <div class="uk-form-controls">
                    <select name="outlet_branch_code" class="outlet_branch">
                        <option value="" >Select a outlet</option>
                    </select>
                    <i class="uk-icon-refresh uk-icon-spin no-show outlet-branch-loader"></i>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="promoter_type_id">Department</label>
                <div class="uk-form-controls">
                    <select name="promoter_type_id">
                        <option value="">-- Depertment --</option>
                        <option value="1">HA/HE</option>
                        <option value="2">Mobile</option>
                    </select>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="phone">Phone number</label>
                <div class="uk-form-controls">
                    <input type="text" name="phone" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="mobile_money">Mobile money no.</label>
                <div class="uk-form-controls">
                    <input type="text" name="mobile_money" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="salary">Salary</label>
                <div class="uk-form-controls">
                    <input type="number" name="salary" >
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="salary_level">Salary level</label>
                <div class="uk-form-controls">
                    <select name="salary_level">
                    <option value="">-- Salary Level --</option>
                        @for($i = 1; $i <= 5; $i++)
                        <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                    </select>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="bank">Bank</label>
                <div class="uk-form-controls">
                    <select name="bank_code" class="bank">
                        @foreach($banks as $bank)
                        <option value="{{ $bank->code }}" >{{ $bank->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="bank">Bank Branch</label>
                <div class="uk-form-controls">
                    <select name="bank_branch_code" class="bank_branch">
                        <option value="" >Select a bank</option>
                    </select>
                    <i class="uk-icon-refresh uk-icon-spin no-show bank-branch-loader"></i>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="account_no">Account number</label>
                <div class="uk-form-controls">
                    <input type="number" name="account_no" >
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary ladda-button" data-style="expand-right"><span class="ladda-label">Save</span></button>
            </div>
        </form>
    </div>
</div>
<div class="uk-modal" id="view-modal">
    <div class="uk-modal-dialog uk-modal-dialog-large">
        <div class="uk-modal-header">
            <div class="uk-modal-body"></div>
        </div>

    </div>
</div>
@endsection()