<?php 
// var_dump($dates); die();
// echo json_encode($promoters); die();
    $title = 'Staff';
	$sub_title = 'Promoters';

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<h3 class="heading_b uk-margin-bottom uk-width-2-5">{{ $promoter->first_name }} {{ $promoter->last_name }}</h3>

<div class="md-card">
    <h3>Promoter Details</h3>
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-5">
                <strong>Name</strong>
            </div>
            <div class="uk-width-2-5">
                {{ @$promoter->first_name }} {{ @$promoter->last_name }}
            </div>
            <div class="uk-width-1-5">
                <strong>Code</strong>
            </div>
            <div class="uk-width-1-5">
                {{ @$promoter->code }}
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-5">
                <strong>ID Number</strong>
            </div>
            <div class="uk-width-2-5">
                {{ @$promoter->id_number }}
            </div>
            <div class="uk-width-1-5">
                <strong>Phone</strong>
            </div>
            <div class="uk-width-1-5">
                {{ @$promoter->phone }}
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-5">
                <strong>Merchandiser</strong>
            </div>
            <div class="uk-width-2-5">
                {{ @$promoter->merchandiser->first_name }} {{ @$promoter->merchandiser->last_name }}
            </div>
            <div class="uk-width-1-5">
                <strong>Phone</strong>
            </div>
            <div class="uk-width-1-5">
                {{ @$promoter->phone }}
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-5">
                <strong>Email</strong>
            </div>
            <div class="uk-width-2-5">
                {{ @$promoter->email }}
            </div>
            <div class="uk-width-1-5">
                <strong>Salary</strong>
            </div>
            <div class="uk-width-1-5">
                {{ number_format(@$promoter->salary) }}
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-5">
                <strong>Salary Level</strong>
            </div>
            <div class="uk-width-2-5">
                {{ @$promoter->salary_level }}
            </div>
            <div class="uk-width-1-5">
                <strong>Account number</strong>
            </div>
            <div class="uk-width-1-5">
                {{ @$promoter->account_no }}
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-5">
                <strong>Outlet</strong>
            </div>
            <div class="uk-width-2-5">
                {{ (@$promoter->outlet)? $promoter->outlet->title: 'N/A' }}
            </div>
            <div class="uk-width-1-5">
                <strong>Branch</strong>
            </div>
            <div class="uk-width-1-5">
                {{ (@$promoter->branch_of_outlet(@$promoter->outlet_code,@$promoter->outlet_branch_code))? @$promoter->branch_of_outlet(@$promoter->outlet_code,@$promoter->outlet_branch_code)->title: 'N/A' }}
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-5">
                <strong>Bank</strong>
            </div>
            <div class="uk-width-2-5">
                {{ (@$promoter->bank)? @$promoter->bank->title: 'N/A' }}
            </div>
            <div class="uk-width-1-5">
                <strong>Branch</strong>
            </div>
            <div class="uk-width-1-5">
                {{ (@$promoter->branch_of_bank(@$promoter->bank_code,@$promoter->bank_branch_code))? @$promoter->branch_of_bank(@$promoter->bank_code,@$promoter->bank_branch_code)->title: 'N/A' }}
            </div>
        </div>
    </div>
</div>

@endsection