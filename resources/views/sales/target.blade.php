<?php 
// var_dump($dates); die();
// echo json_encode($promoters); die();
    $title = 'Sales Targets';
	$sub_title = 'Targets';

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<div class="uk-grid">
    <h3 class="heading_b uk-margin-bottom uk-width-2-5">{{ $sub_title }}</h3>
    
</div>
<div class="md-card">
    <div class="md-card-content">
        <div class="filters uk-width-2-5 uk-float-right filters filters-float">
            <form class="uk-form" action="/sales/targets" id="filter-form" method="get">
                    <label for="year">Year: </label>
                    <select name="year" class="form-control year"><?php $year = date('Y') ?>
                        <option value="">-- Year --</option>
                        @while($year >= 2015)
                        <option value="{{ $year }}" {{ ($year == $old_year)? 'selected':'' }}>{{ $year }}</option><?php $year-- ?>
                        @endwhile
                    </select>
                    <label for="month">Month: </label>
                    <select name="month" class="form-control month">
                        <option value="">-- Month --</option>
                        @for($month = 1; $month <= 12; $month++)
                        <option value="{{ $month }}" {{ ($month == $old_month )? 'selected':'' }}>{{ $month }}</option>
                        @endfor
                    </select>
                    <button type="submit" class="uk-button uk-button-primary" id="set-btn" disabled >Filter</button>
            </form>
        </div>
        <p></p>
        <div class="clearfix" ></div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1 ">
                    <form action="/sales/targets" id="set-target-form" method="post" >
                        {!! csrf_field() !!}
                        <input type="hidden" name="month" value="{{ $old_month }}"> 
                        <input type="hidden" name="year" value="{{ $old_year }}"> 
                        @if(Auth::user()->admin)<button type="submit" class="uk-button uk-button-primary ladda-button" data-style="expand-right" ><span class="ladda-label">Save targets</span></button>@endif
                        <table class="uk-table uk-table-striped">
                            <thead>
                                <th>Promoter name</th>
                                <th>Merchandiser name</th>
                                <th>Outlet</th>
                                <th>Branch</th>
                                <th>Year&#47;Month</th>
                                <th class="number">Target</th>
                                <th class="number">Sales</th>
                                <th class="number">Percent achieved</th>
                            </thead>
                            <tbody>
                            @foreach($promoters as $key => $promoter)
                            <?php $total_sales = @$promoter->sales_sum($dates) ?>
                            <tr>
                                <td>{{ @$promoter->first_name . ' ' . $promoter->last_name }}</td>
                                <td>{{ @$promoter->merchandiser->first_name . ' ' . @$promoter->merchandiser->last_name }}</td>
                                <td>{{ (@$promoter->outlet_branch)? @$promoter->outlet_branch->outlet->title : '*'}}</td>
                                <td>{{ (@$promoter->outlet_branch)? @$promoter->outlet_branch->title : '*'}}</td>
                                <td>{{ $old_year.'/'.$old_month }}</td>
                                <td class="number">
                                    @if(Auth::user()->admin)
                                    <input type="number" name="target[{{ $promoter->id }}]" min="0" value="{{ (count($promoter->sales_targets) > 0 )? $promoter->sales_targets[0]->sales_target : 0 }}" >
                                    @else
                                    {{ (count($promoter->sales_targets) > 0 )? number_format($promoter->sales_targets[0]->sales_target) : 0 }}
                                    @endif
                                </td>
                                <td class="number">{{ number_format($total_sales) }}</td>
                                <td class="number">{{ (count($promoter->sales_targets) > 0 && $promoter->sales_targets[0]->sales_target != 0 )? number_format($total_sales / $promoter->sales_targets[0]->sales_target * 100) : 0 }} &#37;</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </form>
            </div>
        </div>
    </div>
</div>
@endsection()