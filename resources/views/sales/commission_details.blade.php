<?php 
// var_dump($dates); die();
// echo json_encode($promoters); die();
    $title = 'Commission Details';
	$sub_title = '';

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<div class="uk-grid">
    <h3 class="heading_b uk-margin-bottom uk-width-2-5">{{ $title }} - {{ $old_year }} Week {{ $old_week }}</h3>
    
    
</div>
<div class="md-card">
    <div class="md-card-content">
        @if(count($promoters) > 0)
        <a href="/commissions/details/csv?week={{ $old_week }}&year={{ $old_year }}" class="uk-button uk-button-primary" ><i class="uk-icon-file-excel-o"></i> Export</a>
        @endif
        <div class="clearfix"></div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1 ">
                <table class="uk-table uk-table-striped">
                	<thead>
                        <th>Promoter</th>
                        <th>Total Sales Value</th>
                        <th>Commissions Earned</th>
                        <th>Commissions Paid</th>
                        <th>Balance</th>
                	</thead>
                	<tbody>
                        @foreach( $promoters as $key => $promoter )
                        <tr>
                            <td>{{ $promoter->code . ': ' .$promoter->first_name . ' ' . $promoter->last_name }}</td>
                            <td>{{ (isset($promoter->commission_details[0]))?number_format($promoter->commission_details[0]->sales_value):0 }}</td>
                            <td>{{ (isset($promoter->commission_details[0]))?number_format($promoter->commission_details[0]->earned_commissions):0 }}</td>
                            <td>{{ (isset($promoter->commission_details[0]))?number_format($promoter->commission_details[0]->paid_commissions):0 }}</td>
                            <td>{{ (isset($promoter->commission_details[0]))?number_format($promoter->commission_details[0]->unpaid_commissions):0 }}</td>
                        </tr>
                        @endforeach
                	</tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection