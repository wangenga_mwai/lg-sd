<?php 
// var_dump($dates); die();
// echo json_encode($promoters); die();
    $title = 'Commissions';
	$sub_title = '';

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<div class="uk-grid">
    <h3 class="heading_b uk-margin-bottom uk-width-2-5">{{ $title }} - {{date('M',mktime(0,0,0,$old_month,10)) }} {{ $old_year }}</h3>
    
    
</div>
<div class="md-card">
    <div class="md-card-content">
        @if(count($commissions) > 0)
        <a href="/commissions/csv?year={{ $old_year }}&month={{@$old_month}}" class="uk-button uk-button-primary" ><i class="uk-icon-file-excel-o"></i> Export</a>
        @endif
        <div class="filters uk-float-right">
            <form class="uk-form" action="/commissions" id="filter-form" method="get">

                    <label for="year">Year: </label>
                    <select name="year" class="form-control year"><?php $year = date('Y') ?>
                        <option value="">-- Year --</option>
                        @while($year >= 2015)
                        <option value="{{ $year }}" {{ ($year == $old_year)? 'selected':'' }}>{{ $year }}</option><?php $year-- ?>
                        @endwhile
                    </select>
                    <label>Month:</label>
                    <select name="month" class="form-control month">
                        <option value="">-- Month --</option>
                        @for($month = 1; $month <= 12; $month++)
                        <option value="{{ $month }}" {{ (@$old_month && $month == $old_month )? 'selected':'' }}>{{ date('M',mktime(0,0,0,$month,10)) }}</option>
                        @endfor
                    </select>
                    <button type="submit" class="uk-button uk-button-primary" id="set-btn" disabled >Filter</button>
            </form>
        </div>
        <div class="clearfix"></div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1 ">
                <table class="uk-table uk-table-striped">
                	<thead>
                        <th>Week</th>
                        <th>Period</th>
                        <th>Total Sales Value</th>
                        <th>Commissions Earned</th>
                        <th>Commissions Paid</th>
                        <th>Balance</th>
                        <th>Details</th>
                	</thead>
                	<tbody>
                        @foreach( $commissions as $key => $commission )
                        <tr>
                            <td>{{ $commission->week }}</td>
                            <td>{{ date('jS M',strtotime($commission->start_date)) }} &#47; {{ date('jS M',strtotime($commission->end_date)) }}</td>
                            <td>{{ number_format($commission->sales_value) }}</td>
                            <td>{{ number_format($commission->earned_commissions) }}</td>
                            <td>{{ number_format($commission->paid_commissions) }}</td>
                            <td>{{ number_format($commission->unpaid_commissions) }}</td>
                            <td><a href="/commissions/details?week={{ $commission->week }}&year={{ $commission->year }}" class="view md-btn action-btn md-btn-info"><i class="uk-icon-info"></i></a></td>
                        </tr>
                        @endforeach
                	</tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="uk-modal" id="view-modal">
    <div class="uk-modal-dialog uk-modal-dialog-large">
        <div class="uk-modal-header">
            <div class="uk-modal-body"></div>
        </div>

    </div>
</div>
@endsection