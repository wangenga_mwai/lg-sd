<?php 
// var_dump($dates); die();
// echo json_encode($promoters); die();
    $title = 'Sales';
	$sub_title = '';

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<div class="uk-grid">
    <h3 class="heading_b uk-margin-bottom uk-width-2-5">{{ $title }}</h3>
    
    
</div>
<div class="md-card">
    <div class="md-card-content">
        @if(count($sales) > 0)
        <a href="/sales/csv?start_date={{ $old_start_date }}&end_date={{ $old_end_date }}" class="uk-button uk-button-primary" ><i class="uk-icon-file-excel-o"></i> Export</a>
        @endif
        <div class="filters uk-float-right">
            {{ csrf_field() }}
            <form class="uk-form" action="/sales" id="filter-form" method="get">
                    <label for="year">Year: </label>
                    <select name="year" class="form-control year"><?php $year = date('Y') ?>
                        <option value="">-- Year --</option>
                        @while($year >= 2015)
                        <option value="{{ $year }}" {{ ($year == $old_year)? 'selected':'' }}>{{ $year }}</option><?php $year-- ?>
                        @endwhile
                    </select>
                    <label for="month">Month: </label>
                    <select name="month" class="form-control month">
                        <option value="">-- Month --</option>
                        @for($month = 1; $month <= 12; $month++)
                        <option value="{{ $month }}"  {{ ($month == $old_month )? 'selected':'' }}>{{ date('F', strtotime($month.'/01/2016')) }}</option>
                        @endfor
                    </select>
                    <button type="submit" class="uk-button uk-button-primary" id="set-btn" disabled >Filter</button>
            </form>
        </div>
        @include('inc.date_filter')
        <div class="clearfix"></div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1 uk-overflow-container">
                <table class="uk-table uk-table-striped uk-table-codensed table-search-paging">
                	<thead>
                        <th>Date</th>
                        <th>Promoter</th>
                        <th>Merchandiser</th>
                        <th>Outlet</th>
                        <th>Branch</th>
                        <th>Category</th>
                        <th>Sub-Category</th>
                        <th>Model</th>
                        <th>Quantity</th>
                        <th>Amount</th>
                        <th>Commission</th>
                        <th>Additional commission</th>
                        @if(Auth::user()->admin)<th>Action</th>@endif
                	</thead>
                	<tbody>
                        @foreach( $sales as $key => $sale )
                        <tr>
                            <td>{{ date('Y-m-d', strtotime($sale->created_at)) }}</td>
                            <td>{{ @$sale->promoter->first_name . ' ' . @$sale->promoter->last_name }}</td>
                            <td>{{ @$sale->merchandiser->first_name . ' ' . @$sale->merchandiser->last_name }}</td>
                            <td>{{ (@$sale->outlet)? @$sale->outlet->title : '*' }}</td>
                            <td>{{ (@$sale->sale_outlet_branch())? @$sale->sale_outlet_branch()->title : '*' }}</td>
                            <td>{{ @$sale->category->title }}</td>
                            <td>{{ @$sale->sub_category->title }}</td>
                            <td>{{ @$sale->model->title }}</td>
                            <td>{{ @$sale->quantity }}</td>
                            <td class="number">{{ number_format(@$sale->amount) }}</td>
                            <td class="number">{{ number_format(@$sale->commission, 2) }}</td>
                            <td class="number">{{ number_format(@$sale->additional_commission) }}</td>
                            @if(Auth::user()->admin)
                            <td>
                                <a href="/sale/delete/{{ $sale->id }}" class="delete md-btn action-btn md-btn-danger" ><i class="uk-icon-trash-o" ></i></a>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                	</tbody>
                </table>
                <div class="uk-width-1-1">
                    {{$sales->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection()