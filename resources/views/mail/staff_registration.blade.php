@extends('layouts.mail')

@section('content')
<div>
	<p>Dear {{ $m->first_name }} {{ $m->last_name }},</p>
	<p>You have been added to LG merchant system. Your code is <code>{{ $m->code }}</code></p>
</div>
@endsection