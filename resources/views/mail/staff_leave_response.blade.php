@extends('layouts.mail')

@section('content')
<div>
	<p>Dear {{ $person->first_name }} {{ $person->last_name }},</p>
	<p>Your leave request from {{ $leave->start_date }} to {{ $leave->end_date }} has been {{ ($leave->status == 1)? 'approved': 'disapproved' }}</p>
	<p>{{ $leave->reason }}</p>
</div>
@endsection