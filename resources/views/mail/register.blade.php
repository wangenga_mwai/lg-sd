@extends('layouts.mail')

@section('content')
Hi, {{ $user->username }}
<p>You have been registered as a user in LG Kenya incentives panel.<p>
<p>Username: <strong>{{ $user->username }}</strong>
<p>Password: <strong>{{ $password }}</strong>
@endsection