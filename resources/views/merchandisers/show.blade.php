<?php 
// var_dump($dates); die();
// echo json_encode($promoters); die();
    $title = 'Staff';
	$sub_title = 'Merchandisers';

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<h3 class="heading_b uk-margin-bottom uk-width-2-5">{{ $merchandiser->first_name }} {{ $merchandiser->last_name }}</h3>

<div class="md-card">
    <h3>Merchandiser Details</h3>
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-5">
                <strong>Name</strong>
            </div>
            <div class="uk-width-2-5">
                {{ $merchandiser->first_name }} {{ $merchandiser->last_name }}
            </div>
            <div class="uk-width-1-5">
                <strong>Code</strong>
            </div>
            <div class="uk-width-1-5">
                {{ $merchandiser->code }}
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-5">
                <strong>ID Number</strong>
            </div>
            <div class="uk-width-2-5">
                {{ $merchandiser->id_number }}
            </div>
            <div class="uk-width-1-5">
                <strong>Phone</strong>
            </div>
            <div class="uk-width-1-5">
                {{ $merchandiser->phone }}
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-5">
                <strong>Email</strong>
            </div>
            <div class="uk-width-2-5">
                {{ $merchandiser->email }}
            </div>
            <div class="uk-width-1-5">
                <strong>Salary</strong>
            </div>
            <div class="uk-width-1-5">
                {{ number_format($merchandiser->salary) }}
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-5">
                <strong>Salary Level</strong>
            </div>
            <div class="uk-width-2-5">
                {{ $merchandiser->salary_level }}
            </div>
            <div class="uk-width-1-5">
                <strong>Account number</strong>
            </div>
            <div class="uk-width-1-5">
                {{ $merchandiser->account_no }}
            </div>
        </div>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-5">
                <strong>Bank</strong>
            </div>
            <div class="uk-width-2-5">
                {{ ($merchandiser->bank)? $merchandiser->bank->title: 'error' }}
            </div>
            <div class="uk-width-1-5">
                <strong>Branch</strong>
            </div>
            <div class="uk-width-1-5">
                {{ ($merchandiser->branch_of_bank($merchandiser->bank_code,$merchandiser->bank_branch_code))? $merchandiser->branch_of_bank($merchandiser->bank_code,$merchandiser->bank_branch_code)->title: 'error' }}
            </div>
        </div>
    </div>
</div>

@endsection