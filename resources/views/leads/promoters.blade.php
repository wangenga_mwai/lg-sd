<?php 


	$title = 'Leads';

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<h3 class="heading_b uk-margin-bottom">{{ $title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        @include('inc.leads_nav')
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                <table class="uk-table uk-table-striped">
                	<thead>
                        <th>Promoter Code</th>
                        <th>Promoter Name</th>
                        <th>Closure Rate</th>
                        <th>Action</th>
                	</thead>
                	<tbody>
                		@foreach($promoters as $key => $promoter)
                		<tr>
                            <td>{{ $promoter->code }}</td>
                            <td>{{ $promoter->first_name . ' ' . $promoter->last_name }}</td>
                            <td>{{ $promoter->lead_count ? $promoter->lead_count->lead_count / count($promoter->leads) * 100: 0}} &#37;</td>
                            <td>
                                <a href="/leads/promoter/{{ $promoter->code }}/view" class="view md-btn action-btn md-btn-info"><i class="uk-icon-info" ></i></a>
                                
                            </td>
                			
                		</tr>
                		@endforeach
                	</tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="uk-modal" id="view-modal">
    <div class="uk-modal-dialog uk-modal-dialog-large">
        <div class="uk-modal-header">
            <div class="uk-modal-body"></div>
        </div>

    </div>
</div>
@endsection