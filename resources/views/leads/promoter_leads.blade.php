<?php 


	$title = 'Leads';

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<h3 class="heading_b uk-margin-bottom">{{ $title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        <a href="/leads/promoter/{{ $promoter->code }}/view?status=1" class="uk-button uk-button-primary view">Open</a>
        <a href="/leads/promoter/{{ $promoter->code }}/view?status=2" class="uk-button uk-button-primary view">Closed Won</a>
        <a href="/leads/promoter/{{ $promoter->code }}/view?status=3" class="uk-button uk-button-primary view">Closed Lost</a>
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                @if($promoter->leads)
                <table class="uk-table uk-table-striped">
                	<thead>
                        <th>Customer Name</th>
                        <th>Customer Phone</th>
                        <th>Email</th>
                        <th>Date</th>
                        <th>Time</th>
                		<th>Status</th>
                	</thead>
                	<tbody>
                		@foreach($promoter->leads as $key => $lead)
                		<tr>
                            <td>{{ $lead->name }}</td>
                            <td>{{ $lead->phonenumber }}</td>
                            <td>{{ $lead->email }}</td>
                            <td>{{ $lead->date }}</td>
                            <td>{{ $lead->time }}</td>

                            <td>{{ $lead->statusRel->title }}</td>
                			
                		</tr>
                		@endforeach
                	</tbody>
                </table>
                @else
                <p>No records found.</p>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="uk-modal" id="view-modal">
    <div class="uk-modal-dialog uk-modal-dialog-large">
        <div class="uk-modal-header">
            <div class="uk-modal-body"></div>
        </div>

    </div>
</div>
@endsection