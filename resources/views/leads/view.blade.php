<?php 


	$title = 'Leads';

?>
@extends('layouts.master')

@section('title', $title)

@section('content')
<h3 class="heading_b uk-margin-bottom">{{ $title }}</h3>

<div class="md-card">
    <div class="md-card-content">
        @include('inc.leads_nav')
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-1-1">
                <table class="uk-table uk-table-striped">
                	<thead>
                        <th>Promoter Code</th>
                        <th>Promoter Name</th>
                        <th>Customer Name</th>
                        <th>Customer Phone</th>
                        <th>Email</th>
                        <th>Date</th>
                        <th>Time</th>
                		<th>Status</th>
                	</thead>
                	<tbody>
                		@foreach($leads as $key => $lead)
                		<tr>
                            <td>{{ $lead->promoter_code }}</td>
                            <td>{{ $lead->promoter ? $lead->promoter->first_name . ' ' . $lead->promoter->last_name: '*' }}</td>
                            <td>{{ $lead->name }}</td>
                            <td>{{ $lead->phonenumber }}</td>
                            <td>{{ $lead->email }}</td>
                            <td>{{ $lead->date }}</td>
                            <td>{{ $lead->time }}</td>

                            <td>{{ $lead->statusRel->title }}</td>
                			
                		</tr>
                		@endforeach
                	</tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection